--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: bounding_box; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE bounding_box AS (
	x integer,
	y integer,
	width integer,
	height integer
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: image_frame; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE image_frame (
    path_to_frame text NOT NULL,
    view_date date,
    view_time time without time zone,
    camera_num smallint
);


--
-- Name: get_dates(date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE result image_frame%rowtype; 				 BEGIN 				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 					AND   view_date <= end_date; 				 END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE result image_frame%rowtype; 				 BEGIN 				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time; 					END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone, smallint[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone, cameras smallint[]) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE result image_frame%rowtype; 				 BEGIN 				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time 						AND   camera_num = ANY(cameras);				    END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone, integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone, cameras integer[]) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE result image_frame%rowtype; 				 BEGIN 				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time 						AND   camera_num = ANY(cameras);				    END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone, text[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone, camera_text text[]) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE 				 	cameras smallint[];				 	result image_frame%rowtype; 				 BEGIN 				 	cameras = string_to_array(camera_text,',');				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time 						AND   camera_num = ANY(cameras);				    END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone, smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone, camera_text smallint) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE 				 	result image_frame%rowtype; 				 	       cameras smallint[];				 BEGIN 				 	cameras = string_to_array(camera_text,',');				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time 						AND   camera_num = ANY(cameras);				    END; $$;


--
-- Name: get_dates(date, date, time without time zone, time without time zone, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_dates(start_date date, end_date date, start_time time without time zone, end_time time without time zone, camera_text text) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE 				 	cameras smallint[];				 	result image_frame%rowtype; 				 BEGIN 				 	cameras = string_to_array(camera_text,',');				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date 						AND   view_time >= start_time 						AND   view_time <= end_time 						AND   camera_num = ANY(cameras);				    END; $$;


--
-- Name: get_datess(date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_datess(start_date date, end_date date) RETURNS SETOF image_frame
    LANGUAGE plpgsql
    AS $$ 				 DECLARE result image_frame%rowtype; 				 BEGIN 				  	RETURN QUERY 				 	SELECT * FROM image_frame 					WHERE view_date >= start_date 						AND   view_date <= end_date; END; $$;


--
-- Name: imagecategories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE imagecategories (
    image_path text NOT NULL,
    categories integer[]
);


--
-- Name: profiler_info; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE profiler_info (
    path_to_frame text NOT NULL,
    boxes bounding_box[]
);


--
-- Name: scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE scores (
    image_path text,
    box bounding_box,
    head_histo real[],
    torso_histo real[],
    legs_histo real[],
    score real
);


--
-- Name: image_frame_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY image_frame
    ADD CONSTRAINT image_frame_pkey PRIMARY KEY (path_to_frame);


--
-- Name: imagecategories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY imagecategories
    ADD CONSTRAINT imagecategories_pkey PRIMARY KEY (image_path);


--
-- Name: profiler_info_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY profiler_info
    ADD CONSTRAINT profiler_info_pkey PRIMARY KEY (path_to_frame);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nicholasjakins;
GRANT ALL ON SCHEMA public TO nicholasjakins;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

