--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: box; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE box AS (
	x real,
	y real,
	width real,
	height real
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: partition; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE partition (
    image_path text NOT NULL,
    image_height real,
    image_width real,
    z_base pg_catalog.box,
    z_head pg_catalog.box,
    z_torso pg_catalog.box,
    z_legs pg_catalog.box
);


--
-- Name: partition_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY partition
    ADD CONSTRAINT partition_pkey PRIMARY KEY (image_path);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nicholasjakins;
GRANT ALL ON SCHEMA public TO nicholasjakins;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

