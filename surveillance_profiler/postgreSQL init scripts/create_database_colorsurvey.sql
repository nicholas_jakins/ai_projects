--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: hsv; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE hsv AS (
	hue integer,
	saturation integer,
	value integer
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: colorcategory; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE colorcategory (
    username text NOT NULL,
    category text,
    color hsv NOT NULL
);


--
-- Name: hsvobservations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE hsvobservations (
    color hsv NOT NULL,
    category integer
);


--
-- Name: modified_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE modified_users (
    username text NOT NULL,
    colors integer[]
);


--
-- Name: colorcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY colorcategory
    ADD CONSTRAINT colorcategory_pkey PRIMARY KEY (username, color);


--
-- Name: hsvobservations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY hsvobservations
    ADD CONSTRAINT hsvobservations_pkey PRIMARY KEY (color);


--
-- Name: modified_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY modified_users
    ADD CONSTRAINT modified_users_pkey PRIMARY KEY (username);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nicholasjakins;
GRANT ALL ON SCHEMA public TO nicholasjakins;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

