\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Brief description}{2}{0}{1}
\beamer@sectionintoc {2}{Overview}{3}{0}{2}
\beamer@sectionintoc {3}{Front-end}{4}{0}{3}
\beamer@subsectionintoc {3}{1}{Specify query}{5}{0}{3}
\beamer@subsectionintoc {3}{2}{View results}{6}{0}{3}
\beamer@sectionintoc {4}{Back-end}{7}{0}{4}
\beamer@subsectionintoc {4}{1}{Connector}{7}{0}{4}
\beamer@subsectionintoc {4}{2}{Profiler}{10}{0}{4}
\beamer@subsubsectionintoc {4}{2}{1}{PGM's used}{11}{0}{4}
\beamer@sectionintoc {5}{Results}{13}{0}{5}
\beamer@subsectionintoc {5}{1}{Performance and colour model}{13}{0}{5}
\beamer@subsectionintoc {5}{2}{Scoring results}{14}{0}{5}
\beamer@subsectionintoc {5}{3}{Conclusion}{15}{0}{5}
