\contentsline {section}{Acronyms}{3}{section*.1}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Outline}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Approach}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Scope}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Structure}{4}{subsection.1.4}
\contentsline {section}{\numberline {2}Requirements}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}System outline}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Front-end}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Functional requirements}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Functional requirements not implemented}{5}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Additional functionality implemented}{6}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Back-end}{6}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Person detection and body segmentation}{6}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Scoring of image frames}{6}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Connection to the \acrshort {csl} \acrshort {DVR}}{6}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Training of parameters}{6}{subsubsection.2.3.4}
\contentsline {subsubsection}{\numberline {2.3.5}Database interaction}{7}{subsubsection.2.3.5}
\contentsline {subsubsubsection}{\numberline {2.3.5.1}Retrieval}{7}{subsubsubsection.2.3.5.1}
\contentsline {subsubsubsection}{\numberline {2.3.5.2}Storage}{7}{subsubsubsection.2.3.5.2}
\contentsline {subsection}{\numberline {2.4}Performance requirements}{7}{subsection.2.4}
\contentsline {section}{\numberline {3}Design}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}System overview}{7}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Structure of the query: input profile}{9}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Front-end}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Views}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Design paradigm}{13}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Back-end}{14}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Connector component}{14}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Tiered detection algorithm}{15}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Homography of the foot to head plane}{16}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Databases}{16}{subsubsection.3.5.4}
\contentsline {subsubsection}{\numberline {3.5.5}Colour survey}{19}{subsubsection.3.5.5}
\contentsline {section}{\numberline {4}Profiling and modeling}{20}{section.4}
\contentsline {subsection}{\numberline {4.1}Need for \acrshort {PGM}'s}{22}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The models}{22}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}The colour model}{22}{subsubsection.4.2.1}
\contentsline {subsection}{\numberline {4.3}Estimating the state for the parameter variable}{23}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Body model}{24}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Estimating the parameters of the body model}{28}{subsubsection.4.3.2}
\contentsline {subsection}{\numberline {4.4}The scoring algorithm}{30}{subsection.4.4}
\contentsline {section}{\numberline {5}Implementation and challenges}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}Connection to the \acrshort {csl} \acrshort {DVR}}{32}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Efficiency}{33}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Caching of log factorials}{33}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Detection before scoring}{33}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Faster generation of the observed variables for the body model}{33}{subsubsection.5.2.3}
\contentsline {subsection}{\numberline {5.3}Novelties}{34}{subsection.5.3}
\contentsline {section}{\numberline {6}Evaluation and testing}{35}{section.6}
\contentsline {subsection}{\numberline {6.1}Correctness testing}{35}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Performance testing}{36}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Testing of the colour model}{38}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Static tests}{39}{subsection.6.4}
\contentsline {section}{\numberline {7}Conclusion}{40}{section.7}
\contentsline {section}{\numberline {A}Pixels and colour spaces}{41}{appendix.A}
\contentsline {section}{\numberline {B}The Dirichlet distribution}{42}{appendix.B}
\contentsline {section}{\numberline {C}Derivation of the component score}{43}{appendix.C}
\contentsline {section}{\numberline {D}Derivations of quantities required for Newton-Raphson optimization}{44}{appendix.D}
\contentsline {section}{\numberline {E}Visual representation of the modified hue distributions}{46}{appendix.E}
