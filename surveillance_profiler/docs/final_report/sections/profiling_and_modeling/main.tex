
This section is dedicated to introducing the probabilistic graphical models used by the \acrshort{sscp}. First, a simple example of a \acrshort{PGM} is given, this is intended to introduce the basic concepts necessary. Then structures of the \acrshort{PGM}'s used for this project followed by the descriptions of the algorithms will be detailed. The focus will be on defining the forms of the underlying distributions as well as the methods for training. \newline


\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/simple_pgm.png}
\captionof{figure}{A simple example \acrshort{PGM} with three variables, A, B and C.}
\label{fig:simple}
\end{minipage}}
\hfill
\newline

\acrshort{PGM}'s allow for the representation of statistical dependencies amongst random variables. Figure (\ref{fig:simple}) shows an example \acrshort{PGM} containing three variables, A, B and C. Each of these \say{nodes} represent a random variable (discrete or continuous) and arrows indicate where there are dependencies. We observe that variable C is dependent on variables A and B (indicated by the arrows). An important term used in association with a \acrshort{PGM} is the joint probability distribution. \newline

\theoremstyle{definition}
\begin{definition}{Joint probability distribution:}
When given at least two random variables $V1,..., Vj : j > 2$ (discrete or continuous), the joint probability distribution is a probability distribution that gives the probability that variable $V_{i} i \in [1, j]$ falls in a particular range given the dependancies of the other variables.
\end{definition}

We represent the joint probability distribution of variables A, B and C:

\[P(A, B, C) = P(A)P(B)P(C|A,B)\]

The dependencies represented by the graph govern the factorization (right-hand side) of the joint probability distribution. The dependencies are formulated based on assumptions about how the variables interact. When considering large \acrshort{PGM}'s and without making assumptions about the dependencies of the variables it is likely that the factorization will have exponentially many terms. \newline

It is crucial to note when trying to understand \acrshort{PGM}'s the different types of variables. For example, note that variables $A$ and $B$ are different to variable $C$ in that they don't have any dependencies. Where there is a variable with a corresponding term in the factorization that doesn't depend on other variables, we denote that variable a prior. The models that will be discussed will contain prior variables.


\theoremstyle{definition}
\begin{definition}{Prior variable:}
Refers to a variable in a \acrshort{PGM} that has an associated factor not dependent on any of the other variables. In bayesian statistics, priors are beliefs that are made about a variable before analysing the rest of the model.
\end{definition}

\acrshort{PGM}'s typically have at most three types of variables:

\begin{itemize}
\item Parameter variables: A prior variable is an example of a parameter variable. States of a parameter variable are determined externally. \footnote{A state of a variable refers to a specific value assigned to that variable. The convention for indicating that a variable $v$ has been assigned a value is denoting it $\hat{v}$.} In the models to be discussed this external assignment to parameter variables will be referred to as training.
\item Observed variables: We assign a state to an observed variable by making an observation of the environment that variable is modelling. There are many ways to assign states to observed variables including with the use of another \acrshort{PGM}. This will be the case for the models to come.
\item Hidden/Latent variables: These variables are not intuitive to understand as they are as assigned states typically by statistical inference \footnote{Statistical inference is the process of deducing properties of an underlying distribution by the analysis of data. This data may be in the form of observed states, parameter states or a combination of both.} (note that other variables such as parameter variables can also be assigned states using inference). Latent variables are unknown until inferred.
\end{itemize}

Given are additional definitions for terms that will be used in this chapter.

\theoremstyle{definition}
\begin{definition}{Marginal probability: (\cite{barber2012bayesian})}
Given a joint distribution of two or more variables $P(x_{1},\ ...,\ x_{j}):      
j > 1$ we define the probability of one of the variables as:

\[P(x_{1}, ..., x_{i - 1}, x_{i + 1}, ..., x_{j}) = \sum_{x_{i}} P(x_{1},\ ...,\ x_{j}) : i \in [1, j], j > 1\]

For continuous variables the sum becomes an integral, so that we define the marginal probability as:

\[P(x_{1}, ..., x_{i - 1}, x_{i + 1}, ..., x_{j}) = \int_{x_{i}} P(x_{1},\ ...,\ x_{j}) : i \in [1, j], j > 1\]

The left-hand side of the equations above refers to the marginal of the joint distribution. The process of calculating the marginal is referred to as marginalisation.

\end{definition}


\theoremstyle{definition}
\begin{definition}{Maximum likelihood estimation:}
The method for estimating the states of variables in a \acrshort{PGM} given observations. This is done by estimating parameter values that maximise the joint probability distribution.
\end{definition}


\newpage

\subsection{Need for \acrshort{PGM}'s}

This project is focused on the analysis image frames. Ultimately, we want the \acrshort{sscp} to make accurate decisions on the colours of certain regions of an image (regions surrounding the head, torso and legs). Assigning a colour category to a region on an image frame is not a trivial task. Two factors make this type of decision making difficult: firstly, we need a way to account for the variability in how people perceive colours. What one person may see as the colour blue, another might see as
the colour gray. Consider the viral phenomenon of the \say{blue} dress. \footnote{URL: \texttt{https://en.wikipedia.org/wiki/The\_dress\_(viral\_phenomenon)}} The dispute about the colour of the dress highlights the fact that colour is perceived differently. Secondly, the colour of clothing is susceptible to environment changes and clothing materials. The structures and inference techniques of the models to be discussed allow for an environment where accurate decision making about colours of regions in an image can be made. This is dealt with by soft allocation of colours to regions and weighing evidence in a probabilistic environment.


\subsection{The models}

\subsubsection{The colour model} \label{colour_model}

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/colour_model.png}
\captionof{figure}{The structure of the colour model. This model is used to classify pixel coordinates by assigning to them colour categories.}
\label{fig:col_model}
\end{minipage}}
\hfill
\newline

Figure \ref{fig:col_model} shows the structure of what is referred to as the colour model. We describe the properties of the model and the variables. The plate notation in the model, $i = 1 : K$ means that the variables enclosed in the rectangular region are replicated K times (we do this to simplfy the visualization). The colour model's joint likelihood distribution can be represented with a factorization, as with the example \acrshort{PGM}:

\[P(\theta, O_{1}, ..., O_{k}, H_{1}, ..., H_{k}) =  P(\theta)\prod_{i=1}^{K}P(O_{i}|H_{i}, \theta)P(H_{i})\]


The observed variables of the model are denoted $O_{i}$, they represent individual pixel coordinate observations from the chosen colour space. \footnote{Pixels can be represented using one of many colour spaces. Such example colour spaces are RGB, HSV and CMYK.} Each pixel has an associated latent variable denoted $H_{i}$. Variable $\theta$ is a parameter, assumed unknown, and inferred a state using an offline training process. The method for the inference of variable $\theta$ is that of maximum likelihood and is detailed in the chapter (\cite{CHAP}). Variable state $\hat{\theta}$ comprises a set of mean and covariance matrices $\{\mu_{j}, \sigma_{j} : j \in [1, 12]\}$ (we choose twelve colour categories that can be assigned to any pixel observation). Each colour category is assumed to be represented as a normal distribution over the pixel observations of the chosen colour space. \newline

Inference of the latent variables allows the assignment of colour categories to individual pixels (one region within an image is comprised of many pixels). The choice of the colour space used for the pixel observations is crucial. \footnote{Appendix \ref{App:AppendixA}} Thornton argues that the use of HSV, as opposed to RGB, allows for a more accurate model. This is based on the assumption that the hue component corresponds well to how humans perceive colour. Due to the circular property of the hue component however, calculations of the mean and covariances cannot be found using the standard approach. To account for this Thorton introduces a quasi-normal density function to representation  distributions. \footnote{CITE CHAPTER section 8.2.1, equation (8.2)} To avoid defining a quasi-normal density in the manner that Thornton proposes, an alternative colour space was used. \newline

To remove the circular property of the hue, we define the following transformation:


\begin{equation} \label{transformation}
T(h, s, v) = (cos(h),\ sin(h),\ s,\ v)
\end{equation}

The transformation is a map from the 3-dimensional HSV coordinate space to one of 4 dimensions
(a modified HSV coordinate space).

\subsection{Estimating the state for the parameter variable}

We discuss the training of the model parameter $\theta$ from the colour model. Variable $\theta$ comprises 12 mean vectors and 12 covariance matrices, one pair governing a distribution for each colour category. Samples are obtained via an assignment of colour categories to HSV observations with test subjects. This was done using a colour survey, implemented in the form of a local web application. \footnote{The colour survey was designed using the Django web application framework with a paradigm similar to figure \ref{fig:des_paradigm}.} Test subjects are able to assign colour categories to HSV colour blocks (colour blocks generated uniformly) and the results are stored to a local PostgreSQL database \ref{col_database}, Storing results in a database meant parameters can be configured offline and queried on demand. When we wish to estimate mean vector and covariance matrix for colour category $i$, first a query is used to return all observations for a particular colour. Then we estimate a state for the variable using the approach of maximum likelihood. \footnote{The standard formulae for estimating parameters of a multivariate Gaussian distribution are used.} With $M$ observations returned by the query labelled colour $i$, we generate mean vector $\mu_{i}$ and covariance matrix $\sigma_{i}$ as below:

\begin{eqnarray}
\mu_{i} &=& \frac{1}{M} \sum_{i=1}^{M} o_{i} \\
\Sigma_{i} &=& \frac{1}{M-1} \sum_{i=1}^{M}(o_{i} - \mu_{i})(o_{i} - \mu_{i})^{T}
\end{eqnarray}

Note that $o_{i}$ refers to the $i_{th}$ pixel observation returned by the query after being transformed using equation (\ref{transformation}). We assign the full set of mean vectors and covariance matrices to $\theta$ as:

\begin{equation}
\theta = \{(\mu_{i},\ \sigma_{i}) : i \in [1, 12]\}
\end{equation}

\subsubsection{Body model}


\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/body_model.png}
\captionof{figure}{The structure of the body model. Inferring the hidden states of the body model allows the formulation of a scoring algorithm.}
\label{fig:body_model}
\end{minipage}}
\hfill
\newline

Figure \ref{fig:body_model} shows the structure of the second \acrshort{PGM}, the body model. The body model will be used to determine whether an image frame matches the input profile. As in the colour model, we see that the body model uses plate notation (variables $Y_{i}$, $\pi_{i}$ and their dependencies are replicated N times). Here $N$ refers to the number of \acrshort{roi}'s (one observed state for each of the three body parts 1-head, 2-torso and 3-legs), thus $N=3$. We begin by discussing the observed variables, $Y_{i}$ and $A$. We define the observed variable $Y_{i}$ as the histogram of colour observations with dimension 12.  \newline

The $k_{th}$ component of the histogram, denoted $\hat{Y_{i}(k)}$ is the number of observed pixel values of colour $k$ within region $i$ of the image. We assign the colour categories to the pixels by using the colour model, then the histogram is normalised to sum to 100. In doing so we remove the effect of image resolution. We are only interested in the relative frequency of the colour category for each region. Note how the colour model is used to generate the observed states of the body model. \newline

Observed variable A are the attributes of the query. The observed state of A encodes the colour assignments for each body part given by the user query. Giving the mapping of body parts $\{1:\ head,\ 2:\ torso,\ 3:\ legs\}$ observed variable $A$ has the following form:

\[A = (c_{1}, c_{2}, c_{3})\]

Where the component $c_{i} : i \in [1, 3]$ is the integer corresponding to the colour queried for body part $i$. \newline

Variable $\Phi$ is a parameter, this variable is assigned a state using an offline training process (\ref{training_body}). The state assignment to $\Phi$ is a mean vector and covariance matrix. This mean vector and covanance matrix govern a multivariate normal distribution, where the samples drawn are of dimension 12. Let $s$ be a sample from this distribution, then $s$ has the following form: 

\begin{equation} \label{partition_vec}
s = ([b_{1}],\ [b_{2}],\ [b_{3}])
\end{equation}

Where $b_{i}$ is a bounding box surrounding body part $i \in (1,3)$ and is defined as:

\[b_{i} = (x_{0},\ y_{0}, x_{1}, y_{1})\]

The first two components $(x_{0}, y_{0})$ specify the location of the top left corner of the box. The remaining components ($x_{1}, y_{1}$) specify the bottom right corner of the box. Since the location of a body in an image isn't fixed, the coordinates are calculated as percentages of the \acrshort{roi} surrounding the whole body. \newline \footnote{Given that the origin of the image has the coordinate $(0, 0)$ and is geometrically located at the top left corner of an image. We calculate the components of a bounding box $b_{i}$ as follows. Let $H$ be the height of the \acrshort{roi} surrounding the body and let $W$ be the width. Furthermore assume $(b_{x0}. b_{y0}. b_{x1}, b_{y1})$ defines the \acrshort{roi} for the body. Then if $c_{xo}$ is the x-coordinate of the top left \acrshort{roi} surrounding body part $i$, $x_{0} = \frac{b_{x0} - c_{x0}}{100} * W$ with similar calculations for the other coordinates.}

Variables $Z$ and $\alpha$ are latent, $Z$ allows for the partitioning of the image frames into the three body parts. The inference of $Z$ involves using the state assigned to parameter $\Phi$, more precisely $Z$ is inferred by sampling from the normal distribution governed by $\hat{\Phi}$. \newline

In the same way that the parameter variable $\Phi$ is assigned a state externally, so is the parameter $\alpha$ (CITE training). Parameter variable $\alpha$ comprises three vectors of dimension 12, each vector taken from a separate matrix of the following form:

\begin{equation} \label{alpha}
\begin{bmatrix}
    \omega_{1}(1) & \omega_{1}(2) &  \dots  & \omega_{1}(12) \\
    \omega_{2}(1) & \omega_{2}(2) &  \dots  & \omega_{2}(12) \\
    \vdots & \vdots & \ddots & \vdots \\
    \omega_{12}(1) & \omega_{12}(2) &  \dots  & \omega_{12}(12) \\
\end{bmatrix}
\end{equation}

We generate three matrices corresponding to the three body parts. Considering a single matrix: each row, denoted $\omega_{i}$ corresponds to colour $i$ and each component $k$ of row $i$ denoted $\omega_{i}(k)$ is a nonzero integer. Where the observed state $Y_{i}$ is a normalized histogram of colour observations with dimension 12, the vector $\omega_{i}$ is considered a \texttt{pseudocount} parameter. \footnote{See form of the Dirichlet distribution in Appendix (\ref{App:AppendixB})}. Parameter states $\omega_{i} : i \in [1, 3]$ are selected by choosing a row from $\alpha_{i}$ according to the encoding of the observed variable $A$. For example consider there being a query for black hair, green torso and blue legs. Then rows $\alpha$ would comprise three vectors denoted $\omega_{i}$, $\omega_{j}$, $\omega_{k}$. Where $\omega_{i}$ is the row taken from the first matrix $\alpha_{1}$ corresponding to the colour black. Similarly, $\omega_{j}$ is the row vector selected from the second matrix $\alpha_{2}$ where j corresponds to the row associated with green, and $\omega_{k}$ is selected from the third matrix $\alpha_{3}$ where k corresponds the row associated with blue. \newline

We discuss the form of the remaining latent variable, $\pi_{i}$. This variable has the following form after inference:

\[\hat{\pi_{i}} = (w_{i}(k),\ ...,\ w_{i}(12))\]

With the property that $\sum_{k=1}^{12} \pi_{i}(k) = 1$. Variable $\pi_{i}$ is best understood as a vector where each component is a proportional weighting for colour $k$. Furthermore, $\pi_{i}$ is obtained by drawing a sample from a special type of distribution called the Dirichlet distribution. \newline

\begin{equation} \label{hidden_state_form}
P(\pi_{i}|\omega) = \Phi(\omega)\cdot \prod_{k=1}^{12} \pi_{i}(k)^{\omega(k) - 1}
\end{equation}

Equation (\ref{hidden_state_form}) details the process of drawing the colour mixure $\pi_{i}$ using as input $\omega_{i}$ to a form of the Dirichlet distribution. We define $\Phi(\omega)$ as the Dirichlet normalization constant. Consider an example query encoded in $A$ and it's corresponding Dirichlet form. Suppose we are querying a green torso, then the associated Dirichlet distribution would have the following form:

\begin{equation}
P(\pi_{i} | A, \omega_{green}) = \Phi(\omega_{green})\cdot \prod_{k=1}^{12} \pi_{i}(k)^{\omega_{green}(k) - 1}
\end{equation}

What remains for discussion is the form of the underlying distribution relating to variables $Y_{i},\ \pi_{i}$ and $Z$. The probability of the resulting histogram $Y_{i}$ given the partition variable $Z$ and latent topic $\pi_{i}$ takes the form of a multinomial distribution. \footnote{Multinomial distributions are used for calculating the probabilities of categorical outcomes. The intuition here is that we are looking for an explanation of the observations $Y_{i}$ based on the colour mixture $\pi_{i}$. The explanation takes the form of a probabilistic likelihood.}

\begin{equation}
P(Y_{i}|Z,\ \pi_{i}) = \frac{100}{\prod_{k=1}^{12} f(Z,\ i,\ k)!} \cdot \prod_{k=1}^{12} \pi_{i}(k)^{f(Z,\ i,\ k)}
\end{equation}

Function $f$ is defined as a selector function. The value returned by f is the number of pixels in region $i$ assigned by the partition variable $Z$ and of colour $k$. The numerator of 100 is used because the observed histograms are normalised to sum to 100. \newline

This gives the full descriptions for the variables and underlying distributions. Next, a method for inference for the latent variables is discussed, followed by the method for the training of the parameters and then a description of the scoring algorithm.

\subsection*{Formulation for inference of the latent variables}

Considered is the joint probability distribution of the hidden and observed variables for the body model.

\begin{equation}
P(O,\ H) = P(Z|\Phi,\ A)P(Y_{i}|Z,\ \pi_{i})P(\pi_{i}|A,\ \alpha)
\end{equation}

Here we denote $O$ to be the set of observed variables and H to be the set of latent variables. Marginalising out the hidden variables of the joint distribution allows for the probabilistic likelihood of the observed variables. Since we have continuous hidden variables, we integrate:

\begin{equation}
P(O) = \int P(O, H)\ dH
\end{equation}

An expression for the likelihood of the observed variables as above could be used as an estimate of the score. However as mentioned by Thornton, this type of integral is too computationally expensive to find a solution for. We instead use an alternative method, maximum likelihood estimation. So instead of marginalising over all possible values for the hidden states we find the form of the hidden states that maximises the joint probability. There are two different types of latent variables associated with the body model ($\pi_{i}$ and $Z$). We assume a fixed state for variable $Z$ and use \acrshort{MLE} to approximate a state for variable $\pi_{i}$:

\begin{equation} \label{infer_pi}
\hat{\pi_{i}} = \arg \max_{i}\ P(O,\ H) = \arg \max_{i}\ P(\pi_{i}|A,\ \omega_{i})P(Y_{i}|\hat{Z},\ \pi_{i})
\end{equation}

Using Legrange multipliers, the equation (\ref{infer_pi}) reduces to the following form \footnote{Derivation is not shown in this report, as the final form is taken directly \cite{CHAP}.}:

\begin{equation} \label{infer_derived}
\hat{\pi_{i}} = \left[\frac{Y_{i}(1) + \omega_{i}(1) - 1}{\sum_{k=1}^{12}(Y_{i}(k) + \omega_{i}(k) - 1)},\ ...,\ \frac{Y_{i}(12) + \omega_{i}(12) - 1}{\sum_{k=1}^{12}(Y_{i}(k) + \omega_{i}(k) - 1)} \right]
\end{equation}

Equation \ref{infer_derived} allows for a computationally inexpensive and closed-form solution for the inference of $\pi_{i}$. However, we are still assuming a fixed $\hat{Z}$. An iterative approach is needed for maximising the joint likelihood $P(O,H)$. To avoid having to apply an exhaustive search, the scoring algorithm to be presented will apply the following idea. We find the likelihoods of the model by inferring hidden states $\pi_{i}$ which maximise the joint probability, then an estimate of the score is obtained using only a few differently selected approximations of $\hat{Z}$. \newline

\subsubsection{Estimating the parameters of the body model} \label{training_body}

We discuss the generation of states for the two parameter variables of the body model, namely variables $\alpha$ and $\Phi$. We begin the discussion of generating the state for $\Phi$. Recall that $\hat{\Phi}$ comprises one mean vector and covariance matrix. These are used to govern a multivariate Gaussian distribution of partitions, we obtain samples of the partitions with the use of 1) a large set of training data and 2) an interactive partitioning tool developed in Python. Samples used to generate the mean vector and covariance matrix are in the form of equation \ref{partition_vec}. Training data takes the form of video segments obtained from the \acrshort{DVR} (stored to a local directory). The interactive partitioning tool facilitates the labelling of \acrshort{roi}'s for the head, torso and legs in the form of separate bounding boxes. \newline

The following steps describe the role of the partitioning tool:

\begin{enumerate}
\item Detection: Videos are first analysed individually and with the use of the tiered detection algorithm, image frames with detected bodies are isolated and stored locally.
\item The user \footnote{User in this section refers to anybody making use of the partitioning tool for parameter estimation and not necessarily a user of the \acrshort{sscp}.} is presented with each consecutive image frame and given two options:
\begin{enumerate}
\item For each image frame presented, the user isolates a region of the head, torso and legs
in that order.
\item Skip current image frame.
\end{enumerate}
\item For each successfully labelled partition, the interactive tool converts the \acrshort{roi}'s to the form given in equation \ref{partition_vec} and stores the results in a database.
\end{enumerate}

We ensure the partitioning tool performs a detection first. By only interacting with frames containing a person, the user spends less time skipping over frames that don't have one or more detections. Estimations of the mean vector $\mu$ and covariance matrix $\Sigma$ are calculated using the standard formulae for the estimation of parameters of a multivariate Gaussian distribution. We assign variable $\Phi$ a state as follows:

\begin{equation}
\Phi = (\mu,\ \Sigma)
\end{equation}

We estimate a set of matrices, $\{\alpha_{i} : [1,3]\}$ where each matrix of the set is of the form given by (\ref{alpha}). We first consider the training for each $\alpha_{i}$, then for each $\alpha_{i}$ we assign values row-wise. Each row denoted $\omega_{j}$ of matrix $\alpha_{i}$ corresponds to colour category $j \in [1, 12]$. Furthermore, each row is considered a pseudocount input for the associated Dirichlet distribution (see equation \ref{hidden_state_form}). The training for one row is a four part process: 1) obtain a collection of raw training data in the form of video sequences, 2) interactively isolate \acrshort{roi}'s pertaining to the head, torso and legs, 3) label colours associated with the \acrshort{roi}'s extracted in part two and 4) perform an analysis on labelled regions to generate row vector $\omega_{i}$ for matrix $\alpha_{i}$. \newline

As with the training of parameter $\Phi$, video sequences are collected from the \acrshort{DVR} and stored in a local directory. An interactive partitioning tool allows the isolation of the head, torso and legs by analysing the frames of each video. Each isolated \acrshort{roi} is considered a sample and is stored as a cropped image. As such there are three types of samples, (head, torso and legs). Part three describes the labelling process. Labelling involves assigning colour categories to the samples. In order to be able to assign labels easily another web application, run locally was developed \footnote{Creating a web application allows for easy labelling. The assignment of colour $i$ to sample $j$ is handled by copying the cropped image to a directory named the colour associated with $i$. Instead of manually copying the samples to the correct label directory, a button in the web application handles that process.} This application ensures that samples can be labelled according to their perceived colour and that many samples can be labelled quickly. Once all observations have been labelled, part four describes the process of generating the row vector $\omega_{i}$. \newline

Generating $\omega_{i}$ involves the optimization of equation (8.35) in (\cite{CHAP}). The algorithm is derived using \acrshort{MLE} of the parameters that maximise the joint probability distribution of the plate diagram, Figure (8.10) in \cite{CHAP}. We defer the derivation of the function to optimise for the reader to chapter (8.3.1.2) in (\cite{CHAP}), but present the derived function for discussion.

\begin{equation} \label{opt_function}
\hat{\omega_{i}} = \arg \max_{\omega} \left[M\cdot \log\Gamma \left( \sum_{k=1}^{12}\omega_{k}\right) - \sum_{j=1}^{M} \log \Gamma \left( \sum_{k=1}^{12} (\omega(k) + Y_{j}(k))\right) + \sum_{j=1}^{M}\sum_{k=1}^{12} \log \Gamma (\omega(k) + Y_{j}(k)) - M\sum_{k=1}^{12} \log \Gamma(\omega(k)) \right]
\end{equation}


The function denoted with the symbol $\Gamma$ is the the \say{gamma} function and $\log \Gamma$ is the \say{log gamma} function. \footnote{URL: \texttt{http://www.sosmath.com/calculus/improper/gamma/gamma.html.}} The inputs to this function are a set of M normalised histograms. This set is generated in the same manner that the observed states $Y_{i}$ from the body model are generated. \footnote{Namely, each pixel is evaluated and assigned a colour, say $c$, then the associated component index pertaining to $c$ of the histogram is incremented. This is repeated for every pixel within a given region.} The training images used to generate these histograms are those images labelled as colour $i$ from step 3. Estimation of $\hat{\omega_{i}}$ is done using the fixed point iteration technique, Nelder-Mead. \newline

Optimising all row vectors for the three matrices denoted $\alpha_{i} : i \in [1, 3]$ allows for the assignment of a state to $\alpha$. Given the observed state assigned to variable $A$, $\alpha$ comprises the three row vectors selected according to $A$'s encoding.

\subsubsection*{Optimization technique}

Nelder-Mead is a common numerical technique for finding a minimum or maximum of a multi-dimensional objective function. Using an initial estimate, the algorithm converges on a solution using a simplex. \footnote{A simplex is a generalisation of a triangle or a tetrahedron to arbitrary dimensions. If the objective function is n-dimensional, Nelder-Mead optimisation uses a simplex of dimensions $n + 1$.} Numerical techniques such as Nelder-mead are susceptible to local maxima or minima. This means that the solution isn't guaranteed to find a global maximum or minimum. Therefore the choice for the initial estimate is important, for this project a uniform initial estimate was used. This assumes that all colours are equally as likely to occur initially, the estimate changes based on the histograms that are passed as evidence. It may be worthwhile to consider the other techniques that are commonly used in addition to Nelder-Mead. For example, Newton-Raphson which is a generalisation of newton's method to multi-dimensional functions. This approach makes use of the objective functions first and second partial order derivatives to converge on a solution. \footnote{Derivations for the first order and second order partial derivatives are found in Appendix (\ref{App:AppendixD}).}


\subsection{The scoring algorithm}

Recall, as an alternative approach to marginalization of the hidden states for the joint likelihood we have instead used \acrshort{MLE}. Given fixed estimations of the hidden variables, an image score can be formulated as follows:

\begin{equation} \label{loose_score}
\hat{s} = P(O,\ \hat{H})
\end{equation}

Variable $\hat{s}$ refers to the score, note how we use $\hat{H}$ to indicate that we have used \acrshort{MLE} to approximate states for the hidden variables. Before evaluating the score we adjust equation (\ref{loose_score}) to the log likelihood of the joint probability distribution:

\begin{equation} \label{loose_score_log}
\hat{s} = \log\ P(O,\ \hat{H})
\end{equation}

We use the log likelihood because it makes subsequent computations easier to evaluate (logs turn products into sums). Note further that the log function is monotonic \footnote{A function that is either entirely increasing or decreasing.} and therefore doesn't affect the estimates of the hidden variables ($\hat{H}$). We expand the factorization of the joint likelihood, then present the derived equation for approximating the score.

\begin{equation} \label{the_score}
\hat{s} = \log P(\hat{Z}|A,\ \Phi) + \sum_{i=1}^{3} \log P(Y_{i}|\pi_{i},\ \hat{Z}) + \log P(\pi_{i}|\alpha,\ A)
\end{equation}

Furthermore, let variables $s_{0}$ and $s_{i}$ be defined as:

\begin{eqnarray}
s_{0} &=& \log \mathcal{N}(\hat{Z}|A,\ \Phi) \label{likelihood_s0} = \log P(\hat{Z}|A,\ \Phi) \\ 
s_{i} &=& \log P(Y_{i}|\pi_{i},\ \hat{Z}) + \log P(\pi_{i}|\alpha,\ A)
\end{eqnarray}

Deferring the derivation of the component score $s_{i}$ to Appendix (\ref{App:AppendixC}), $s_{i}$ expands to the following form:


\begin{equation} \label{comp_score}
s_{i} = -\sum_{k=1}^{12}\log Y_{i}(k)! + \sum_{k=1}^{12}(Y_{i}(k) + \alpha_{i}(k) - 1) \log \hat{\pi_{i}}(k) + \phi_{i}
\end{equation}

\newpage
We are now in a position to defining the algorithm for scoring. Detailed is pseudocode for assigning a score to an image frame. It is assumed that states have been assigned to the parameter variables as well as the encoding of the input profile assigned to variable A: \newline

\begin{algorithm}
  \caption{Iterative scoring algorithm} \label{MOG_class}
  \begin{algorithmic}[1]

    \Procedure{get\_current\_score}{$\hat{z_{i}}$}
        \State Compute histogram $Y_{i}$ using $\hat{z_{i}}$ 
        \State Infer hidden state $\pi_{i}$ \Comment{Equation (\ref{infer_derived})}

        \State Calculate component score $s_{i}$ \Comment{Equation (\ref{comp_score})}
        \State return $s_{i}$
    \EndProcedure \\


    \Procedure{Estimate score}{$img,\ params$}

    \State $\hat{Z} \gets Z_{mean}$
    \State $\hat{s_{0}} \gets \text{likelihood of observing} \hat{Z}$ \Comment{Equation (\ref{likelihood_s0})}

     \For{\texttt{< i in body\_parts>}}
         \State $\hat{s_{i}} \gets \text{GET\_CURRENT\_SCORE}(z_{i})$
     \EndFor

    \While {$\text{number of maximum iterations not exceeded}$}
        \For{\texttt{< i in body\_parts>}}
        \For{\texttt{< positions x in neighbourhood($z_{i}$)>}}
            \State $s_{0} \gets \text{likelihood of observing} \hat{Z}$
            \State $s_{i} \gets \text{GET\_CURRENT\_SCORE}(x)$
            
            \If{$s_{0} + s_{i} > \hat{s}_{0} + \hat{s}_{i}$}
                   \State $z_{i} \gets x$
                   \State $\hat{s}_{0} \gets s_{0}$
                   \State $\hat{s}_{i} \gets s_{i}$
               \EndIf 
        \EndFor
        \EndFor

     \EndWhile

    \State return $\hat{s_{0}} + \sum_{k=1}^{3} \hat{s_{i}}$
    \EndProcedure
  \end{algorithmic}
\end{algorithm}

The algorithm depicted above consists of two procedures. The first procedure, \texttt{GET\_CURRENT\_STATE} computes the component score given parameter $z_{i}$, this parameter defines the \acrshort{roi} surrounding body part $i$. The second procedure, \texttt{CONVERGE} performs an iterative search to find states of $Z$ that maximise the score given by equation (\ref{the_score}). \texttt{CONVERGE} takes in as arguments an image frame $img$ and parameters $params$, these parameters are the states assigned to the parameters of both \acrshort{PGM}'s. The algorithm as an initial estimate uses $Z_{mean}$. We see that new estimates of $\hat{Z}$ are approximated using a neighbourhood function (line 16). For each new estimate of $Z$ the score for the associated component $i$ is calculated, should the score be larger than the currently found score, the component score is updated. The algorithm returns with the highest score after a fixed number of iterations. \footnote{The number of iterations in practice is fairly small, this is to reduce the overall time taken to score an image frame.}





