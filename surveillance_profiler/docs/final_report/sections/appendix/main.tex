
\appendix


\section{Pixels and colour spaces} \label{App:AppendixA}

We know image frames are represented digitally using an array of pixels. Furthermore, the representation of each individual pixel is a coordinate from one of many colour spaces. This project is concerned with two such colour spaces, namely RGB and HSV.

\begin{itemize}
\item RGB: stands for red, blue and green with each pixel coordinate a 3-tuple. The most common representation for RGB values is 24-bits (8 bits per component). The higher the value of the pixel component the greater the density of that colour. Using the common representation of RGB pixels there are 16.7 million possible coordinates.
\item HSV: stands for hue, saturation and value. The HSV colour model is designed as a re-arrangement of the RGB colour model, this is to better correspond with how colours are perceived by the human visual system. Often visualized as a cone in 3-D space, the convention is to restrict the range of h to $[0, 1]$ with $\{s,\ v\} \in [0,\ 256)$. This is not the case with all image processing packages, \texttt{OpenCV2} represent the hue with $h \in [0, 177)$.
\end{itemize}

Figure (\ref{fig:hsv_col}) shows how increases and decreases of the h, s and v components affect the resultant colour. \newline

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/hsv.png}
\captionof{figure}{Visualization of the HSV colour space.}
\label{fig:hsv_col}
\end{minipage}}
\hfill
\newline

\newpage
\section{The Dirichlet distribution} \label{App:AppendixB}

This chapter focuses on introducing the Dirichlet distribution, how it works and why it is used for the body model. \newline

Bela A. Frigyik et al. (\cite{frigyik2010introduction}) give and introduction to the form and use of the Dirichlet distribution. We define the Dirichlet distrbution as follows:

\theoremstyle{definition}
\begin{definition}{Dirichlet distribution:}
Given a random \acrshort{PMF} defined by $Q = [Q_{1}, ...,\ Q_{k}]$ \footnote{With the assumption that each component $Q_{i} \geq 0$ and $\sum_{i=1}^{k} Q_{i} = 1$.} Also, given $\alpha = [\alpha_{1}, ...,\ \alpha_{k}]$, where $\alpha_{i} : i \in [1, k] > 0$. Then $Q$ is said to have a Dirichlet distribution with parameter $\alpha$, denoted $Q \sim Dir(\alpha)$:

\[f(q,\ \alpha) = \begin{cases} 
      0 & \text{if q is not a pmf}  \\
      \frac{\Gamma(\sum_{i=1}^{k} \alpha_{i})}{\prod_{i=1}^{k} \Gamma(\alpha_{i})} \prod_{i=1}^{k}q_{i}^{\alpha_{i} - 1} & \text{otherwise} \\
      
   \end{cases}\]

\end{definition}


We interpret the definition of the Dirichlet distribution with an example. Consider the scenario involving drawing a die from a bag of dice. Further, consider a bag with $N$ distinct 6-sided dice, and not all dice are equal i.e. the probability of rolling a specific number varies between die. The probability of rolling a number $i$ on each dice can be represented using a probability mass function $D_{t}$ with dimension $k=6$.

\[D_{t} = [c_{1},\ ...,\ c_{k}].\]

We relate the process of drawing a die from the bag as drawing a \acrshort{PMF} from a Dirichlet distribution. Looking at the hidden states of the body model, we draw variables $\pi_{i}$ using $\omega_{i}$ as parameters to a Dirichlet distribution. These parameters are known as \texttt{pseudocounts}. Intuitively, the pseudocounts are loosely defined colour mixtures representing a particular colour. Each component $k$ of the parameter is an informed guess to the weighting of that colour category. \footnote{A larger \say{weight} for colour $k$ implies a higher expected occurrence in the mixture.}

\[\pi_{i} \sim D_{i}(\omega_{i})\]

Representing $\pi_{i}$ as a \acrshort{PMF} in this manner accounts for the possible colour variations that occur within a given rectangular region of an image frame. Note $D_{i}$ refers to the Dirichlet distribution with $\omega_{i}$ as input in the place of $\alpha$ in the definition above.

\newpage
\section{Derivation of the component score} \label{App:AppendixC}

When substituting in the log of the underlying distributions and simplifying we see that we obtain equation (\ref{comp_score}).

\begin{eqnarray}
\log\ p(\pi_{i} | A, \alpha) &=& \log\left( \phi(\omega_{i}) \prod_{k=1}^{N_{c}} \pi_{i}(k)^{\omega_{i}(k) - 1} \right) \\
\log\ p(\pi_{i} | A, \alpha) &=& \log(\phi(\omega_{i})) + \sum_{k=1}^{N_{c}} \log(\pi_{i}(k)^{\omega_{i}(k)-1}) \\
\log\ p(\pi_{i} | A, \alpha) &=& \log(\phi(\omega_{i})) + \sum_{k=1}^{N_{c}} (\omega_{i}(k) - 1) \log(\pi_{i}(k)) \\
\log\ p(Y_{i} | Z, \pi_{i}) &=& \log \left( \frac{100!}{\prod_{k=1}^{N_{c}} Y_{i}(k)!} \prod_{k=1}^{N_{c}} \pi_{i}(k)^{Y_{i}(k)} \right) \\
\log p(Y_{i} | Z, \pi_{i}) &=& -\log (\sum_{k=1}^{N_{c}}Y_{i}(k)!) + \log(100!) + \sum_{k=1}^{N_{c}} \log (\pi_{i}(k)) Y_{i}(k)
\end{eqnarray}

By summing equations (24) and (26) we get the expanded component score:

\begin{equation}
s_{i} = -\log (\sum_{k=1}^{N_{c}}Y_{i}(k)!) + \sum_{k=1}^{N_{c}} \log (\pi_{i}(k)) (Y_{i} + \omega_{i}(k) - 1) + \phi_{i}
\end{equation}

Note that:

\begin{eqnarray}
\phi_{i} &=& \log(100!) + \log(\phi(\omega_{i})) \\
\log\ \phi(\omega_{i}) &=& \log\ B(\omega_{i}) \\
\log\ B(\omega_{i}) &=& \log\ \frac{\prod_{j=1}^{K}\Gamma(\omega_{i}(j))}{\Gamma(\prod_{j=1}^{K}\omega_{i}(j))} \\
\log\ B(\omega_{i}) &=& \sum_{j=1}^{K} \log(\Gamma(\omega_{i}(j))) + \log\ \Gamma(\prod_{j=1}^{K}\omega_{i}(j))
\end{eqnarray}

\newpage
\section{Derivations of quantities required for Newton-Raphson optimization} \label{App:AppendixD}
We want to optimize function $f$ represented by equation (\ref{opt_function}). Let $f = T_{1} + T_{2} + T_{3} + T_{4}$:

\begin{eqnarray}
T_{1} &=& M \log \Gamma (\sum_{i=1}^{k} \omega_{i}) \\
T_{2} &=& - \sum_{j=1}^{M} \log \Gamma (\sum_{i=1}^{k}( \omega_{i} + Y_{i})) \\
T_{3} &=& \sum_{j=1}^{M} \sum_{i=1}^{k} \log \Gamma ( \omega_{i} + Y_{i}) \\
T_{4} &=& -M \sum_{i=1}^{k} \log \Gamma (\omega_{i})
\end{eqnarray} 

and $M$ refers to the number of training histograms. Newton-Raphson optimization of $f$ uses the update rule below:

\begin{equation}
\hat{\omega}^{n + 1} = \hat{\omega}^{n} - H_{\omega}(f)^{-1}\cdot\ \nabla_{\omega} (f)
\end{equation}

Where $H_{\omega}(f)$ is the Hessian matrix of $f$, evaluated at $\hat{\omega_{n}}$.

\begin{equation}
H_{w}(f) = \begin{bmatrix}
    \frac{\partial^{2}}{\partial \omega_{1}^2} f & \frac{\partial^{2}}{\partial \omega_{1} \omega_{2}} f &  \dots  & \frac{\partial^{2}}{\partial \omega_{1} \omega_{k}} f \\
    \frac{\partial^{2}}{\partial \omega_{2} \omega_{1}} f & \frac{\partial^{2}}{\partial \omega_{2}^2} f& \dots & \frac{\partial^{2}}{\partial \omega_{2} \omega_{k}} f \\
    \vdots & \vdots & \ddots & \vdots \\
    \frac{\partial^{2}}{\partial \omega_{k} \omega_{1}} f & \frac{\partial^{2}}{\partial \omega_{k} \omega_{2}} f & \dots & \frac{\partial^{2}}{\partial \omega_{k}^2} f
\end{bmatrix}
\end{equation}


We give the first-order partial derivatives for each term of $f$ individually, this allows the formation of the gradient vector $\frac{\partial}{\partial \omega}(f)$. The function $\Psi^{n}(x)$ refers to the polygama function, where $n$ is the $n_{th}$ derivative of $\log \Gamma(x)$. 

\begin{eqnarray}
\frac{\partial T_{1}}{\partial \omega_{i}} &=& \Psi^{0}(\omega_{1}+...+\omega_{k}) \\
\frac{\partial T_{2}}{\partial \omega_{i}} &=& - \sum_{j=1}^{M} \sum_{i=1}^{k} \Psi^{(0)}(\omega_{i} + Y_{j}(i)) \\
\frac{\partial T_{3}}{\partial \omega_{i}} &=& \sum_{j=1}^{M} \Psi^{(0)}(\omega_{i} + Y_{j}(i)) \\
\frac{\partial T_{4}}{\partial \omega_{i}} &=& -M (\Psi^{(0)}(\omega_{i}))
\end{eqnarray}

The second order partial derivatives are:

\begin{eqnarray}
\frac{\partial^{2} T_{1}}{\partial \omega_{i}\ \omega_{j}} &=& \frac{\partial^{2} T_{1}}{\partial \omega_{i}^{2}} = M \Psi^{(1)} (\sum_{i=1}^{k}(\omega_{i})) \\
\frac{\partial^{2} T_{2}}{\partial \omega_{i}\ \omega_{j}} &=& \frac{\partial^{2} T_{2}}{\partial \omega_{i}^{2}} = - \sum_{j=1}^{M} \sum_{i=1}^{k} \Psi^{(1)}(\omega_{i} + Y_{j}(i)) \\
\frac{\partial^{2} T_{3}}{\partial \omega_{i}^{2}} &=& \sum_{j=1}^{M} \Psi^{(1)}(\omega_{i} + Y_{j}(i)) \\
\frac{\partial^{2} T_{3}}{\partial \omega_{i}\ \omega_{j}} &=& 0 \\
\frac{\partial^{2} T_{4}}{\partial \omega_{i}^{2}} &=& - M (\Psi^{(1)}(\omega_{i})) \\
\frac{\partial^{2} T_{4}}{\partial \omega_{i}\ \omega_{j}} &=& 0
\end{eqnarray}

\newpage
\section{Visual representation of the modified hue distributions} \label{App:AppendixE}

In the final implemenation of the colour model the hue component of the observed pixels was transformed as follows:

\[h_{new} = (cos(h_{original}),\ sin(h_{original}))\]

Figure \ref{fig:hue_distribution} shows the spread of the hues for each colour category along with the unit circle. \newline

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/HSV_dist.png}
\captionof{figure}{Spread of hues for each colour category after pixel transformation.}
\label{fig:hue_distribution}
\end{minipage}}
\hfill
\newline

The means for the transformed data points are calculated and shown as lines projecting outwards. The individual data points for each colour category are too plotted.

