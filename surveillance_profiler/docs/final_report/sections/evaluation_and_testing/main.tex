\subsection{Correctness testing}

The body model was tested on its capability of classifying colour regions with the use of confusion matrices. The number of colour categories that the model was able to classify was limited by the amount of attainable training data. For the testing phase, the amount of training data obtained, allowed for the classification of nine colour categories. Figure \ref{fig:test_body} depicts one of 114 plots generated for the construction of the confusion matrices.

\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/torso_classification.png}
\captionof{figure}{Summary plot showing assignments of torso component score with other related information on a sample image frame.}
\label{fig:test_body}
\end{minipage}
\hfill
\newline

The image in figure \ref{fig:test_body} shows a plot divided into four columns. Column one shows the original image with the \acrshort{roi} surrounding the body and it's associated mask (obtained from background subtraction). Column two shows the component score (equation (\ref{comp_score})) for a given colour. We see that the query for colour blue yielded the highest score (-513.22), thus resulting in an assignment of blue to the torso. Columns three and four show the observed histogram as well as the prior's obtained from the training of the body model parameters (equation (\ref{opt_function})).

\newpage
The torso classification was tested using a total of 114 images. Table 8 below summarises the results:

\begin{tabularx}{\linewidth}{|c|cccc|}
\hline
      & True positives & False negatives & False positives & True negatives\\
\hline
\rowcolor{Gray}
Black & 19 & 0 & 51 & 47 \\
Blue & 15 & 8 & 0 & 90 \\
\rowcolor{Gray}
Brown & 0 & 12 & 0 & 102 \\
Gray & 0 & 17 & 0 & 97  \\
\rowcolor{Gray}
Green & 1 & 3 & 3 & 110 \\
Purple & 4 & 11 & 0 & 99 \\
\rowcolor{Gray}
Red & 8 & 1 & 1 & 95 \\
White & 13 & 3 & 6 & 90  \\
\hline
Average & 7.5 & 6.8 & 7.6 & 91.25 \\
\hline
\caption{Collection of nine confusion matrices for labelled torso's.}
\label{tab:confusion_torso}
\end{tabularx}

Each row of table \ref{tab:confusion_torso} $r_{i}$, represents the confusion matrix for colour $i$. True positives for colour category $i$ refers to the number of torso's labelled $i$ that have been correctly classified as colour $i$. False negatives are those torso's labelled colour $i$ but not correctly classified as colour $i$. False negatives are torso's other than $i$ that have been incorrectly classified as colour $i$. Lastly, true negatives are those torso colours correctly classified not being colour $i$. The last row of the matrix represents the average of each column. \newline

We see that the system performs well for colours black, blue, red and white (ratio of true positives to false negatives). But very badly for colour's brown, gray, green and purple. We given the precision, recall and accuracy for the classifications of torso colours.

\begin{tabularx}{\linewidth}{|c|c|}
\hline
      & Torso \\
\hline
\rowcolor{Gray}
Precision & 0.49 \\
Recall & 0.52 \\
\rowcolor{Gray}
Accuracy & 0.87\\
\hline
\caption{Derivations from the last row of the confusion matrix.}
\label{tab:confusion_torso}
\end{tabularx}

\subsection{Performance testing}

A summary of the running times for different stages of the back-end allows the identification of bottlenecks. We evaluated the performance of 1) rate of body detection and time taken to store image frames of the connector component, and 2) rate at which frames are scored for the front-end.

\begin{tabularx}{\linewidth}{|c|ccc|}
\hline
      & Control & Standard HOG & Modified HOG \\
\hline
\rowcolor{Gray}
Contours & NA & NA & 0.45s \\
Isolating regions & NA & NA & 2.03s \\
\rowcolor{Gray}
Detection & NA & 44.32s & 3.06s \\
Mask & NA & 10.36s & 11.92s \\
\hline
Total running time(s) & 19.2 & 71.02 & 35.05 \\
Frame rate (f/s) & 76.92 & 20.00 & 50.00 \\
\hline
\caption{Performance results for the detection rate of the connector component on a total of 1414 image frames.}
\label{tab:performance_connector}
\end{tabularx}

Table \ref{tab:performance_connector} summarises the performance of the two approaches used for this system. We also use a control where no detection is performed (shown by the first column). The standard HOG approach refers to applying the HOG descriptor on the whole image. The modified HOG approach refers to applying the HOG descriptor on regions that have motion. The rows give the running times for the individual steps associated with detection. Contours and isolating regions are steps that are necessary for identifying the regions of motion from the mask. \footnote{OpenCV2's findContours (URL: \texttt{http://docs.opencv.org/3.1.0/d4/d73/tutorial\_py\_contours\_begin.html)} method identifies \acrshort{roi}'s surrounding connected components of the mask. One such contour consists of a single \acrshort{roi}. Isolating regions is a class implemented in Python that finds an outer \acrshort{roi} that surrounds a group of contours. A threshold on geometric distance between contours ensures that motion detected on opposite sides of an image are registered as two separate \acrshort{roi}'s.} Mask refers to the time taken to generate the mask used for the tiered detection algorithm. \newline

We can see a big speed up with the use of the modified HOG descriptor when compared to the standard HOG descriptor, the time taken to perform detection greatly reduces (44.2s to 3.06s with an overhead of 2.48s). \newline

\newpage
Table \ref{tab:performance_score} summarizes the results the rate at which frames are scored. \newline

\begin{tabularx}{\linewidth}{|c|c|}
\hline
      & Scoring algorithm \\
\hline
\rowcolor{Gray}
Generating observed states & 19.8 \\
KDTree for neighbors & 0.58 \\
\rowcolor{Gray}
Database & 0.49 \\
Calculations & 0.6 \\
\rowcolor{Gray}
Initialization & 1.8\\
\hline
Total running time (s) & 23.8 \\
Frame rate (f/s) & 16.12 \\
\hline
\caption{Performance results for the scoring a total of 380 frames. Images are scored when a query is sent to the back-end from the input view.}
\label{tab:performance_score}
\end{tabularx}

Frames are scored at a rate of $16.12 f/s$. Summarised are the individual times taken for different stages of the scoring algorithm. We see that even with the use of \texttt{discodb} for pixel category assignments, the bottleneck of the algorithm is in generating the observed states of the body model (19.9s of the total time to run 23.8s). This suggests the system will take a substantial amount of time for queries spanning a large time and date range.

\subsection{Testing of the colour model}

The colour model is a key component of the body model and is used to generate observed states $Y_{i} : i \in [1, N]$. The colour model was tested with the use of 9 test subjects. Subjects were asked to assign colour categories to randomly generated colour coordinates. Their answers were compared to the assignments used by the colour model. In total three sets of 81 randomly generated colours were used (three test subjects assigned colours to each set). To account for variability in assignments between the test subjects we used an \say{agreement index}. A score \footnote{Note a score in this chapter is not the score assigned to an image frame equation (\ref{the_score}), but rather an indication of how many test subjects the colour model was able to agree with.} out of 3 was given to a colour block. The colour model performed well if it's colour assignment for a particular colour block agrees with the assignment's of all the test subjects. A score of 3 is given if the model and all three test subjects assign the same colour to a block. If the colour model can't agree with any of the test subjects, a score of 0 is given to that particular block. \newline

\newpage
\begin{tabularx}{\linewidth}{|c|c|}
\hline
      & Agreement index \\
\hline
\rowcolor{Gray}
Set 1 & 0.72 \\
Set 2 & 0.74 \\
\rowcolor{Gray}
Set 3 & 0.81 \\
Average index & 0.75 \\
\hline
\caption{Results of the colour assignment experiment for three sets of 81 randomly generated colour blocks. The agreement index is calculated as a ratio of correctly assigned colour categories according to the agreement of the test subjects.}
\label{tab:performance_score}
\end{tabularx}


Using this approach gives an indication of how well the colour model is able to assign colours to pixel coordinates in the HSV coordinate space. An agreement index close to 1.0 suggests the model makes accurate decisions about colour category assignments. Since the average agreement index 0.75, this suggests that the colour model performs reasonably well.

\subsection{Static tests}

Static tests were used to evaluate the correctness of the image frame database and inference algorithm for the hidden states $\pi_{i}$. The image database was tested by performing queries involving varying date, times and camera subsets. This ensured that the stored procedure always returned the correct image frames associated with a query to the \acrshort{sscp}. Tests for the inference algorithm ensured that the sum of all the components is always one. The static tests covered borderline cases where components of either the $Y_{i}$ histogram or $\pi_{i}$ had values close to zero. Negative values for the hidden variables results in incorrect scores. \footnote{See the numerators of the components for equation (\ref{infer_derived}).}.

