
This section introduces key \acrshort{sscp} software design features. This includes an overall description of the design followed by details of the front-end and back-end (aspects of the back-end requiring mathematical background are deferred).


\subsection{System overview}

We begin by looking at the users of the system. The users are to be video operators and the intended use is to be able to perform attribute-based video searches on stored footage. We also want the system to be secure and most importantly we want the system to be able to access the \acrshort{DVR} in the \acrshort{csl}.

We illustrate the full system and its components as below:

\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/sys_overview.png}
\captionof{figure}{Overview of the system.}
\label{fig:sys_overview}
\end{minipage}
\hfill
\newline

The \acrshort{sscp} comprises five components and facilitates two types of users. \footnote{Note that an arbitrary amount of video operators may have access to the system, but there is only one superuser.} We discuss the role
of each component and their interactions.

\newpage
\subsubsection{Structure of the query: input profile}

The profiler needs to be able to score a frame based on a user's input query. In this section, we define what constitutes a valid query. The attributes associated with a possible query are the colours of the various body parts. Included are a date and time range and camera subset. Table \ref{input_profile} summarizes encoding of the input profile. For reasons that will become evident in chapter four, we restrict the number of colour categories that can be searched for each body part to twelve. \footnote{Namely: white, gray, black, yellow, orange, pink, red, green, blue, purple, brown and beige.}

\begin{tabularx}{\linewidth}{lX}

\textbf{Attribute} & \textbf{Description} \\
\hline 
\texttt{hair\_colour} & The colour of the target's head. \\
\texttt{torso\_colour} & The colour of the target's torso. \\
\texttt{legs\_colour} & The colour of the target's legs. \\
\texttt{start\_date} & The start date of the query. \\
\texttt{end\_date} & The end date of the query. \\
\texttt{start\_time} & The start time for the start date. \\
\texttt{end\_time} & The end time for the end date. \\
\label{input_profile}\\
\caption{Depicts the structure of the input profile. The assignments to the date and time and camera subset attributes result in an initial set of image frames. Matches in this set are found using the assigments for attributes \texttt{hair\_colour}, \texttt{torso\_colour} and \texttt{legs\_colour}.}
\end{tabularx} 

Each component of the \acrshort{sscp} regarding the front-end is now discussed.

\subsection{Front-end}

\subsection{Views}
The front-end needs to be easy to use and accessible. This means the \acrshort{sscp} needs a way to abstract the complexities of the profiling task from the user experience. With this intention in mind, we discuss the design of the front end by first discussing the individual views and then the design paradigm.

\subsection*{Administration}

\begin{figure}[h]
 
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=0.9\linewidth, height=3.5cm]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/adminpage.png} 
\caption{View facilitating the login of the superuser.}
\label{fig:admin_li}
\end{subfigure}
\begin{subfigure}{0.6\textwidth}
\includegraphics[width=0.9\linewidth, height=3.5cm]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/adduser.png}
\caption{View showing currently active video operators.}
\label{fig:admin_add}
\end{subfigure}
 
\caption{Views showing the functionality of the administration component within the front-end.}
\label{fig:image2}
\end{figure}

Administration comprises three views, two of which are shown by the images in figure's \ref{fig:admin_li} and \ref{fig:admin_add}. The administration is provided as built-in functionality by Django. Administration access by a superuser is configured with Django's terminal interface. Once logged in, the superuser is able to manage login credentials of video operators. Figure \ref{fig:admin_li} illustrates the login view and figure \ref{fig:admin_add} shows where the superuser can view currently assigned video operators. An additional view allows for operators to be added or removed. We see that three users are listed, \say{nicholasjakins}, \say{Andrew} and \say{Bob}. Only one user has staff access and assigned the role of superuser (nicholasjakins). The default encryption scheme provided by Django makes sure that user credentials are secure. \footnote{The encryption scheme used is PBKDF2: https://en.wikipedia.org/wiki/PBKDF2.}

\subsection*{Video operator login}

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/signinview.png}
\captionof{figure}{View for where the video operator can login and begin performing video based searches.}
\label{fig:login_VO}
\end{minipage}}
\hfill
\newline

Login comprises one view as shown by the image figure \ref{fig:login_VO}. A user can login under the condition that a video operator has been added to the system by the superuser. Video operators login by providing an assigned username and password and are notified if incorrect credentials are used.

\subsection*{User query}

\begin{figure}[h]
 
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=0.9\linewidth, height=5cm]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/colour_picker.png} 
\caption{View facilitating the configuration of the input profile \ref{input_profile}.}
\label{fig:input_profile_view}
\end{subfigure}
\begin{subfigure}{0.6\textwidth}
\includegraphics[width=0.9\linewidth, height=5cm]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/region_selector.png}
\caption{Modal allowing the video operator to select a region within a camera view for a search.}
\label{fig:region_selector}
\end{subfigure}
\caption{View and modal showing the interactions allowing the configuration of the colour assignments and camera search region's.}
\label{fig:image2}
\end{figure}



\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/date_time.png}
\captionof{figure}{Modal allowing the configuration of the date and time range.}
\label{fig:modal_date_time}
\end{minipage}}
\hfill
\newline

The input view is responsible for managing user-defined queries, this is where the video operator is able to configure the input profile (see table \ref{input_profile}). Included with the input view are multiple modals with which the input profile is generated. In addition to the attributes for the input profile in table \ref{input_profile}, the video operator may also specify a region within each camera view as shown in figure \ref{fig:region_selector}. This region is used to further refine the search. The modal shown in figure \ref{fig:modal_date_time} allows the configuration of the date and time ranges.

\subsection*{Results}

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/results_page.png}
\captionof{figure}{Results from a search, shown on the right of the screenshot. On the left column a summary of the query is given.}
\label{fig:results_page}
\end{minipage}}
\hfill
\newline

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/meta_modal.png}
\captionof{figure}{Modal showing additional information obtained from the \acrshort{PGM}'s described in chapter 4.}
\label{fig:meta}
\end{minipage}}
\hfill
\newline


This view displays the results generated by the back-end in response to a user query (figure \ref{fig:results_page}). The results in the form of images are structured as follows. A container is created for each camera, and a tab within each container represents images for a given date. Each tab comprises a carousel for the images pertaining to that specific date. Situated below each carousel is a slider. This slider can be used to narrow the results for a particular image set. A column on the left of the page summarises the instance of the input profile and finally a modal gives additional details regarding the analysis of a particular frame (see the image shown by figure \ref{fig:meta}).

\subsection{Design paradigm}


The design of the front-end is in the form of a web application. The open source libraries associated with frameworks such as \texttt{node} \footnote{URL: \texttt{https://nodejs.org/en/}} and \texttt{Django} \footnote{URL: \texttt{https://www.djangoproject.com/}} allow for a flexible design. It was decided to use the Django web framework along with AngularJS. Apart from AngularJS being very well documented, the use of directives for manipulating the content of the view provides a means for a robust design. One convenient feature among others that Django offered was a built-in administrative page.

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/frontend.png}
\captionof{figure}{Design paradigm for the front-end (the arrows between the components indicate points of communication).}
\label{fig:des_paradigm}
\end{minipage}}
\hfill
\newline

Figure \ref{fig:des_paradigm} shows the design of the web-application. The front-end consists of three views written in HTML, along with three controllers written in JavaScript. Controllers serve as intermediaries between the views and the server back-end. This works well to support abstracting the back-end processes from the frontend. Changes in a view are monitored by an associated controller and if necessary a \texttt{POST} request is sent to the server. The server then renders an appropriate response. \newline

From here we explore the software design component of the backend. 

\subsection{Back-end}

Both backend subcomponents (being the profiler and connector) were written in Python. This was due to the availability of many open-source libraries. Libraries such as: \texttt{OpenCV2}, \texttt{Scipy} and \texttt{Numpy} facilitated human detection and support for a variety of statistical distributions (chapter 4). Additionally, since the Django web framework is written in Python, writing the backend in Python too made sense from a compatibility point of view. Discussed next is the role of the connector component.

\subsubsection{Connector component}

The connector component is responsible for three tasks: 1) establishing a connection to the \acrshort{DVR}, 2) reading frames that have been written to a filesystem and 3) running a human detection algorithm on each frame before storing the frames into a database. By performing a human detecting algorithm on incoming frames the system avoids storing images that aren't of value to a possible query (images that don't contain at least one person is not useful to the system). The constraints of the built-in software provided by the \acrshort{DVR} in the \acrshort{csl} made the design of the connector component a challenge. It was discovered after some time that the ActiveX plugin used for viewing the feed required the use of a 32-bit Internet Explorer browser. This was a problem since the rest of the system is built on a machine running the Macintosh operating system. The figure below illustrates the component's setup.

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/connector_component.png}
\captionof{figure}{Diagram illustrating the integration of the connector component with the \acrshort{sscp}.}
\label{fig:connector_component}
\end{minipage}}
\hfill
\newline

Figure \ref{fig:connector_component} depicts the integration of the connector component with the \acrshort{sscp}. The full connector component comprises of a \acrshort{VM} (The virtual machine was created and maintained using VirtualBox.\footnote{Where other \acrshort{VM} managing tools exist such as \texttt{VMWare fusion}, Virtual box is open-source and commonly used.}), a shared directory and two python scripts. Once the camera system is configured with the network \footnote{URL for local access: \texttt{https://webcam.cs.sun.ac.za}} the video feed is accessible with the use of a web-interface. This interface is only viewable within an Internet Explorer browser (32-bit). Initially, the footage is interactively stored on the virtual machine's local file system. This happens when a user clicks on a record button from the IE browser. Communication is then established between the host machine (Macintosh V 10.11.1) and the virtual machine via a shared folder. This allows a Python script on the virtual machine to analyse video files and write frames to a location accessible by the host. A Python script on the host machine facilitates the analysis of the shared file storing image frames into the database as they are being written. \newline

 Image frames comply with the following naming convention:

\[\text{\say{[date]\_[cam\_num][time]\_[frame\_num].png}}\]

The \texttt{frame\_num} portion of the file name is how many frames have been read in so far. This is useful in determining the time elapsed since the video started being analysed.

\[t_{e} = time + \frac{frame\_num}{r}\]

Where $t_{e}$ is the time of capture and $r$ is the rate at which video frames are analysed.


\subsubsection{Tiered detection algorithm}

Being able to detect a moving body within an image frame is an important task of the connector. To do so we make use of the approach proposed in (\cite{PAS}). We classify a \acrshort{roi} surrounding a person when it correctly meets three criteria: A) the \acrshort{roi} contains a high ratio of pixels containing motion, B) the pixels associated with the \acrshort{roi} exhibit the shape of a person and C) the \acrshort{roi} fits into the ground plane of the scene. Note, the system assumes that cameras are not motion triggered.

\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/detection.png}
\captionof{figure}{Illustration of the three tiered detection algorithm.}
\label{fig:detection}
\end{minipage}}
\hfill
\newline

Figure \ref{fig:detection} diagrammatically shows three phases involved with the detection for a sample image frame. We assign the term false positives to image frames or regions that are incorrectly classified as containing one or more persons. Discussed below are the approaches for each step individually.

\subsubsection*{Background subtractor}

The first phase is to isolate regions pertaining to motion and is depicted by the arrow marked criteria A on the input image. Authors P. KaewTraKulPong and R. Bowden [5] show how to obtain pixels exhibiting motion for image frames. Their method uses an update rule that generates a mask for consecutive frames where white pixels are pixels exhibiting motion. Isolated regions of the image are passed on as parameters to the next phase which checks the image against criteria's B and C. The Python package OpenCV2, provides an implementation of the background subtraction algorithm presented [5].


\subsubsection*{HOG descriptor}

As proposed in [1] the problem of locating people in image frames can be done with the use of a HOG descriptor. Using a sliding window and applying a gradient kernel \footnote{URL: \texttt{https://en.wikipedia.org/wiki/Kernel\_(image\_processing)}} to an image, the algorithm vectorizes the pixel values and classifies a match using a trained support vector machine. We depict the use of the HOG descriptor with the arrow labelled criteria B. This phase eliminates regions that pertain to moving objects that don't resemble the shape of a human. We make use of OpenCV2's already implemented HOG descriptor and trained support vector machine for pedestrian detection. \footnote{URL: \texttt{http://www.pyimagesearch.com/2015/11/09/pedestrian-detection-opencv/}}

\subsubsection{Homography of the foot to head plane}

In this final step, we use the scene homography captured by the camera to further eliminate false positives (represented by the arrow marked criteria C). In an environment such as the \acrshort{csl} this step is rarely needed since most of the time motion pertains to a human. Whereas in an outdoor environment, one might expect moving objects other than people, such as cars or animals. \newline

This step makes use of a so-called homography matrix. A homography matrix is a well-known construct in computer vision that acts as a transformation taking points from one image plane to another. For this system, we are interested in a mapping from feet positions to head positions. Assuming a flat surface for the floor, we make an assumption. All feet coordinates lie on the foot plane and similarly all corresponding head coordinates lie on a head plane. Assuming a true positive: comparing an estimated head position to a projected head position (a projection from the foot plane to the head plane) will result in a small margin of error. Assuming a false positive this margin of error will be greater. A perspective error between the two heights can then be used to eliminate false positives. \newline

The approximation of the homography H from the foot plane to the head plane is done using the basic direct linear transform algorithm [2]. Where Thornton uses a more robust approach which relies on approximating an homology [11], we settle with using a homography. It is implied that an approximated homology be used for deployment of the \acrshort{sscp} where the environment is expected to be busier.

\subsubsection{Databases}

Managing large amounts of data, mostly in the form of images is an essential element of this project. The \acrshort{sscp} uses one database for the storing of image frames, \acrshort{csl} \acrshort{DVR}. Two more databases are needed for storing data used by the offline processes of the \acrshort{sscp}, COLOUR SURVEY and PARAMETERS. These processes will be discussed in chapter 4.


\centerline{
\begin{minipage}{0.8\textwidth}
\includegraphics[width=\textwidth]{/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/final_report/images/database_overview.png}
\captionof{figure}{Component layout of the databases used. The database used by the \acrshort{sscp} contains three entitites, image frames, model states and bounding boxes. The databases used by the offline processes each contain one entity.}
\label{fig:db_overview}
\end{minipage}}
\hfill
\newline

Various databases were considered for this project, namely, Django's built-in database system using models, MySQL and PostgreSQL. The factors that were taken into consideration included: compatibility with Python, scalability, and ease of use. The richness in data types available with PostgreSQL as opposed to MySql seemed favourable. Additionally, the accessibility of Django's built-in databases system seemed ideal from a designer's perspective. One downside with the use
of Django's models was, however, that the scope of the database was bound to that specific web application. Since two separate web applications were implemented (see the colour survey section in chapter 4), it made more sense to use a more centralised database system. Although it can be argued all three options would have worked, I decided on                   the use of PostgreSQL. Having already had experience with MySQL I was interested in expanding my skill set with a new database system that seemed promising. \newline

We discuss the entities associated with each of the three databases. \newline

\subsubsection*{Image frames}

This entity manages the storage of the video frames taken from the \acrshort{csl}. When footage from the surveillance cameras is analysed by the connector class, metadata relating to each image frame is stored.

\newpage
\begin{tabularx}{\linewidth}{lX}
\textbf{Attribute} & \textbf{Description} \\
\hline 
\texttt{path} &  \texttt{Primary key}: Absolute path on the host machine to the image location. \\
\texttt{view\_date} & The date the image frame was viewed, format: YYYYMMDD. \\
\texttt{view\_time} & The time the image frame was viewed, format: HHMMSS. \\
\texttt{cam\_number} & The camera number that the image was taken from. \\
\caption{List of attributes contained within the image frame entity.}\label{image_entity}\\
\end{tabularx} 

For database efficiency, image frames are stored using their absolute path (as opposed to storing the images themselves). Storing the camera number is important when performing the scoring or moving body detection (for example, when applying criteria C from the tiered detection algorithm it is important to know which homography matrix to use).

\subsubsection*{Profiler specific information}

The entities described here were designed for the improvement of efficiency. These entities store information supporting the procedure of scoring. This information includes \acrshort{roi}'s surrounding a body found by the connector component and metadata associated with the scoring of an image frame (see chapter 4).


\subsubsubsection*{Bounding box}

Can also be referred to as a rectangle this entity stores the location of a detected person in an image frame. Storing the \acrshort{roi}'s surrounding a person is advantageous because it means a decrease in the time taken for the overall scoring process (fewer pixels to process). Once the \acrshort{roi} is stored for an image frame, moving body detection can be accomplished by a query to the database. \newline


\begin{tabularx}{\linewidth}{lX}

 \textbf{Type} & \textbf{Description} \\
\hline 
\texttt{path} & \texttt{Primary Key}: Absolute path on the host machine to the image location. \\
\texttt{boxes} & An array of \acrshort{roi}'s, type \texttt{bounding\_box}. \\
\caption{Attributes for the bounding box entity.}\label{tab:1}\\
\end{tabularx} 

PostgreSQL allows for the creation of custom types. The type \texttt{bounding\_box} stores bounding boxes as \acrshort{roi}s for each image frame pertaining to a moving person.

\subsubsubsection*{Scoring information}

This entity stores the data used in the scoring process. We refer the reader to chapter (4) for the context of the attributes relating to this entity.
\newpage
\begin{tabularx}{\linewidth}{lX}
 \textbf{Attribute} & \textbf{Description} \\
\hline 
\texttt{path} & \texttt{Primary key}: Absolute path on the host machine to the image location. \\
\texttt{bounding\_box} &  \acrshort{roi} defined by a 4-tuple (x, y, w, h): x and y mark the coordinates on the image for the top left corner of the box, w and h mark the width and height of the \acrshort{roi}.\\
\texttt{head\_colour\_histogram} &  A double array of values storing the histogram of colour categories with the region surrounding the head. \\
\texttt{torso\_colour\_histogram} & A double array of values storing the histogram of colour categories with the region surrounding the torso. \\
\texttt{legs\_colour\_histogram} & A double array of values storing the histogram of colour categories with the region surrounding the legs. \\
\texttt{score} & The real valued score associated with the image frame. \\
\caption{List and description of attributes for the model states.}\label{tab:1}\\
\end{tabularx} 

\subsubsection{Colour survey} \label{col_database}


As will be detailed in chapter 4, the system relies on the use of a colour survey (chapter 4). The goal of the survey is to allow anonymous users to assign colour categories to randomly generated HSV values. To manage the data of colour allocations by test subjects the below entity was used.

\begin{tabularx}{\linewidth}{lX}

 \textbf{Type} & \textbf{Description} \\
\hline 
\texttt{hsv} & Three dimensional coordinate in the HSV colour space (appendix \ref{App:AppendixA}), defined as a 3-tuple (h, s, v). \\

\caption{Description of the custom HSV type.}\label{tab:1}\\
\end{tabularx} 

Defining the custom type HSV allows for easy storage and querying. The alternative would have been to assign separate attributes for each pixel component.

\begin{tabularx}{\linewidth}{lX}

\textbf{Attribute} & \textbf{Description} \\
\hline 
\texttt{user\_name} & \texttt{Primary key}: Username: used to anonymously distinguish between survey candidates. \\
\texttt{category} & Colour category assigned by survey candidate. \\
\texttt{color} &  HSV coordinate as type \texttt{hsv}. \\
\caption{Description of attributes used by the colour assignments entity.}\label{tab:1}\\
\end{tabularx} 

Queries from all survey candidates on each colour category allow for analysis of all possible HSV values that represent that colour category. The database allows for entries from different usernames.


\begin{linktext}
This concludes the full design from the software perspective. The component we have deferred to the next section includes the introduction to \acrshort{PGM}'s and a full description of the \acrshort{PGM}'s used in this project. Chapter 4 will conclude with a description of the scoring algorithm used to rank image frames. 
\end{linktext}






