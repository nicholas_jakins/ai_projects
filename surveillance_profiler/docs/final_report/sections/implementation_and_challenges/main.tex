\subsection{Connection to the \acrshort{csl} \acrshort{DVR}}

One of the biggest challenges during the implementation phase was to maintain a connection to the live stream of the \acrshort{DVR}. This remained a problem, and eventually, we had to make to with a workaround. To get image frames into the database required seemingly unnecessary user intervention and the need for a Windows 7 virtual machine. This section describes the particular challenges associated with the connection component to the \acrshort{DVR} in the \acrshort{csl} and how these challenges were dealt with. \newline


The first step was to register the \acrshort{DVR} on the local network: this was achieved by assigning it an IP address. Then to access the feed remotely two options were available: either access to a provided interface via a 32-bit IE web browser, or remote access using a downloaded software package provided by the \acrshort{DVR} developers. \footnote{URL: \texttt{Integrated Remote Station:URL,http://www.cctvcamerapros.com/Remote-Camera-Monitoring-s/285.htm}} Both access options required a windows machine to run on and only one seemed feasible since the provided desktop application did not comply with the \acrshort{csl} network configuration. \newline

Access to the live feed via the IE web browser only facilitated viewing with an option to record. \footnote{The interface used an ActiveX binary.} Requiring the video operator to record on the virtual machine in order to store the images into the database is counter-intuitive. In order to explore a solution that gave automation, the following approaches were considered and trialled. \newline

\subsubsection*{Screenshot grabber for viewing}

Software tools such as Snagit \footnote{URL: \texttt{http ://discover.techsmith.com/try-snagit/}} and Greenshot \footnote{URL: \texttt{https ://sourceforge.net/directory/os:mac/?q=greenshot}} provide functionality for taking screen shots at regular intervals. Using this tool to seemingly grab frames by analysing the video feed from the browser would mean a solution that required no interaction. However in practice this option was not feasible, scalability is a major concern. With each new camera to monitor the resolution of the screenshot degrades.

\subsubsection*{An open-source video surveillance software tool}

Using an alternative tool to the software provided by the \acrshort{DVR} seemed favourable. One such tool considered in this project was a software package called \texttt{ZoneMinder}. \footnote{URL: \texttt{https://zoneminder.com/}} Designed to run in an Ubuntu environment, open source and freedom to manage the images in real-time, \texttt{ZoneMinder} seemed a promising alternative. Unfortunately, conflicts in the software and hardware meant that \texttt{ZoneMinder} wasn't going to work, \texttt{ZoneMinder} did not provide support for surveillance software needing IE to view the footage. \newline

The core of the \acrshort{sscp} is to be able to perform the scoring of frames based on an input query and not to design a system that provided an efficient way to connect to the surveillance. As such, it was decided to not explore further options and unless time permitted, attempt to further automate the connection component.

\subsection{Efficiency}

Ensuring that the final implementation and design of the \acrshort{sscp} were efficient is, of course, crucial. An efficient scoring algorithm would mean video operators would have to wait for a smaller period of time to see search results. Moreover, speedup on aspects such as the training of the parameters would mean the use of a larger training set in a shorter amount of time. Consequently, the following approaches were taken during the course of the implementation phase to improve efficiency.

\subsubsection{Caching of log factorials}

Term 1 from equation (\ref{comp_score}) involved having to compute the log factorial of the observed state $Y_{i}$. Since each histogram is normalised to sum to 100, the values for these terms can be computed ahead of time. This saved on repeated computation.

\subsubsection{Detection before scoring}

The body model relies on observations $Y_{i}$ for each body part. Furthermore, these can only be computed once \acrshort{roi}'s are obtained using the tiered detection algorithm. By performing the detection ahead of time (when the frames are stored in the database with the connector class) we were able to improve on time taken to perform the rest of the scoring.

\subsubsection{Faster generation of the observed variables for the body model}

One of the biggest bottlenecks of the system is the generation of the observed states within an image frame. Generating states $Y_{i}$ from the body model relies on the inference of the hidden variables $H_{1},\ ...,\ H_{k}$ of the colour model (\ref{colour_model}).The original approach involved dynamically inferring the hidden states for each pixel value. During the initial testing phase, the scoring of 280 image frames took over 10 minutes. There were two major issues with this approach: 1) dynamic inference is too computationally expensive for many consecutive images and 2) this approach was re-assigning colour categories to previously calculated pixels at each iteration of the scoring algorithm. The following measures were taken in an attempt to overcome these issues. \newline

As an alternative approach to dynamic inference, it was decided to cache the colour category assignments to all possible HSV coordinates. Mapping objects such as \texttt{discodb} \footnote{URL: \texttt{http://discodb.readthedocs.io/en/latest/}} provide the means for fast lookups for a very large set of key-value pairs. \texttt{discodb} is an open source package for Python, serving as an efficient immutable mapping object. It is implemented in C and makes it possible to search with constant key lookups. Populating the discodb mapping object, of course, takes a very long time. \footnote{With \texttt{OpenCV2}'s representation of the HSV coordintates, approximately 11 million hidden variables of the colour model would need to be inferred.} However, this is only a once off overhead since the once populated the database is writable to a file format.

\subsection{Novelties}

There were aspects of Thornton's model's structure and algorithms that were not fully detailed in the paper. This proved to be a challenging aspect of the implementation and design, requiring custom (and sometimes suboptimal) solutions. Here we discuss where those gray areas arose and the approaches taken to dealing with them.

\subsubsection*{Choice of coordinate space for pixel observations in the colour model}

As already discussed in chapter 4, one of the main challenges in the design of the colour model was the choice of the colour space. There were two options for the coordinate space: RGB and HSV, using the HSV colour space is desired. However, the problem with using the HSV coordinate space is how to effectively represent the distribution states assigned to $\theta$. Thornton accounts for the circular property of the hue component by defining a difference operator (\cite{CHAP}) equation (8.3) and modified normalisation constant \cite{CHAP}, equation (8.2). It was not clear in the text how this normalisation constant was approximated. A solution was found that worked well with the HSV coordinate space by defining a new coordinate space and transformation equation (\ref{transformation}). Listed are two alternative and suboptimal solutions:

\begin{itemize}
\item RGB colour space: using the RGB colour space for pixel observations meant not having to approximate a new normalisation constant. However, using the RGB coordinate space also meant that the model was going to be susceptible to lighting and shadows.
\item HSV using VonMises: The VonMises distribution \footnote{URL: \texttt{https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.vonmises.html}} is a well-known distribution that works with circular variables such as the hue component of the HSV colour space. By separating out the hue component from the saturation and value we adjust the likelihood of observation for pixel $O_{i}$ to $P(O_{i}) = P_{vonmises}(O_{i})P_{multivariate}(O_{i})$. This form of likelihood estimation works when variables are independent. In practice making the assumption that the hue component was independent of the saturation and value was incorrect. As a result, the effectiveness of the model was questionable in the implementation phase.
\end{itemize}

\subsection*{Getting neighbourhoods from the partition distribution}

The scoring algorithm requires an implementation of a neighbourhood function. This is in order to obtain different approximations for the head, torso, and legs at each iteration (equation (\ref{comp_score})). Implementing a neighbourhood function was a challenge initially. However, it was found that by making use of a KDTree, accurate approximations of neighbours sampled from the $\hat{Z}$ distribution was achievable. The KDTree Python package from \texttt{scipy.spacial} \footnote{URL: \texttt{https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html}} provided a fast nearest neighbor lookup for N-dimensional vectors. By first generating N samples from the partition distribution the KDTree package sorts by spatial distance from the mean.



