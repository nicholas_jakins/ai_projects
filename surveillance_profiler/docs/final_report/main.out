\BOOKMARK [1][-]{section*.1}{Acronyms}{}% 1
\BOOKMARK [1][-]{section.1}{Introduction}{}% 2
\BOOKMARK [2][-]{subsection.1.1}{Outline}{section.1}% 3
\BOOKMARK [2][-]{subsection.1.2}{Approach}{section.1}% 4
\BOOKMARK [2][-]{subsection.1.3}{Scope}{section.1}% 5
\BOOKMARK [2][-]{subsection.1.4}{Structure}{section.1}% 6
\BOOKMARK [1][-]{section.2}{Requirements}{}% 7
\BOOKMARK [2][-]{subsection.2.1}{System outline}{section.2}% 8
\BOOKMARK [2][-]{subsection.2.2}{Front-end}{section.2}% 9
\BOOKMARK [3][-]{subsubsection.2.2.1}{Functional requirements}{subsection.2.2}% 10
\BOOKMARK [3][-]{subsubsection.2.2.2}{Functional requirements not implemented}{subsection.2.2}% 11
\BOOKMARK [3][-]{subsubsection.2.2.3}{Additional functionality implemented}{subsection.2.2}% 12
\BOOKMARK [2][-]{subsection.2.3}{Back-end}{section.2}% 13
\BOOKMARK [3][-]{subsubsection.2.3.1}{Person detection and body segmentation}{subsection.2.3}% 14
\BOOKMARK [3][-]{subsubsection.2.3.2}{Scoring of image frames}{subsection.2.3}% 15
\BOOKMARK [3][-]{subsubsection.2.3.3}{Connection to the csl DVR}{subsection.2.3}% 16
\BOOKMARK [3][-]{subsubsection.2.3.4}{Training of parameters}{subsection.2.3}% 17
\BOOKMARK [3][-]{subsubsection.2.3.5}{Database interaction}{subsection.2.3}% 18
\BOOKMARK [4][-]{subsubsubsection.2.3.5.1}{Retrieval}{subsubsection.2.3.5}% 19
\BOOKMARK [4][-]{subsubsubsection.2.3.5.2}{Storage}{subsubsection.2.3.5}% 20
\BOOKMARK [2][-]{subsection.2.4}{Performance requirements}{section.2}% 21
\BOOKMARK [1][-]{section.3}{Design}{}% 22
\BOOKMARK [2][-]{subsection.3.1}{System overview}{section.3}% 23
\BOOKMARK [3][-]{subsubsection.3.1.1}{Structure of the query: input profile}{subsection.3.1}% 24
\BOOKMARK [2][-]{subsection.3.2}{Front-end}{section.3}% 25
\BOOKMARK [2][-]{subsection.3.3}{Views}{section.3}% 26
\BOOKMARK [2][-]{subsection.3.4}{Design paradigm}{section.3}% 27
\BOOKMARK [2][-]{subsection.3.5}{Back-end}{section.3}% 28
\BOOKMARK [3][-]{subsubsection.3.5.1}{Connector component}{subsection.3.5}% 29
\BOOKMARK [3][-]{subsubsection.3.5.2}{Tiered detection algorithm}{subsection.3.5}% 30
\BOOKMARK [3][-]{subsubsection.3.5.3}{Homography of the foot to head plane}{subsection.3.5}% 31
\BOOKMARK [3][-]{subsubsection.3.5.4}{Databases}{subsection.3.5}% 32
\BOOKMARK [3][-]{subsubsection.3.5.5}{Colour survey}{subsection.3.5}% 33
\BOOKMARK [1][-]{section.4}{Profiling and modeling}{}% 34
\BOOKMARK [2][-]{subsection.4.1}{Need for PGM's}{section.4}% 35
\BOOKMARK [2][-]{subsection.4.2}{The models}{section.4}% 36
\BOOKMARK [3][-]{subsubsection.4.2.1}{The colour model}{subsection.4.2}% 37
\BOOKMARK [2][-]{subsection.4.3}{Estimating the state for the parameter variable}{section.4}% 38
\BOOKMARK [3][-]{subsubsection.4.3.1}{Body model}{subsection.4.3}% 39
\BOOKMARK [3][-]{subsubsection.4.3.2}{Estimating the parameters of the body model}{subsection.4.3}% 40
\BOOKMARK [2][-]{subsection.4.4}{The scoring algorithm}{section.4}% 41
\BOOKMARK [1][-]{section.5}{Implementation and challenges}{}% 42
\BOOKMARK [2][-]{subsection.5.1}{Connection to the csl DVR}{section.5}% 43
\BOOKMARK [2][-]{subsection.5.2}{Efficiency}{section.5}% 44
\BOOKMARK [3][-]{subsubsection.5.2.1}{Caching of log factorials}{subsection.5.2}% 45
\BOOKMARK [3][-]{subsubsection.5.2.2}{Detection before scoring}{subsection.5.2}% 46
\BOOKMARK [3][-]{subsubsection.5.2.3}{Faster generation of the observed variables for the body model}{subsection.5.2}% 47
\BOOKMARK [2][-]{subsection.5.3}{Novelties}{section.5}% 48
\BOOKMARK [1][-]{section.6}{Evaluation and testing}{}% 49
\BOOKMARK [2][-]{subsection.6.1}{Correctness testing}{section.6}% 50
\BOOKMARK [2][-]{subsection.6.2}{Performance testing}{section.6}% 51
\BOOKMARK [2][-]{subsection.6.3}{Testing of the colour model}{section.6}% 52
\BOOKMARK [2][-]{subsection.6.4}{Static tests}{section.6}% 53
\BOOKMARK [1][-]{section.7}{Conclusion}{}% 54
\BOOKMARK [1][-]{appendix.A}{Pixels and colour spaces}{}% 55
\BOOKMARK [1][-]{appendix.B}{The Dirichlet distribution}{}% 56
\BOOKMARK [1][-]{appendix.C}{Derivation of the component score}{}% 57
\BOOKMARK [1][-]{appendix.D}{Derivations of quantities required for Newton-Raphson optimization}{}% 58
\BOOKMARK [1][-]{appendix.E}{Visual representation of the modified hue distributions}{}% 59
