from django.conf.urls import url
from train_dirichlet import views

urlpatterns = [
    url(r'^$', views.idx, name='index'),
    url(r'^get_all_data/$', views.get_image_paths, name='getPaths'),
    url(r'^register_selection/$', views.move_image, name='moveImage'),
]