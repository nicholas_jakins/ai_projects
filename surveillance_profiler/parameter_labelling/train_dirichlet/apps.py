from __future__ import unicode_literals

from django.apps import AppConfig


class TrainDirichletConfig(AppConfig):
    name = 'train_dirichlet'
