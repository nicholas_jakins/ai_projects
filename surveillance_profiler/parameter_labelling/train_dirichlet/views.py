import sys
from django.shortcuts import render
import os

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.template.loader import get_template
from django.shortcuts import render_to_response
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

import shutil
from shutil import copyfile

from django.contrib.auth.models import User
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth import authenticate

import json

root_ROIS = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/Dirichlet/train_with_subtractor/trained_ROIs"

def change_path_static(path_actual):
    "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/Dirichlet/train_with_subtractor/trained_ROIs/torso/5.png"
    
    path_static = "../static/images/"
    
    modified_name = path_actual.split("/", 12)[-2] + "_" + path_actual.split("/", 12)[-1]
    
    
    image_root = path_actual[::-1].split("/", 1)[1][::-1]
    image_name = path_actual.split("/", 12)[-1]
    return path_static + modified_name
    
    
def move_image_to_static(image_path):
    dest = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/label_parameters_application/parameter_labelling/static/images"
    
    image_root = image_path[::-1].split("/", 1)[1][::-1]
    image_name = image_path.split("/", 12)[-1]
    image_name_mask = image_name[0:image_name.find(".")] + "_mask.png"
    image_path_mask = image_root + "/" + image_name_mask
    
    #print "image_path_mask: ", image_path_mask
    
    # get image name
    modified_name = image_path.split("/", 12)[-2] + "_" + image_path.split("/", 12)[-1]
    modified_mask_name = image_path.split("/", 12)[-2] + "_" + image_name_mask
    destination_mask = dest + "/" + modified_mask_name
    destination_orig = dest + "/" + modified_name
    
    copyfile(image_path, destination_orig)
    copyfile(image_path_mask, destination_mask)
    
    #print "destination_mask: ", destination_mask
    #print "destination_orig: ", destination_orig
    
def contains_ext(l, extension):
	if not isinstance(l, list):
		return False
	if any(extension in s for s in l):
		return True
	return False

def get_list_image_names(image_dir):
    for files in os.walk(image_dir):
        image_names = [i for i in files if contains_ext(i, ".png") == True]
        if len(image_names) == 0:
            return []
        image_names = [i for i in image_names[0] if ".png" in i and "mask" not in i]
        return image_names

def get_paths(part):
    
    print "body part: ", part
    path_to_images = root_ROIS + "/" + part + "/"
    print path_to_images
    image_names = get_list_image_names(path_to_images)
    
    image_names = [{"path" : path_to_images + i, "active" : "True", "view_flag" : "true"} if idx == 0 \
                   else {"path" : path_to_images + i, "active" : "False", "view_flag" : "true"} for (idx, i) in enumerate(image_names, 0)]
    
    for image_info in image_names:
        print image_info
        move_image_to_static(image_info["path"])
        image_info["path"] = change_path_static(image_info["path"])
        
    print "images: ", image_names
    print "---------------------------------------------"
    return {"image_paths": image_names}
    

@csrf_exempt
def idx(request):
    if request.method != "POST":
        context = RequestContext(request)
        context_dict = {}
        return render_to_response('./views/main_view.html', context_dict, context)
    else:
        return HttpResponse("")
    
    
@csrf_exempt
def get_image_paths(request):
    
    ret_dict = {"head" : {}, "torso" : {}, "legs" : {}}
    
    # get all the paths
    for part in ret_dict:
        paths = get_paths(part)
        ret_dict[part] = paths
        
        
    json_str = json.dumps(ret_dict, sort_keys=True, indent=4)
    print json_str
    return HttpResponse(json_str)


@csrf_exempt
def move_image(request):
    data = json.loads(request.body)
    image_path = smart_str(xstr(data['path']))
    color = smart_str(xstr(data['color']))
    body_part = smart_str(xstr(data['part']))
    skip = smart_str(xstr(data['skip']))
    
    
        
    
    print "color: ", color
    print "path: ", image_path
    print "part: ", body_part
    
    original_root = root_ROIS + "/" + body_part
    
    name = image_path.split("/", 4)[-1]
    num = name[name.find("_")+1:name.find(".")]
    original_path_unmasked = original_root + "/" + num + ".png"
    original_path_masked = original_root + "/" + num + "_mask.png"
    
    
    if int(skip) == 1:
        os.remove(original_path_unmasked)
        os.remove(original_path_masked)
        
        return HttpResponse("")
    
    destination_unmasked = original_root + "/" + color + "/" + num + ".png"
    destination_masked = original_root + "/" + color + "/" + num + "_mask.png"
    print "********************************************************************************"
    print "destination_unmasked: ", destination_unmasked
    print "destination_masked: ", destination_masked
    
    shutil.move(original_path_unmasked, destination_unmasked)
    shutil.move(original_path_masked, destination_masked)
    
    # remove from static
    mask_to_remove = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/label_parameters_application/parameter_labelling/static/images/" + body_part + "_" + num + "_mask.png"
    print mask_to_remove
    unmasked_to_remove = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/label_parameters_application/parameter_labelling/static/images/" + body_part + "_" + num + ".png"
    
    os.remove(mask_to_remove)
    os.remove(unmasked_to_remove)
    
    return HttpResponse("")
    
def xstr(s):
    return None if s == "" else str(s)
    
