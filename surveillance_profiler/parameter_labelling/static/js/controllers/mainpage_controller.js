app.controller('mainpage_controller', ['$scope', '$http', '$window', '$location', '$timeout', function($scope, $http, $window, $location, $timeout) {
    
    $scope.selectedName = "torso"
    $scope.current_part = null
    
    param = {}
    // make a post to get all the training image paths for head, torso, longpants ect
    $http.post('http://localhost:8000/train_dirichlet/get_all_data/', param)
                .success(function(response_data, status) {
            
        $scope.images = response_data
        $scope.current_part = $scope.images[$scope.selectedName].image_paths
        console.log("$scope.current_part: " + $scope.current_part)
        $scope.new_images = $scope.current_part
    })
    
    
    $scope.body_parts = ["head", "torso", "legs"]
    
    $scope.changeImages = function() {
        $scope.current_part = $scope.images[$scope.selectedName].image_paths
        $scope.new_images = $scope.images[$scope.selectedName].image_paths
        console.log("$scope.new_images: " + $scope.current_part);
    }
        
    var clone = function(dic) {
        //console.log("dic.length: " + dic.length)
        new_dict = []
        for (i = 0; i < dic.length; i++) {
            new_dict[i] = dic[i]    
            //console.log("new_dict[i].path: " + new_dict[i].path)
        }
        //console.log("new_dict.length: " + new_dict.length)
        return new_dict
    }
    
    $scope.label_image = function(skip) {
        
        
        
        var current_path = ""
        var color = null
        
        if ($scope.radioModel != null) {
            color = $scope.radioModel
        }
        
        console.log("$scope.new_images.length: " + $scope.new_images.length)
        var clone_images = clone($scope.new_images);
        console.log("clone_images.length: " + clone_images.length)
        
        var old_images = clone_images
        
        $scope.new_images = []
        var new_counter = 0
        //console.log("old_images.len: " + old_images[0].path)
        
        for (i = 0; i < old_images.length; i++) {
            var is_active = old_images[i].active
            var path = old_images[i].path
            console.log("path: " + path)
            if (is_active == true) {
                current_path = path
                //delete $scope.images[$scope.selectedName].image_paths[i]
                //delete $scope.current_part[i]
            } else {
                if (new_counter == 0) {
                    $scope.new_images[new_counter] = {"path" : path, "active" : "true"}
                    new_counter++
                } else {
                    $scope.new_images[new_counter] = {"path" : path, "active" : "false"}
                    new_counter++
                }
                
            }
            
        }
        
        console.log("after all, $scope.new_images.length: " + $scope.new_images.length)
                    
        $scope.images[$scope.selectedName] = $scope.new_images
        $scope.current_part = $scope.new_images
        
        console.log($scope.images[$scope.selectedName])
        console.log($scope.current_part)
        
        param = {
            "color" : color,
            "path" : current_path,
            "part" : $scope.selectedName,
            "skip" : skip
        }
        
        $http.post('http://localhost:8000/train_dirichlet/register_selection/', param)
                .success(function(response_data, status) {
            
            
        })
    }
    
    
    
    
    
}]);