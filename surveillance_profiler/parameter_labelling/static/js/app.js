var app = angular.module('main', ['ngRoute', 'ui.bootstrap']); // creating a module - used to house controllers.

// $routeProvider - "module that routes your application to different pages without reloading the entire application."
app.config(function ($routeProvider, $locationProvider) { 
	$routeProvider 					// updated with 'when' to route specify redirects for Django urls.py to parse. 
        .otherwise({ 				// defaults to routing to the main page
			redirectTo: '/' 		// main page
		});
});