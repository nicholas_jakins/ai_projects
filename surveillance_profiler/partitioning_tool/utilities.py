from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
import psycopg2

def compute_covariance_mean(data_matrix):
		#print "data_matrix: ", len(data_matrix)
		

		N = data_matrix.shape[0]
		mean = np.mean(data_matrix, axis=0)

		dev_from_mean = np.subtract(data_matrix, np.array(mean))
		CSSCP = np.dot(dev_from_mean.T, dev_from_mean)
		covariance = np.dot(CSSCP, (1 / (float(N) - 1)))
		return (mean, covariance)