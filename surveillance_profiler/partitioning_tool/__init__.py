from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
import psycopg2
import utilities as u
import write_config as w

print "------------------------------Initializing partitioning------------------------------"
# Establish connection and write output to 
conn = psycopg2.connect(database="parameters", user="nicholasjakins")
cur = conn.cursor()


print "Connection established to parameters database..."

r = w.partition_distribution(cur, conn)
dist = w.gen_distribution(r)
print "Mean and covariance of partitions written to ./partition_config.py"
print "-------------------------------------------------------------------------------------"