from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
import psycopg2

refPt = []
partition = {0 : [], 1 : [], 2 : [], 3 : []}
partition_index = 0
image_frame = None
number_of_boxes = 0
current_box_number = 0
clone = None
_DIR = None
COUNT = 0

def reset_global_variables():
	global refPt, partition, partition_index, image_frame, number_of_boxes, current_box_number

	refPt = []
	partition = {0 : [], 1 : [], 2 : [], 3 : []}
	partition_index = 0
	image_frame = None
	number_of_boxes = 0
	current_box_number = 0	

def show_partitions():
	global refPt, partition_index

	cv2.destroyAllWindows()

	for idx in range(partition_index):
		cv2.rectangle(image_frame, partition[idx][0], partition[idx][1], (0, 0, 255), 1)

	cv2.namedWindow('image')
	cv2.setMouseCallback('image', register_partition)
	cv2.imshow('image',image_frame)

def register_partition(event, x, y, flags, params):
	global refPt, partition, partition_index, image_frame, number_of_boxes, current_box_number, clone

	if event == cv2.EVENT_LBUTTONDOWN:
		#print "left click"
		refPt = [(x, y)]
		#print refPt
		cropping = True

		
	elif event == cv2.EVENT_LBUTTONUP:
		#print "button up"
		refPt.append((x, y))
		cropping = False
		
		partition[partition_index] = refPt
		print "partition[partition_index]: ", partition[partition_index]
		partition_index = partition_index + 1

		show_partitions()


def analyze_image_frame(data):
	global refPt, partition, partition_index, image_frame, number_of_boxes, current_box_number, clone

	image_dir = "../extracted/"
	for image_data in data:
		boxes = image_data["Boxes"]
		image_file = image_dir + str(image_data["Frame"]) + ".png"
		image_frame = cv2.imread(image_file)

		for box in boxes:
			partition[0] = [(box[0], box[1]), (box[2], box[3])]
			partition_index = partition_index + 1
			
			cv2.rectangle(image_frame, (box[0], box[1]), (box[2], box[3]), (0, 0, 255), 1)

		register_boxes_on_image()

def register_boxes_on_image():
	global refPt, partition, partition_index, image_frame, number_of_boxes, current_box_number, clone, COUNT

	clone = image_frame.copy()
	cv2.namedWindow('image')
	cv2.setMouseCallback('image', register_partition)

	cv2.imshow('image',image_frame)

	while(1):
		print partition_index    
		if partition_index == 4:
			COUNT = COUNT + 1    
			break    
		key = cv2.waitKey(1) & 0xFF    
		if key == ord("c"):    
			print "destroying all windows"    
			cv2.destroyAllWindows()    
			reset_global_variables()
			return

	record_in_database()
	show_partitions()
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	reset_global_variables()

def initialize_count():
	global _DIR, COUNT
	storage_directory = os.path.join(_DIR, "Partitioning/extracted_frames")
	print "storage_directory: ", storage_directory    
	for list_files in os.walk(storage_directory):    
		print "list_files: ", list_files
		for files in list_files:    
			if 'list' in str(type(files)):
				print "files: ", files ; print "len(files): ", len(files)
				if len(files) == 0:
					COUNT = 0
					continue
				if len(files) > 0:
					images = [f for f in files if ".png" in f]
					print "images: ", images
					if len(images) != 0:
						counts = np.array([int(name.replace(".png", "")) for name in images])
						COUNT = np.max(counts)

def gen_partition_input(Z):    
	Z_1 = Z[0][0]    
	Z_2 = Z[0][1]    
	Z_3 = Z[1][0]    
	Z_4 = Z[1][1]    
	return str(Z_1) + "," + str(Z_2) + "," + str(Z_3) + "," + str(Z_4)

def record_in_database():
	global image_frame, conn, cur, _DIR, COUNT
	storage_directory = os.path.join(_DIR, "Partitioning/extracted_frames")
	image_path = storage_directory + "/"+str(COUNT)+".png"
	print "image_path: ", image_path
	print "partition: ", partition
	Z_base = gen_partition_input(partition[0])
	Z_head = gen_partition_input(partition[1])
	Z_torso = gen_partition_input(partition[2])
	Z_legs = gen_partition_input(partition[3])

	cur.execute("INSERT INTO partition(image_path, image_height, image_width, Z_base, Z_head, Z_torso, Z_legs) \
								   VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING", \
                         		   (image_path, image_frame.shape[0], image_frame.shape[0], Z_base, Z_head, Z_torso, Z_legs))

	img = image_frame.copy()
	cv2.imwrite(image_path, img)
	conn.commit()

def process_input(data_str):
	str_to_exec = "res = " + data_str
	str_to_exec = str.replace(str_to_exec, "array", "")
	exec str_to_exec
	return res

# get data from ../data.py and present the results
conn = psycopg2.connect(database="parameters", user="nicholasjakins")
cur = conn.cursor()


_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

f = open("../data.py", "r")
data = f.read()
f.close()

data = process_input(data)
initialize_count()
analyze_image_frame(data)







