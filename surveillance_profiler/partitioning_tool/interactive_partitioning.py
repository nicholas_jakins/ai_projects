from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
import psycopg2

refPt = []
cropping = False

partition = {0 : [], 1 : [], 2 : [], 3 : []}
partition_dict = {0 : "body", 1 : "head", 2 : "torso", 3 : "legs"}
partition_index = 0

clone = cv2.imread("./video_dir/partitionERD.png")
image_frame = cv2.imread("./video_dir/partitionERD.png")

COUNT = 0

def show_partitions():
	global refPt, cropping, partition_index, partition_dict, partition, clone

	cv2.destroyAllWindows()

	for idx in range(partition_index):
		cv2.rectangle(image_frame, partition[idx][0], partition[idx][1], (0, 0, 255), 1)

	cv2.namedWindow('image')
	cv2.setMouseCallback('image', register_partition)
	cv2.imshow('image',image_frame)

def register_partition(event, x, y, flags, params):
	global refPt, cropping, partition_index, partition_dict, partition, clone
	
	
	if event == cv2.EVENT_LBUTTONDOWN:
		#print "left click"
		refPt = [(x, y)]
		#print refPt
		cropping = True

		
	elif event == cv2.EVENT_LBUTTONUP:
		#print "button up"
		refPt.append((x, y))
		cropping = False
		
		partition[partition_index] = refPt
		print "partition[partition_index]: ", partition[partition_index]
		partition_index = partition_index + 1

		show_partitions()

		
def reset_global_parameters():
	global refPt, cropping, partition_index, partition_dict, partition, clone, image_frame
	refPt = []
	cropping = False

	partition = {0 : [], 1 : [], 2 : [], 3 : []}
	partition_dict = {0 : "body", 1 : "head", 2 : "torso", 3 : "legs"}
	partition_index = 0

def get_files_in_dir(dir_name):
	x = 1

def gen_partition_input(Z):    
	Z_1 = Z[0][0]    
	Z_2 = Z[0][1]    
	Z_3 = Z[1][0]    
	Z_4 = Z[1][1]    
	return str(Z_1) + "," + str(Z_2) + "," + str(Z_3) + "," + str(Z_4)

def record_in_database():
	print "inserting into the database: ", COUNT
	global image_frame, conn, cur, _DIR, COUNT
	storage_directory = os.path.join(_DIR, "Partitioning/extracted_frames")
	image_path = storage_directory + "/"+str(COUNT)+".png"
	print "image_path: ", image_path
	Z_base = gen_partition_input(partition[0])
	Z_head = gen_partition_input(partition[1])
	Z_torso = gen_partition_input(partition[2])
	Z_legs = gen_partition_input(partition[3])

	cur.execute("INSERT INTO partition(image_path, image_height, image_width, Z_base, Z_head, Z_torso, Z_legs) \
								   VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING", \
                         		   (image_path, image_frame.shape[0], image_frame.shape[0], Z_base, Z_head, Z_torso, Z_legs))

	img = image_frame.copy()
	cv2.imwrite(image_path, img)
	conn.commit()

def register_box_on_image(frame):
	global refPt, cropping, partition_index, partition_dict, partition, clone, image_frame, COUNT

	clone = frame.copy()
	cv2.namedWindow('image')
	cv2.setMouseCallback('image', register_partition)

	cv2.imshow('image',image_frame)

	while(1):    
		if partition_index == 4:
			COUNT = COUNT + 1    
			break    
		key = cv2.waitKey(1) & 0xFF    
		if key == ord("c"):    
			print "destroying all windows"    
			cv2.destroyAllWindows()    
			return
	record_in_database()
	show_partitions()
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	reset_global_parameters()

	
def record_video(video_path):
	global image_frame

	cap = cv2.VideoCapture(video_path)
	count = 0
	while(1):
		ret, image_frame = cap.read()    
		if image_frame == None:    
				break
		if count % 10 == 0:
			print count
			
			
			register_box_on_image(image_frame)

		count = count + 1

def initialize_count():
	global _DIR, COUNT
	storage_directory = os.path.join(_DIR, "Partitioning/extracted_frames")
	print "storage_directory: ", storage_directory    
	for list_files in os.walk(storage_directory):    
		print "list_files: ", list_files
		for files in list_files:    
			if 'list' in str(type(files)):
				print "files: ", files ; print "len(files): ", len(files)
				if len(files) == 0:
					COUNT = 0
					continue
				if len(files) > 0:
					images = [f for f in files if ".png" in f]
					print "images: ", images
					if len(images) != 0:
						counts = np.array([int(name.replace(".png", "")) for name in images])
						COUNT = np.max(counts)




conn = psycopg2.connect(database="parameters", user="nicholasjakins")
cur = conn.cursor()

_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.join(_DIR, "Partitioning/video_dir/")

initialize_count()
print COUNT
for list_files in os.walk(BASE_DIR):
	for files in list_files:
		
		for f in files:
			if ".mp4" in f:    
				print "file: ", f
				record_video(BASE_DIR + str(f))
				os.remove(BASE_DIR + str(f))

#conn.commit()










