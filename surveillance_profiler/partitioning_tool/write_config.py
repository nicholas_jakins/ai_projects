from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
import psycopg2
import utilities as u

''' This class queries the parameters database for partitions. 
	A mean vector and co-variance matrix is calculated and written to a configuration file partition_config.py, 
		this data governs a multivariate distribution for partitions.

'''


def param_extraction(res):
	temp = "res = " + res
	exec temp

	return (res[1], res[0])

def compute_ratio(outer_dim, inner):
	#print "outer dim: ", outer_dim
	#print "inner dim: ", inner
		
	initial_ratio = ((float(inner[0]) / float(outer_dim[0])) * 100, (float(inner[1]) / float(outer_dim[1])) * 100)

	inner_w = inner[2] - inner[0]
	inner_h = inner[3] - inner[1]
	
	a1 = (float(inner_w) / float(outer_dim[0])) * 100.0
	a2 =  (float(inner_h) / float(outer_dim[1])) * 100.0

	v = (initial_ratio[0], initial_ratio[1], a1, a2)
	#print "v: ", v
	return v

def display_partition(v, image):
	#print "partition is: ", v
	
	w = image.shape[0]
	h = image.shape[1]
	#print (w, h)
	x = (v[0] / 100.0) * w
	y = (v[1] / 100.0) * h
	b_w = (v[2] / 100.0) * w
	b_h = (v[3] / 100.0) * h
	x1 = x + b_w
	y1 = y + b_h

	cv2.rectangle(image, (int(x), int(y)), (int(x1), int(y1)), (0, 0, 0), 1)

	coord_of_box = (x, y, x1, y1)
	dim_body_box = (x1 - x, y1 - y)
	for i in xrange(4, 15, 4):

		print i
		w = image.shape[0]
		h = image.shape[1]
		print (w, h)
		x_o = coord_of_box[0] + (v[i] / 100.0) * dim_body_box[0]
		y_o = coord_of_box[1] + (v[i+1] / 100.0) * dim_body_box[1]
		
		x_o1 = x_o + (v[i+2] / 100.0) * dim_body_box[0]
		y_o1 = y_o + (v[i+3] / 100.0) * dim_body_box[1]

		cv2.rectangle(image, (int(x_o), int(y_o)), (int(x_o1), int(y_o1)), (0, 0, 0), 1)

	cv2.imshow("the image: ", image)
	cv2.waitKey(0) 

def partition_distribution(cur, conn):
	cur.execute("SELECT * from partition")
	results = cur.fetchall()
	
	data = np.zeros((len(results), 12))
	count = 0
	for par in results:
		img = cv2.imread(par[0])

		size_of_image = (img.shape[0], img.shape[1])
		body_dim = param_extraction(par[3])
		body_dim = (body_dim[0][0], body_dim[0][1], body_dim[1][0], body_dim[1][1])
		body_ratio = compute_ratio(size_of_image, body_dim)
				
		body_w = body_dim[2] - body_dim[0]
		body_h = body_dim[3] - body_dim[1]
		
		head_dim = param_extraction(par[4])
		head_dim = (head_dim[0][0], head_dim[0][1], head_dim[1][0], head_dim[1][1])
		
		head_w = head_dim[2] - head_dim[0]
		head_h = head_dim[3] - head_dim[1]
		x1 = head_dim[0] - body_dim[0]
		y1 = head_dim[1] - body_dim[1]
		head_dim = (x1, y1, x1 + head_w, y1 + head_h)
		head_ratio = compute_ratio((body_w, body_h), head_dim)
		
		torso_dim = param_extraction(par[5])
		torso_dim = (torso_dim[0][0], torso_dim[0][1], torso_dim[1][0], torso_dim[1][1])
		torso_w = torso_dim[2] - torso_dim[0]
		torso_h = torso_dim[3] - torso_dim[1]
		x1 = torso_dim[0] - body_dim[0]
		y1 = torso_dim[1] - body_dim[1]
		torso_dim = (x1, y1, x1 + torso_w, y1 + torso_h)
		torso_ratio = compute_ratio((body_w, body_h), torso_dim)
		
		legs_dim = param_extraction(par[6])
		legs_dim = (legs_dim[0][0], legs_dim[0][1], legs_dim[1][0], legs_dim[1][1])
		legs_w = legs_dim[2] - legs_dim[0]
		legs_h = legs_dim[3] - legs_dim[1]
		x1 = legs_dim[0] - body_dim[0]
		y1 = legs_dim[1] - body_dim[1]
		legs_dim = (x1, y1, x1 + legs_w, y1 + legs_h)
		legs_ratio = compute_ratio((body_w, body_h), legs_dim)
		
		'''partition = (body_ratio[0], body_ratio[1], body_ratio[2], body_ratio[3],\
					 head_ratio[0], head_ratio[1], head_ratio[2], head_ratio[3], \
					 torso_ratio[0], torso_ratio[1], torso_ratio[2], torso_ratio[3], \
					 legs_ratio[0], legs_ratio[1], legs_ratio[2], legs_ratio[3])'''

		partition = (head_ratio[0], head_ratio[1], head_ratio[2], head_ratio[3], \
					 torso_ratio[0], torso_ratio[1], torso_ratio[2], torso_ratio[3], \
					 legs_ratio[0], legs_ratio[1], legs_ratio[2], legs_ratio[3])

		img = cv2.imread(par[0])
		#display_partition(partition, img)

		data[count] = partition
		count = count + 1

	return data


def valid_partition(ratio_of_partitions):
	for elem in ratio_of_partitions:
		if elem < 0.0:
			return False
	return True

''' Given a collection of partition ratios, generate the mean vector and co-variance matrix that governs 
		the multivariate distribution of partitions, write this to a config file.
'''
def gen_distribution(partition_config):
	indices_to_delete = []
	for idx in range(len(partition_config)):
		row = partition_config[idx]
		if not valid_partition(row):
			indices_to_delete.append(idx)
		
	partition_config = np.delete(partition_config, indices_to_delete, axis=0)

	mean, covariance = u.compute_covariance_mean(partition_config)
	
	f = open("./Partitioning/gaussian_config.py", "w")
	f.write(str((mean, covariance)))
	f.close()





