from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2

from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
import cv2.bgsegm as contrib
import itertools

#import Homography.groundPlane as gp

def get_bounding_boxes(frame, h):
		
		(rects, weights) = h.detectMultiScale(frame, winStride=(4, 4), padding=(8, 8), scale=1.03)
		rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
		pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
		return pick

def is_valid_box(mask, box, threshold, cnt, ind):
	#print "ABOUT TO DISPLAY THE MASKED IMAGE"
	#cv2.imwrite("/Users/nicholasjakins/Desktop/masked_images/" + str(cnt) + "_" + str(ind) + "_" + ".png", mask)
	#cv2.imshow("masked image", mask)
	#cv2.waitKey(0)

	number_pixels = 0

	for (i, j) in itertools.product(xrange(box[0] + 1, box[2] - 1, 1), xrange(box[1] + 1, box[3] - 1, 1)):
		try:
			if mask[j][i] != 0:
				number_pixels = number_pixels + 1
		except IndexError:
			continue

	#print "number_pixels: ", number_pixels

	ratio = 0
	if number_pixels != 0:
		ratio = float(number_pixels) / ((box[2] - box[0]) * (box[3] - box[1])) 

	if ratio > threshold:
		return 1
	else:
		return 0


class boundingBoxes:

	def __init__(self, image_paths, name_indicator):
		#print "detecting the bodies..."
		self.images = image_paths
		#print self.images
		self.hog = cv2.HOGDescriptor()
		self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

		# initialize background subtractor
		self.fgbg = contrib.createBackgroundSubtractorMOG()
		self.frames = {}
		self.count = 0
		self.thing = 12
		self.captureBoxes()
		

	def captureBoxes(self):

		for img in self.images:

			#print "img: ", img
			frame = cv2.imread(img)
			if frame == None:
				continue
			
			pyramid_frame = imutils.resize(frame, width=min(400, frame.shape[1]))

			fgmask = self.fgbg.apply(pyramid_frame)
						

			boxes = get_bounding_boxes(pyramid_frame, self.hog)

			valid_boxes = []
			box_dict = {"Frame": "None", "Boxes": valid_boxes, "Frame_mask" : None}

			for box in boxes:
					
				is_box = is_valid_box(fgmask, box, 0.01, self.count, self.thing)
				self.count = self.count + 1
				if is_box == 1:
					valid_boxes.append(box)


			self.frames[img] = {"boxes" : valid_boxes, "mask" : fgmask}
			#print boxes

	def get_frames(self):
		return self.frames

	
	def write_output(self):

		f = open("data.py", "w")
		f.write(str(self.frames))
		f.close()












