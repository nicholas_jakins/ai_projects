from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2

from skimage.feature import hog
from skimage import data
import matplotlib.pyplot as plt
from skimage import data, color, exposure

import boundingBoxes as bb
import segmentation as s
import paramConfig as pc

import itertools

import Partitioning.write_config as P
import Homography.groundPlane as gp

from scipy.stats import multivariate_normal

import inference.Infer as I
import Color_model.paramConfig as cm
import scoring.score_frame as sf

from md5 import md5

def get_histo(img, ROI, color_model):
		
	print "img.shape: ", img.shape
	print ROI
	roi = img[ROI[1]:ROI[3], ROI[0]:ROI[2]]
	#cv2.imshow("torso", roi)
	#cv2.waitKey(0)

	roi_hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

	counts = np.zeros((len(pc.dict_colors),))

	for (i, j) in itertools.product(xrange(roi.shape[0]), xrange(roi.shape[1])):
		try:
			idx = color_model.get_color_category(roi_hsv[i][j])
			#print "idx: ", idx
			counts[idx] = counts[idx] + 1
		except IndexError:
			continue

	total = counts.sum()

	counts = [(i/float(total)) * 100.0 for i in counts]
	
	return counts

# get all information per frame
def segment_output(color_model):
	f = open('data.py', 'r')

	bounding_boxes = f.read()

	bounding_boxes = str.replace(bounding_boxes, "array", "")

	f.close()

	str_to_execute = "dict_boxes = " + bounding_boxes

	exec str_to_execute

	for elem in dict_boxes:
		image_path = "./extracted/"+str(elem['Frame'])+".png"
		mask_path = "./extracted/"+str(elem['Frame'])+"_mask.png"
		image = cv2.imread(image_path)
		image_mask = cv2.imread(mask_path, 0)

		for box in elem['Boxes']:
			f = plt.figure()

			bs = s.bodySegmentation(image_mask, image, box, ((30, 20), (60, 40), (50, 100)), 0.05)
			partition = bs.get_partition()
			
			# plot histogram of torso
			img_to_show = partition[4]
			img_RGB = cv2.cvtColor(img_to_show, cv2.COLOR_BGR2RGB)
		
			plt.title("Color analysis of moving body detection")

			a1 = plt.subplot2grid((2,2), (0,0), rowspan=2)
			plt.imshow(img_RGB)
			plt.title("Sample frame")

			counts = get_histo(img_to_show, partition[2], color_model)
			a2 = plt.subplot2grid((2,2), (0,1))
			t = plt.gca()
			t.axes.get_xaxis().set_visible(False)
			t.axes.get_yaxis().set_visible(False)
			bar_list = plt.bar([1,2,3,4,5,6,7,8,9,10,11], counts)
			for i in range(len(pc.dict_colors)):
				bar_list[i].set_color(pc.dict_colors[i])

			plt.title("Torso histogram")

			counts_legs = get_histo(img_to_show, partition[3], color_model)
			a3 = plt.subplot2grid((2,2), (1,1))
			t = plt.gca()
			t.axes.get_xaxis().set_visible(False)
			t.axes.get_yaxis().set_visible(False)
			bar_list = plt.bar([1,2,3,4,5,6,7,8,9,10,11], counts_legs)
			for i in range(len(pc.dict_colors)):
				bar_list[i].set_color(pc.dict_colors[i])

			plt.title("Legs histogram")
			
			plt.show()


def read_partition_config():
	f = open("./Partitioning/gaussian_config.py", "r")
	data = f.read()
	str_to_execute = "D = " + data
	str_to_execute = str.replace(str_to_execute, "array", "")
	exec str_to_execute
	f.close()

	return (D[0], D[1])


def analyze_frames(mu, cov, frames, ground_plane, color_model, color_spec):

	for frame in frames:
		img_path = "./extracted/" + str(frame["Frame"]) + ".png"
		
		img = cv2.imread(img_path)
		
		score_obj = sf.score(frame, img, mu, cov, color_spec, color_model)
		s = score_obj.estimate_score()
		


ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", required=True, help="path to video")
ap.add_argument("-r", "--rate", required=True, help="rate of analysis")

args = vars(ap.parse_args())

# Detect and segment
print "------------------------Analysing video feed------------------------"
bounding_box_detector = bb.boundingBoxes(args['video'], args['rate'])
print "--------------------------------------------------------------------"

bounding_box_detector.write_output()
print "Initializing color model..."
color_model = pc.colorModel(1)


frames_with_bounding_boxes = bounding_box_detector.get_frames()

np.set_printoptions(suppress=True)

# initailize groundplane analyzer
ground_plane = gp.groundPlaneAnalysis(15)

# Apply Gaussian segmentation
print "----------------Generating gaussian for partitioning----------------"
M, C = read_partition_config()



# initialze color model
color_model = cm.HSVModel()
analyze_frames(M, C, frames_with_bounding_boxes, ground_plane, color_model, {0 : 1, 1 : 5, 2 : 1})
print "--------------------------------------------------------------------"

 