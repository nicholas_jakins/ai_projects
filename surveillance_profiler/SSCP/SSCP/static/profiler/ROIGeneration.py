from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import cv2.bgsegm as contrib
import itertools
import os
import time
from matplotlib import pyplot as plt
import math

def is_in_corner(r, c):
	if (c[0] <= r.x1 and c[0] >= r.x0) and (c[1] <= r.y1 and c[1] >= r.y0):
		return True
	else:
		return False

def intersect(r1, r2):

	# does r2 intersect r1?
	width = r2.x1 - r2.x0
	height = r2.y1 - r2.y0
	
	top_left = (r2.x0, r2.y0)
	top_right = (r2.x0 + width, r2.y0)
	bot_left = (r2.x0, r2.y0 + height)
	bot_right = (r2.x1, r2.y1)
	
	# if bottom left is in
	dic = {"tl" : is_in_corner(r1, top_left), "tr" : is_in_corner(r1, top_right), "bl" : is_in_corner(r1, bot_left), "br" : is_in_corner(r1, bot_right)}
	if dic["tl"] or dic["tr"] or dic["bl"] or dic["br"]:
		return (True, dic)
	else:
		return (False, dic)
	
def intersects(r1, r2):
	dir_1, dic1 = intersect(r1, r2)
	dir_2, dic2 = intersect(r2, r1)
	if dir_1 == True:
		return (True, dic1)
	elif dir_2 == True:
		return (True, dic2)

	return (False, {})

def dist(p1, p2):
	diff1 = p1[0] - p2[0]
	diff2 = p1[1] - p2[1]
		
	return math.sqrt((diff1 ** 2) + (diff2 ** 2))

def rectangle_distance(r1, r2):
	left = r2.x1 < r1.x0
	right = r1.x1 < r2.x0
	bottom = r2.y1 < r1.y0
	top = r1.y1 < r2.y0

	if top and left:
		return dist((r1.x0, r1.y1), (r2.x1, r2.y0))
	elif left and bottom:
		return dist((r1.x0, r1.y0), (r2.x1, r2.y1))
	elif bottom and right:
		return dist((r1.x1, r1.y0), (r2.x0, r2.y0))
	elif right and top:
		return dist((r1.x1, r1.y1), (r2.x0, r2.y0))
	elif left:
		return r1.x0 - r2.x1
	elif right:
		return r2.x0 - r1.x1
	elif bottom:
		return r1.y0 - r2.y1
	elif top:
		return r2.y0 - r1.y1

class rect:

	def __init__(self, x0, y0, x1, y1):

		self.x0 = x0
		self.y0 = y0
		self.x1 = x1
		self.y1 = y1
		self.colour = (100, 255, 0)

	def __str__(self):
		return "(" + str(self.x0) + ", " + str(self.y0) + ", " + str(self.x1) + ", " + str(self.y1) + ")"

	def set_colour(self, col):
		self.colour = col

	def get_size(self):
		width = self.x1 - self.x0
		height = self.y1 - self.y0
		return width * height

class generateOuterROI:

	def __init__(self, threshold_x, threshold_y, black_image):
		# threshold_x is the maximum horizontal distance two ROIs can be
		# threshold_y is the maximum vertical distance two ROIs can be
		self.black_image = black_image 

		self.img_width = black_image.shape[0]
		self.img_height = black_image.shape[1]

		self.x_thresh = threshold_x
		self.y_thresh = threshold_y 
		self.boxes = []

	def draw(self, message, *recs):
		
		image_copy = self.black_image.copy()
		for r in recs[0]:
			col = r.colour
			cv2.rectangle(image_copy, (r.x0, r.y0), (r.x1, r.y1), col, 1)

		cv2.imshow(message, image_copy)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

	def is_in_range(self, outer_box, current_box):
		# check overlapping
		# x0, y0, x1, y1
		intersect, cor_dic = intersects(outer_box, current_box)

		if intersect:
			#print "intersects"
			return True
		else:
				
			# how far away is this bounding box?			
			distance = rectangle_distance(outer_box, current_box)
	
			length = (self.x_thresh / 100.0) * self.img_width
			#print "length: ", length
			if distance > length:
				return False
			else:
				return True

	def handle_intersection(self, r1, r2, cor_dic):
		# r1 is the outer
		# if intersects
		new_outer = rect(r1.x0, r1.y0, r1.x1, r1.y1)

		if cor_dic["tl"] and cor_dic["bl"] and cor_dic["br"] and cor_dic["tr"]:
			#print "enclosed!"
			return new_outer
		if cor_dic["bl"] and cor_dic["br"]:
			new_outer.y0 = r2.y0
		elif cor_dic["bl"]:
			new_outer.y0 = r2.y0
			new_outer.x1 = r2.x1
		elif cor_dic["br"]:
			new_outer.x0 = r2.x0
			new_outer.y0 = r2.y0
		elif cor_dic["tr"] and cor_dic["br"]:
			new_outer.x0 = r2.x0
		elif cor_dic["tr"]:
			new_outer.x0 = r2.x0
			new_outer.y1 = r2.y1
		elif cor_dic["tl"] and cor_dic["tr"]:
			new_outer.y1 = r2.y1
		elif cor_dic["tl"]:
			new_outer.y1 = r2.y1
			new_outer.x1 = r2.x1
		elif cor_dic["tl"] and cor_dic["bl"]:
			new_outer.x1 = r2.x1


		return new_outer

	def update_outer(self, r1, r2, cnt):
		if r1.get_size() < r2.get_size():
			self.update_outer(r2, r1, cnt)
			return

		new_outer = rect(r1.x0, r1.y0, r1.x1, r1.y1)

		intersection, cor_dic = intersects(r1, r2)

		if intersection == True:
			if self.debug == True:
				new_outer.set_colour((100, 255, 0))
			self.boxes[cnt] = new_outer
			return
			

		left = r2.x1 <= r1.x0
		right = r1.x1 <= r2.x0
		bottom = r1.y1 <= r2.y0
		top = r1.y0 >= r2.y1
		position = "nada"

		if left and top:
			position = "left and top"
			new_outer.x0 = r2.x0
			new_outer.y0 = r2.y0
		elif left and bottom:
			position = "left and bottom"
			new_outer.x0 = r2.x0
			new_outer.y1 = r2.y1
		elif bottom and right:
			position = "bottom and right"
			new_outer.y1 = r2.y1
			new_outer.x1 = r2.x1
		elif right and top:
			position = "right and top"
			new_outer.y0 = r2.y0
			new_outer.x1 = r2.x1
		elif left:
			position = "left"
			new_outer.x0 = r2.x0
		elif right:
			position = "right"
			new_outer.x1 = r2.x1
		elif bottom:
			position = "bottom"
			new_outer.y1 = r2.y1			
		elif top:
			position = "top"
			new_outer.y0 = r2.y0

		if self.debug == True:
			new_outer.set_colour((100, 255, 0))

		self.boxes[cnt] = new_outer

	def draw_all(self):
		self.draw("all boxes", self.boxes)

	def print_outers(self):
		print "---------------------------------------------------------------------"
		for o in self.boxes:
			print o
		print "---------------------------------------------------------------------"

	def get_new_coordinates(self, new_rectangle, debug=False):
		self.debug = debug
		# current c_x0, c_x1, c_y0, c_y1, 
		# new x0, y0, x1, y1
		if debug == True:
			new_rectangle.set_colour((0, 0, 255))
			self.draw("about to update with: ", [new_rectangle])

		if len(self.boxes) == 0:
			self.boxes.append(new_rectangle)

			if debug == True:
				new_rectangle.set_colour((100, 255, 0))
				self.draw("first rect", [new_rectangle])

		else:
			if debug == True:
				self.draw("not first current_outers: ", self.boxes)

			boxes_copy = self.boxes[:]
			
			for cnt, b in enumerate(boxes_copy):
				if self.is_in_range(b, new_rectangle):
					if debug == True:
						print "in range, updating outer"
						print "outers before update: "; self.print_outers()

					self.update_outer(b, new_rectangle, cnt)

					if debug == True:
						self.draw("outers after update: ", self.boxes)
						print "outers after update: " ; self.print_outers()
					return
			if debug == True:				
				print "not in range"
				new_rectangle.set_colour((100, 255, 0))

			self.boxes.append(new_rectangle)

			if debug == True:
				self.draw("new outers: ", self.boxes)

	def pad_boxes(self, padding, img_shape):
		# img_shape[0] = height, img_shape[1] = width
		width = img_shape[1]
		height = img_shape[0]

		for i, box in enumerate(self.boxes):
			# check top
			if box.y0 - padding > 0:
				box.y0 = box.y0 - padding
			# check left
			if box.x0 - padding > 0:
				box.x0 = box.x0 - padding
			# check bottom 
			if box.y1 + padding < height:
				box.y1 = box.y1 + padding
			# check right 
			if box.x1 + padding < width:
				box.x1 = box.x1 + padding

			box.set_colour((0, 255, 0))