import numpy as np

def adjust_for_zeors(x):
	ret = [i for i in x if i > 0]
	return np.array(ret)

class hiddenStateInference:

	def __init__(self, config_params):
		'''
			Hidden state inference is a function of the observed histograms for each component and the associated prior weights.
			The inferred states is a vector that sums to 1.
		'''
		#print "inferrring hidden states: "
		#print "inference config: ", config_params

		self.histograms = config_params['observations']
		

		self.color_specifications = config_params["color_specifications"]
		self.inferred_states = {0 : [], 1 : [], 2 : []}

		for Y_idx in self.histograms:
			if len(self.histograms[Y_idx]) != 0:
				self.infer_hidden_state(self.color_specifications[Y_idx], self.histograms[Y_idx], Y_idx)

		

	def infer_hidden_state(self, omega, Y, idx):

		adjusted_params = [(omega[i], Y[i]) if Y[i] + omega[i] > 1.0 else (omega[i], 1.1) for i in range(len(omega))]

		#adjusted_params = [(omega[i], Y[i]) for i in range(len(omega)) if Y[i] + omega[i] > 1.0 ]

		one, two = zip(*adjusted_params)
		omega = one
		Y = two

		#print np.array(omega)
		#print np.array(Y)

		list_to_sum = np.array([Y[i] + omega[i] - 1 for i in range(len(Y))])

		denominator = np.sum(list_to_sum)

		hidden = np.zeros((len(Y)))

		for i in range(len(omega)):
			hidden[i] = (Y[i] + omega[i] - 1) * (1.0 / denominator)

		self.inferred_states[idx] = hidden
		self.color_specifications[idx] = omega
		self.histograms[idx] = Y


	def get_inferred_states(self):
		return (self.inferred_states, self.color_specifications, self.histograms)

