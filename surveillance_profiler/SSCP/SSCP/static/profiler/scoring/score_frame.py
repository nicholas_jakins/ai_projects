import numpy as np
import math
from scipy.stats import multivariate_normal
from scipy.spatial import KDTree
import imutils
import cv2
import matplotlib.pyplot as plt
import Color_model.paramConfig as pc
import itertools
import inference.Infer as I
import scipy.special as sp
import psycopg2
import time
import struct

dict_colors = {0: "White", 1:"Gray", 2:"Black", 3:"Yellow", 4:"Orange", 5:"Pink", 6:"Red", 7:"Green", 8:"Blue", 9:"Purple", 10:"Brown", 11:"Beige"}
#dict_colors = {0:"Gray", 1:"Black", 2:"Yellow", 3:"Orange", 4:"Pink", 5:"Red", 6:"Green", 7:"Blue", 8:"Purple", 9:"Brown", 10:"Beige"}
colors_to_train = [1, 2, 6, 7, 8, 9, 10, 11]

body_part_dict = {0: "head", 1 : "torso", 2 : "legs"}
colors_to_train = {0 : [1, 2, 10], 1 : [0, 1, 2, 6, 7, 8, 9, 10, 11], 2 : [1, 2, 10, 11]}

parts_to_score = [0, 1, 2]

def pack_data(data):
	return struct.pack('d'*len(data), *data)

def restrict_color_observations(vect, indexes_to_consider):
	return [vect[i] for i in indexes_to_consider]

def get_homography():
	f = open("./Homography/homography.py", "r")
	H = f.read()
	f.close()
	str_to_exe = "h = " + H
	str_to_exe = str.replace(str_to_exe, "array", "")
	exec str_to_exe
	#print h
	#BY P
	# find the position of the foot
	#print partitions
	x_foot = int(partitions[2][0] + (partitions[2][2] - partitions[2][0]) / 2.0)
	y_foot = partitions[2][3]

	y_est = int(partitions[0][1])

	coord_head, valid = ground_plane.get_head_position(x_foot, y_foot, y_est)
	#print coord_head, ", ", valid

	#print "x_foot: ", x_foot
	#print "y_foot: ", y_foot

	cv2.line(img, (int(x_foot), int(y_foot)), (int(coord_head[0]), int(coord_head[1])), (255, 255, 255), 1)

'''
	Param indicator: 0 for nelder_mead
					 1 for cumulative
'''
def get_dirichlet():
	f = open("/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/profiler/Dirichlet/trained_mixtures.py")
	
	output = f.read()

	str_to_exe = str.replace(output, "array", "")
	str_to_exe = "D = " + str_to_exe
	exec str_to_exe
	return D

def get_abs_path(s):
	root = "/Users/nicholasjakins/Desktop/test_shared_folder/Footage/"
	#s = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images_from_query/20160916_03140946_120.png"
	image_name = s.split("/", 11)[-1]
	camera_number = image_name[10:11]
	return root + "C" + str(camera_number) + "/" + image_name

def get_histo(img, ROI, color_model, mask, category_arr, bounding_box, db):
	#category_arr = np.array(category_arr)
	#category_arr = category_arr[0][0]
	
	roi = img[ROI[1]:ROI[3], ROI[0]:ROI[2]]
	roi_mask = mask[ROI[1]:ROI[3], ROI[0]:ROI[2]]

	img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

	roi_hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
	#print "roi_hsv.shape: ", roi_hsv.shape

	counts = np.zeros((len(dict_colors),))

	
	for (i, j) in itertools.product(xrange(ROI[0], ROI[2], 1), xrange(ROI[1], ROI[3], 1)):
		try:
			#if image_path == "/Users/nicholasjakins/Desktop/test_shared_folder/Footage/C3/20160916_03152253_1990.png":
			#	debugging_image[j][i] = [255, 0, 0]

			packed_data = pack_data([img_hsv[j][i][0], img_hsv[j][i][1], img_hsv[j][i][2]])
			'''hsv_to_query = "(" + str(roi_hsv[i][j][0]) + ", " + str(roi_hsv[i][j][1]) + ", " + str(roi_hsv[i][j][2]) + ")"
			#if roi_mask[i][j][0] == 255:
			cur.execute("SELECT category FROM hsvobservations WHERE hsvobservations.color = %s::hsv", (hsv_to_query,))
			res = cur.fetchall()
			idx = res[0][0]'''
			#print "idx is: ", idx
			#idx = color_model.get_color_category(roi_hsv[i][j])
			#print "(", i, ", ", j, ")"

			try:
				idx = int(list(db[packed_data])[0])
				#print "idx is: ", idx
				#idx = category_arr[j][i]
			except:
				#print "exception"
				continue

			'''if category_arr[j][i] != -1:
				#print "--------------------------------------------------------"
				display_arr[j][i] = [0, 255, 0]'''

			#print "idx: ", idx
			counts[idx] = counts[idx] + 1
		except IndexError:
			#print "index error: ", i, ", ", j
			continue
	
	total = counts.sum()

	counts = [(i/float(total)) * 100.0 for i in counts]

	return counts

def normalize_vec1(x):
	#return x
	summation = np.sum(x)
	normalized_vec = [(i / float(summation)) for i in x]
	return normalized_vec

def normalize_vec(x):
	#return x
	summation = np.sum(x)
	normalized_vec = [(i / float(summation) * 100) for i in x]
	return normalized_vec

def adjust_for_zeors(x):
	ret = [i for i in x if i > 0]
	return np.array(ret)

	
class score:

	'''
		Accessing dirichlet prior:
			self.D[[head|torso|legs]][[gray]|...|[beige]]
	'''

	def __init__(self, frame_info, image_frame, image_mask, M, C, color_spec, color_model, image_path, db, conn_col_survey, conn_dvr, log_cache, testing=False):

		'''
			Time information: 
		'''
		
		self.testing = testing
		self.db = db
		self.time_to_update_att = 0
		self.time_for_updating_neighbours = 0
		self.database_time = 0
		self.calculation_time = 0
		self.initial_time = 0
		self.whole_score = 0
		self.log_caching = 0

		s_init = time.time()
		self.abs_image_path = image_path
		meta_root = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images_from_query/"
		image_path = frame_info["Image"]
		image_name = image_path.split("/", 8)[-1]
		self.meta_write_dir = meta_root + image_name[0:image_name.find(".")] + "_meta.png"
		self.mask = image_mask
		self.image_path = image_path

		self.body_estimations = {0: [], 1: [], 2: []}
		
		self.log_cache = log_cache

		self.color_model = color_model
		self.image_frame = image_frame
		
		self.frame_info = frame_info
		self.D = get_dirichlet()
		
		self.color_spec = color_spec 


		# normalize the color prior matrix
		self.target_body_colours = {i : dict_colors[color_spec[i]].lower() for i in range(len(body_part_dict))}
		#print "colour spec: ", self.color_spec				
		#print "self.target_body_colours: ", self.target_body_colours


		for part in self.target_body_colours:
			body_col_vector = self.D[body_part_dict[part]][self.target_body_colours[part]]
			if np.sum(body_col_vector) == 0:
				continue
			self.D[body_part_dict[part]][self.target_body_colours[part]] = normalize_vec(body_col_vector)

		self.mean = M
		self.cov = C
		e_init = time.time()
		self.initial_time = self.initial_time + (e_init - s_init)

		s_db = time.time()

		self.cur = conn_col_survey.cursor()
		cur_cslab = conn_dvr.cursor()
		
		self.conn_cslabdvr = conn_dvr
		self.cur_cslab = cur_cslab
		e_db = time.time()
		self.database_time = self.database_time + (e_db - s_db)
		# Get the category array
		#p = get_abs_path(self.abs_image_path)
		#cur_cslab.execute("SELECT categories FROM imagecategories WHERE imagecategories.image_path = (%s)", (p,))
		#self.category_arr = cur_cslab.fetchall()
		#e_db = time.time()
		#self.database_time = self.database_time + (e_db - s_db)
		s = time.time()
		self.score = self.estimate_score(False)
		e = time.time()
		self.whole_score = self.whole_score + (e - s)
		#print "self.time_to_update_att: ", self.time_to_update_att
	
	def approximate_normalization_constant(self, body_part):
		print "------------------Approximating normalization constant----------------------"
		#omega = self.color_parameters[body_part]

		omega = self.D[self.color_spec[1]]

		#omega = np.array([10.3923445, 1.4784689, 2.0, 5.61509835, 11.91121744, 50.24720893, 0.0, 0.0, 0.0, 80.57097289, 6.784689])

		print "trained dirichlet parameter: ", omega

		terms_to_sum = [np.log(sp.gamma(i)) for i in omega]

		t1 = np.sum(terms_to_sum)
		
		t2 = np.log(sp.gamma(np.prod(omega)))
		t3 = t1 - t2
		print "t3: ", t3
		print "log(100)! is: ", self.fact_sum(100)
		print "---------------------------------------------------------------------------"
		return t3 + self.fact_sum(100)
		
	def update_body_part(self, img, ROI_dim, row_index, show_partition, bounding_box):
		# need index into row 
		x0 = ROI_dim[0]
		x1 = ROI_dim[1]
		y0 = ROI_dim[2]
		y1 = ROI_dim[3]

		counts = get_histo(img, [int(x0), int(y0), int(x1), int(y1)], self.color_model, self.mask, None, bounding_box, self.db)

		counts = [counts[i] for i in colors_to_train[row_index]]							# observations for the body part

		counts = normalize_vec(counts)											# normalize to sum to 100

		self.observed_histograms[row_index] = np.array(counts)						

		

	def update_attributes(self, partition_est, show_partition=False):
			
		box = self.frame_info["Boxes"][0]
		
		x1_per = (box[0] / float(self.image_frame.shape[1])) * 100.0
		y1_per = (box[1] / float(self.image_frame.shape[0])) * 100.0
		x2_per = (box[2] / float(self.image_frame.shape[1])) * 100.0
		y2_per = (box[3] / float(self.image_frame.shape[0])) * 100.0

		img = imutils.resize(self.image_frame, width=300)
		
		width = img.shape[1]
		height = img.shape[0]

		x_1 = (x1_per / 100.0) * width
		y_1 = (y1_per / 100.0) * height
		x_2 = (x2_per / 100.0) * width
		y_2 = (y2_per / 100.0) * height
		
		box = [int(x_1), int(y_1), int(x_2), int(y_2)]
		
		img_w = img.shape[0]
		img_h = img.shape[1]
		box_w = box[2] - box[0]
		box_h = box[3] - box[1]

		partitions = []

		inference_config = {"observations" : {}, "color_specifications" : {}}
		self.observed_histograms = {0 : [], 1: [], 2: []}
		
		img_to_show = img
		img_RGB = cv2.cvtColor(img_to_show, cv2.COLOR_BGR2RGB)					# opencv2 uses BGR

		print "width scoring: ", width
		print "height scoring: ", height

		body_count = 0
		for i in xrange(0, 12, 4):

			x0 = box[0] + ((partition_est[i] / 100.0) * box_w)
			y0 = box[1] + ((partition_est[i+1] / 100.0) * box_h)
			x1 = x0 + ((partition_est[i+2] / 100.0) * box_w)
			y1 = y0 + ((partition_est[i+3] / 100.0) * box_h)

			partitions.append([int(x0), int(y0), int(x1), int(y1)])

			self.update_body_part(img, (x0, x1, y0, y1), body_count, show_partition, box)

			self.body_estimations[body_count] = [x0, x1, y0, y1]

			body_count = body_count + 1

		self.color_parameters = {i : restrict_color_observations(self.D[body_part_dict[i]][self.target_body_colours[i]], colors_to_train[i]) for i in range(len(self.target_body_colours))}
		print "self.color_parameters: ", self.color_parameters

		inference_config["observations"] = self.observed_histograms
		inference_config["color_specifications"] = self.color_parameters

		inference_obj = I.hiddenStateInference(inference_config)
		(hidden_states, color_specifications, histograms) = inference_obj.get_inferred_states()
		
		self.hidden_states = hidden_states
				

	def get_neighbors(self, current_estimate):
		samples = np.random.multivariate_normal(self.mean, self.cov, 100)

		kd_tree = KDTree(samples, leafsize=10)
		result = kd_tree.query(current_estimate, k=5)

		neighbors = [kd_tree.data[i] for i in result[1]]
		return neighbors

	def store_scores(self, score):
		image_path = self.image_path 
		
		head_histo = np.array(self.observed_histograms[0]).tolist()
		torso_histo = np.array(self.observed_histograms[1]).tolist()
		legs_histo = np.array(self.observed_histograms[2]).tolist()
		bb = self.frame_info["Boxes"][0]

		box = (str(bb[0]), str(bb[1]), str(bb[2]), str(bb[3]))

		self.cur_cslab.execute("SELECT EXISTS(SELECT 1 FROM scores where image_path = (%s))", (self.image_path,))
		is_in = self.cur_cslab.fetchall()[0][0]
		if is_in:
			self.cur_cslab.execute("DELETE FROM scores WHERE scores.image_path = (%s)", (self.image_path,))
			self.cur_cslab.execute("INSERT INTO scores(image_path, box, head_histo, torso_histo, legs_histo, score) VALUES (%s, %s::bounding_box, %s, %s, %s, %s)", (image_path, box, head_histo, torso_histo, legs_histo, score))
		else:
			self.cur_cslab.execute("INSERT INTO scores(image_path, box, head_histo, torso_histo, legs_histo, score) VALUES (%s, %s::bounding_box, %s, %s, %s, %s)", (image_path, box, head_histo, torso_histo, legs_histo, score))			
		

	def estimate_score(self, show_partition):
		#print "estimating score now"
		
		current_estimate = self.mean

		# cache results for log factorial
		s_cache = time.time()
		#log_cache = np.zeros((101,))
		#temp = [self.fact_sum(i) for i in xrange(1, 101)]
		#log_cache[1:101] = temp
		e_cache = time.time()
		self.log_caching = self.log_caching + (e_cache - s_cache)
		#print "log_cache: ", log_cache

		for i in range(1):
			s_n = time.time()
			neighbors = self.get_neighbors(current_estimate)
			e_n = time.time()
			self.time_for_updating_neighbours = self.time_for_updating_neighbours + (e_n - s_n)

			for n in neighbors:
				s = time.time()
				self.update_attributes(n, show_partition)					# sets observed color histograms and infers the new states
				e = time.time()
				#print "the time: ", (e - s)
				self.time_to_update_att = self.time_to_update_att + (e - s)
				

				s0 = multivariate_normal.pdf(n, mean=self.mean, cov=self.cov)

				log_s0 = np.log(s0)
				
				s = 0

				s_calc = time.time()
				for Y_idx in self.observed_histograms:
					if Y_idx in parts_to_score:

						norm_const = 0
						histo = self.observed_histograms[Y_idx]
						histo = [self.log_cache[i] for i in histo]
						T1 = -1 * np.sum(histo)
						
						modified_weight_vector = self.color_parameters[Y_idx]
						terms_to_sum = [(self.observed_histograms[Y_idx][i] + modified_weight_vector[i] - 1) \
								* np.log(self.hidden_states[Y_idx][i]) for i in range(len(self.observed_histograms[Y_idx]))]

						terms_to_sum = [0 if math.isnan(i) == True else i for i in terms_to_sum]
						T2 = np.sum(terms_to_sum)
						s = s + (T1 + T2)

				e_calc = time.time()
				self.calculation_time = self.calculation_time + (e_calc - s_calc)

				
				if self.testing == False:
					s_db = time.time()
					self.store_scores(s + s0)
					e_db = time.time()
					self.database_time = self.database_time + (e_db - s_db)

				break

				# store scoring information
				
				
		self.conn_cslabdvr.commit()		

		return s + s0
		


		