/*app.filter('customDate', function() {
   return function(input_date) {
       day 
       month
       year
   } 
});*/



app.controller('results_controller', ['$scope', '$http', '$window', '$location', '$timeout', 'growl', '$filter', '$uibModal', function($scope, $http, $window, $location, $timeout, growl, $filter, $uibModal) {
    
    $scope.logout_results = function() {
        $window.location.href = "http://localhost:8000/surveillance_interface/"
    }
    
    $scope.open_metainfo_modal = function () {
        
        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'myMetaModalContent.html',
          scope: $scope,
          size: 'lg'
        });
    }
    
    $scope.show_meta_info = function(img_name, sco) {
        
        console.log("processing score now: " + img_name + ", " + sco);
        
        var param = {
            "image_name" : img_name
        }
        
        $http.post('http://localhost:8000/surveillance_interface/getScore/', param)
                .success(function(response_data, status) {
                
            var path = img_name.substring(0, img_name.indexOf(".", 5)) + "_meta.png"
            $scope.meta_path = path
            $scope.meta_score = sco
            $scope.open_metainfo_modal()
                
        });
        //alert("img_name: " + img_name);
        
        
    }
    
    
    
    
    $scope.goTo = function(elem_id) {
            
            /*var topPos = document.getElementById(elem_id).offsetTop;
            document.getElementById('carousels').scrollTop = topPos-2;*/

        }
    
    $scope.query = $scope.$root.input_profile
        
    $scope.time_index = ['12am', '01am', '02am', '03am', '04am', '05am', '06am', '08am', '09am', '10am', '11am', '12pm', '01pm', '02pm', '03pm', '04pm', '05pm', '06pm', '07pm', '08pm', '09pm', '10pm', '11pm']
    
    
     $scope.images_1 = [{active : true, path : "../static/images_from_query/20160727_03123016_0.png", hour : 2, view_flag : true},
                       {active : false, path : "../static/images_from_query/20160727_03123016_10.png", hour : 2, view_flag : true}]
    
    $scope.images_2 = [{active : true, path : "../static/images_from_query/20160728_03141907_0.png", hour : 6, view_flag : true},
                       {active : false, path : "../static/images_from_query/20160728_03141907_10.png", hour : 6, view_flag : true}]
    
    
    $scope.Sliders = [{
        min: 0000,
        max: 2359,
        options: {
            id : "images_1",
            floor: 0,
            ceil: 450,
            stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
            translate: function(value) {
                return value.substring(0,2) + ":00" + value.substring(2,4);
            },
            showTicks: true,
            showTicksValues: false,
            onEnd: function(sliderId, modelValue, highValue) {
                //console.log("the slider ID is: " + sliderId + ", " + modelValue + ", " + highValue)
                //console.log("scope.id: " + $scope[sliderId][0].path)
                //$scope[sliderId][0].path = "garbage"
                var lower_hour = $scope.time_index.indexOf(modelValue)
                var upper_hour = $scope.time_index.indexOf(highValue)
                console.log("lower: " + lower_hour);
                console.log("upper: " + upper_hour);       
                
                //get the list of image paths
                console.log($scope[sliderId].length);
                for (i = 0; i < $scope[sliderId].length; i++) {
                    if ($scope[sliderId][i].hour >= lower_hour && $scope[sliderId][i].hour <= upper_hour) {
                        $scope[sliderId][i].view_flag = true
                    } else {
                        $scope[sliderId][i].view_flag = false
                    }
                }
            }
        }
    }, {
        min: 0000,
        max: 2359,
        options: {
            id : "images_2",
            floor: 0,
            ceil: 450,
            stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
            translate: function(value) {
                return value.substring(0,2) + ":00" + value.substring(2,4);
            },
            showTicks: true,
            showTicksValues: false,
            onEnd: function(sliderId, modelValue, highValue) {
                
                //console.log("the slider ID is: " + sliderId + ", " + modelValue + ", " + highValue)
                //console.log("scope.id: " + $scope[sliderId][0].path)
                //$scope[sliderId][0].path = "garbage"
                var lower_hour = $scope.time_index.indexOf(modelValue)
                var upper_hour = $scope.time_index.indexOf(highValue)
                
                console.log("lower: " + lower_hour);
                console.log("upper: " + upper_hour);
                
                
                //get the list of image paths
                console.log($scope[sliderId].length);
                for (i = 0; i < $scope[sliderId].length; i++) {
                   
                    console.log("hour of image: " + $scope[sliderId][i].hour)
                    if (($scope[sliderId][i].hour >= lower_hour) && ($scope[sliderId][i].hour <= upper_hour)) {
                        $scope[sliderId][i].view_flag = true
                    } else {
                        $scope[sliderId][i].view_flag = false
                    }
                }
                
            }
        }
    }]
    
                   
    
    $scope.data = [{
        day : "20160728",
        slider : $scope.Sliders[0],
        images : $scope.images_1
    },
    {
        day : "20160729",
        slider : $scope.Sliders[1],
        images : $scope.images_2
    }];
    
    $scope.cameras = $scope.$root.results.cams
    $scope.navbar_elements = []
    
    //generate the navbar elements
    for (i = 0; i < $scope.cameras.length; i++) {
        //{"name" : "Camera 1", "id_to_jump" : "section1"}
        //console.log("the index is: " + i);
        var name = "Camera " + String($scope.cameras[i])
        var id_to_jump = "section" + String($scope.cameras[i])
        $scope.navbar_elements[i] = {"name" : name, "id_to_jump" : id_to_jump}
    }
    
    $scope.results_data = $scope.$root.results.data
    
    //generate details
    $scope.details = {}
    
    $scope.GetDetails = function(obj, idx) {
            //alert("index number is: " + idx)
            $scope.details[idx] = obj;
    }
    
    for (i = 0; i < $scope.cameras.length; i++) {
        
        var cam = $scope.cameras[i]
        
        $scope.details[cam] = $scope.results_data[cam][0] 
        
        /*var copy_element_in = function(j) {
           var num_images = $scope.$root.results.data[cam][j].images.length 
           for (k = 0; ) {
               
           }
        }*/
        
        //alert($scope.results_data[$scope.cameras[i]].length)
        for (j = 0; j < $scope.results_data[$scope.cameras[i]].length; j++) {
            
            //alert("cam is: " + cam);
            var Id = String(j) + "_" + String(cam) 
            var number_images = $scope.$root.results.data[cam][j].images.length
            //console.log("number of images: " + $scope.$root.results.data[cam][j].day + ", " + number_images);
            $scope.$root.results.data[cam][j].slider = {
                 min: 1,
                 max: number_images,
                 options: {
                     id : Id,
                     floor: 1,
                     ceil: number_images,
                     step: 1,
                     onEnd: function(sliderId, modelValue, highValue) {
                         
                         //console.log("sliderId is: " + sliderId);
                         //console.log("lower_value: " + modelValue + ", higher: " + highValue);
                         // Get the images
                         split_id = sliderId.split("_")
                         var cam_num = split_id[1]
                         var idx_into_data = parseInt(split_id[0])
                         var num_images = $scope.$root.results.data[cam_num][idx_into_data].images.length
                         
                         $scope.$root.results.data[cam_num][idx_into_data].actual_list = []
                         var actual_list_counter = 0
                         for (i = 0; i < num_images; i++) {
                           
                            if ((i >= modelValue - 1) && (i <= highValue - 1)) {
                                $scope.$root.results.data[cam_num][idx_into_data].images[i].view_flag = true
                                if (actual_list_counter == 0) {
                                    $scope.$root.results.data[cam_num][idx_into_data].actual_list[actual_list_counter] = {
                                        "path" : $scope.$root.results.data[cam_num][idx_into_data].images[i].path,
                                        "view_flag" : true,
                                        "score" : $scope.$root.results.data[cam_num][idx_into_data].images[i].score
                                    }
                                    //console.log("counter: " + actual_list_counter);
                                    actual_list_counter = actual_list_counter + 1
                                } else {
                                   $scope.$root.results.data[cam_num][idx_into_data].actual_list[actual_list_counter] = {
                                        "path" : $scope.$root.results.data[cam_num][idx_into_data].images[i].path,
                                        "view_flag" : false,
                                       "score" : $scope.$root.results.data[cam_num][idx_into_data].images[i].score
                                    } 
                                   //console.log("counter: " + actual_list_counter);
                                   actual_list_counter = actual_list_counter + 1
                                }
                            } else {
                                $scope.$root.results.data[cam_num][idx_into_data].images[i].view_flag = false                            
                            }
                        }
                        //console.log("Updated actual_list is: " + $scope.$root.results.data[cam_num][idx_into_data].actual_list)
                     }
                 }
                
            }
            
            /*$scope.$root.results.data[cam][j].slider = {
                min: 0000,
                max: 2359,
                options: {
                    id : Id,
                    floor: 0,
                    ceil: 450,
                    stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
                    translate: function(value) {
                        return value.substring(0,2) + ":00" + value.substring(2,4);
                    },
                    showTicks: true,
                    showTicksValues: false,
                    onEnd: function(sliderId, modelValue, highValue) {
                
                        //$scope[sliderId][0].path = "garbage"
                        var lower_hour = $scope.time_index.indexOf(modelValue)
                        var upper_hour = $scope.time_index.indexOf(highValue)
                
                        console.log("lower: " + lower_hour);
                        console.log("upper: " + upper_hour);
                        split_id = sliderId.split("_")
                        
                        var cam_num = split_id[1]
                        console.log("the CAM number is: " + cam_num);
                                    
                        var idx_into_data = parseInt(split_id[0])
                        console.log("idx_into_data: " + idx_into_data);
                        //get the images
                        var num_images = $scope.$root.results.data[cam_num][idx_into_data].images.length
                        
                        console.log("number of images: " + num_images);
                        
                        for (i = 0; i < num_images; i++) {
                            
                            //get hour
                            var h = $scope.$root.results.data[cam_num][idx_into_data].images[i].hour
                        
                            if ((h >= lower_hour) && (h <= upper_hour)) {
                                $scope.$root.results.data[cam_num][idx_into_data].images[i].view_flag = true
                            } else {
                                $scope.$root.results.data[cam_num][idx_into_data].images[i].view_flag = false                            
                            }
                        }
                
                    }
                }
            
            }*/
        }
    }
         
        
        
        
    
    
    /*$scope.results_data = $scope.$root.results.C3
    
    alert("length: " + $scope.results_data.length)
    //Need to create the slider
    for (i = 0; i < $scope.results_data.length; i++) {
        //day object, create the slider
        //ID must be index of results
        console.log("the date: " + $scope.$root.results.C3[i].day);
        console.log("the string: " + String(i))
        
        $scope.$root.results.C3[i].slider = { 
            min: 0000,
            max: 2359,
            options: {
                id : String(i),
                floor: 0,
                ceil: 450,
                stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
                translate: function(value) {
                    return value.substring(0,2) + ":00" + value.substring(2,4);
                },
                showTicks: true,
                showTicksValues: false,
                onEnd: function(sliderId, modelValue, highValue) {
                
                    //$scope[sliderId][0].path = "garbage"
                    var lower_hour = $scope.time_index.indexOf(modelValue)
                    var upper_hour = $scope.time_index.indexOf(highValue)
                
                    console.log("lower: " + lower_hour);
                    console.log("upper: " + upper_hour);
                
                
                    //get the list of image paths
                    var idx_into_data = parseInt(sliderId)
                    //alert("index into data: " + idx_into_data);
                    //get the images
                    var num_images = $scope.$root.results.C3[idx_into_data].images.length
                    console.log("number of images: " + num_images);
                    for (i = 0; i < num_images; i++) {
                            
                        //get hour
                        var h = $scope.$root.results.C3[idx_into_data].images[i].hour
                        
                        if ((h >= lower_hour) && (h <= upper_hour)) {
                            $scope.$root.results.C3[idx_into_data].images[i].view_flag = true
                        } else {
                            $scope.$root.results.C3[idx_into_data].images[i].view_flag = false                            
                        }
                    }
                
                }
            }
        }
        
    }*/
    
    console.log($scope.$root.results)
    
    
    
    
    $scope.get_keys = function(dict) {
        var keys = [];
        for (var key in dict) {
            if (dict.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys
    }
    
    
    var generate_slider = function(slider_name) {
        var slider = {
            min: 0000,
            max: 2359,
            options: {
            floor: 0,
            ceil: 450,
            stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
            translate: function(value) {
                return value.substring(0,2) + ":00" + value.substring(2,4);
            },
            showTicks: true,
                showTicksValues: false
            }
        };
        return slider
    }
    
    $scope.arr = [1, 2]
    
    $scope.thing = '<tab heading="day1" select="refreshSlider()"><rzslider rz-slider-model="slider1.min" rz-slider-high="slider1.max" rz-slider-options=" slider1.options"></rzslider></tab><tab heading="day2" select="refreshSlider()"><rzslider rz-slider-model="slider2.min" rz-slider-high="slider2.max" rz-slider-options=" slider2.options"></rzslider></tab>'
    
    console.log("in the results controller")
    //console.log("books.data: " + books.data);
    /*function($scope, books) {
      $scope.books = books.data;
      
    }]);
    
    
    console.log("Data from books is: " + $scope.books)*/
    
    
    $scope.refreshSlider = function () {
        $timeout(function () {
            $scope.$broadcast('rzSliderForceRender');
        });
    };
    
    $scope.day_tabs = ""
    
    $scope.add_day_tab = function(slider_name, day) {
        
        
        
        /*
        
        <rzsliderrz-slider-model="slider1.min"rz-slider-high="slider1.max"rz-slider-options="slider1.options"></rzslider>
        */
        
        var tab_content = '<rzslider ' + 'rz-slider-model=\"' + slider_name + '.min\" ' + 'rz-slider-high=\"' + slider_name +                                   '.max\" ' + 'rz-slider-options=\" ' + slider_name + '.options\"></rzslider>'
        
        
        
        var tab = '<tab heading=\"' + day + '\" select="refreshSlider()">' + tab_content + '</tab>'
        
        $scope.day_tabs = $scope.day_tabs + tab
        
    }
    
    
    $scope.simple_slider = {
        value: 10,
        options: {
            showSelectionBar: true
        }
    };
    
    $scope.tabSliders = {
        slider1: {
            value: 100
        },
        slider2: {
            value: 200
        }
    };
    
    
    
    $scope.yay = function() {
        //alert("something");
    }
    // In your controller
    $scope.slider1 = {
        min: 0000,
        max: 2359,
        options: {
            floor: 0,
            ceil: 450,
            stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
            translate: function(value) {
                return value.substring(0,2) + ":00" + value.substring(2,4);
            },
            showTicks: true,
            showTicksValues: false
        }
    };
    
    
    $scope.slider2 = {
        min: 0000,
        max: 2359,
        options: {
            floor: 0,
            ceil: 450,
            stepsArray: '12am:01am:02am:03am:04am:05am:06am:08am:09am:10am:11am:12pm:01pm:02pm:03pm:04pm:05pm:06pm:07pm:08pm:09pm:10pm:11pm'.split(':'),
            translate: function(value) {
                return value.substring(0,2) + ":00" + value.substring(2,4);
            },
            showTicks: true,
            showTicksValues: false
        }
    };
    
    
    $scope.sliders = [$scope.slider1, $scope.slider2]
    
    $scope.add_day_tab("slider1", "day1");
    $scope.add_day_tab("slider2", "day2");
    
    
    
    //$scope.slidertab ='<rzslider rz-slider-model="slider.min" rz-slider-high="slider.max" rz-slider-options="slider.options"></rzslider>'
    
    $scope.priceSlider = 150;
    
    console.log("in results controller")
    $scope.num_cams = 0
    $scope.images = []
    $scope.cameras = $scope.$root.results.cams
        
    
    $scope.navigate_back = function() {
        $window.location.href = "http://localhost:8000/surveillance_interface/#input"    
    }
    
    
    var imgs = $scope.$root.results.images
    
    $scope.tabSliders = []
        
    $scope.day_tabs = []
    
    
    
    
    // Get the camera numbers
    /*var camera_numbers = $scope.$root.results.cams
    $scope.num_cameras = camera_numbers.length
    
    for (i = 0; i < $scope.num_cameras; i++) {
        var c_num = "C" + String(camera_numbers[i])
        console.log("c_num is: " + c_num)
        
        var camera = $scope.$root.results.images[c_num]
        var days_val = $scope.get_keys(camera)
        console.log("days are: " + days_val);
        
        var days_active = []
        
        for (day in days_val) {
            console.log("day is: " + day + "\n");
            if (day == 0) {
                days_active[day] = {active : true, 
                                    value : days_val[day]}
            } else {
                days_active[day] = {active : false, 
                                    value : days_val[day]}
            }
        }
        
        $scope.day_tabs[i] = {camera_num : c_num,
                              tabs : days_active}
        
        //$scope.tabSliders[i] = slider
    }
    
    $scope.tabs = $scope.day_tabs[0].tabs
    
    console.log($scope.day_tabs);*/
    
    
    
    /*var settings = {
        total : 4,
        counts : ,
        defaultSort : ,
        groupBy : ,
        data : ,
    }
    
    $scope.tableParams = new ngTableParams(parameters, settings)*/
    
    //$window.location.href = "http://localhost:8000/surveillance_interface/#resultsview"
    //console.log("The thing: " + $scope.$root.results.images[ss])
    
    /*var cam_num = $scope.$root.results.cams
    $scope.num_cams = $scope.$root.results.cams.length
    
    for (i = 0; i < $scope.$root.results.cams.length; i++) {
        
        console.log("cam_num[i]: " + cam_num[i])
        var c_num = "C" + String(cam_num[i])
        console.log("c_num is: " + c_num)
        
        var image_names = $scope.$root.results.images[c_num]
        console.log("image_names: " + image_names)
        
        var img_names = []
        for (j = 0; j < image_names.length; j++) {
            
            console.log("image path: " + image_names[j] + "\n");
            
            var is_active = 0
            if (j == 0) {
                is_active = 1
            }
            
            img_names[j] = {"path" : image_names[j], 
                            "active" : is_active}
            
        }
            
        console.log("img_names: " + img_names);
        $scope.images[i] = img_names
        
        
    }
    console.log("$scope.images.length: " + $scope.images.length);*/
    
    
    
}]);