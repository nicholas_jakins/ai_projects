app.service(
            "modals",
            function( $rootScope, $q ) {
                // I represent the currently active modal window instance.
                var modal = {
                    deferred: null,
                    params: null
                };
                // Return the public API.
                return({
                    open: open,
                    params: params,
                    proceedTo: proceedTo,
                    reject: reject,
                    resolve: resolve
                });
                // ---
                // PULBIC METHODS.s
                // ---
                // I open a modal of the given type, with the given params. If a modal
                // window is already open, you can optionally pipe the response of the
                // new modal window into the response of the current (cum previous) modal
                // window. Otherwise, the current modal will be rejected before the new
                // modal window is opened.
                function open( type, params, pipeResponse ) {
                    var previousDeferred = modal.deferred;
                    // Setup the new modal instance properties.
                    modal.deferred = $q.defer();
                    modal.params = params;
                    // We're going to pipe the new window response into the previous
                    // window's deferred value.
                    if ( previousDeferred && pipeResponse ) {
                        modal.deferred.promise
                            .then( previousDeferred.resolve, previousDeferred.reject )
                        ;
                    // We're not going to pipe, so immediately reject the current window.
                    } else if ( previousDeferred ) {
                        previousDeferred.reject();
                    }
                    // Since the service object doesn't (and shouldn't) have any direct
                    // reference to the DOM, we are going to use events to communicate
                    // with a directive that will help manage the DOM elements that
                    // render the modal windows.
                    // --
                    // NOTE: We could have accomplished this with a $watch() binding in
                    // the directive; but, that would have been a poor choice since it
                    // would require a chronic watching of acute application events.
                    $rootScope.$emit( "modals.open", type );
                    return( modal.deferred.promise );
                }
                // I return the params associated with the current params.
                function params() {
                    return( modal.params || {} );
                }
                // I open a modal window with the given type and pipe the new window's
                // response into the current window's response without rejecting it
                // outright.
                // --
                // This is just a convenience method for .open() that enables the
                // pipeResponse flag; it helps to make the workflow more intuitive.
                function proceedTo( type, params ) {
                    return( open( type, params, true ) );
                }
                // I reject the current modal with the given reason.
                function reject( reason ) {
                    if ( ! modal.deferred ) {
                        return;
                    }
                    modal.deferred.reject( reason );
                    modal.deferred = modal.params = null;
                    // Tell the modal directive to close the active modal window.
                    $rootScope.$emit( "modals.close" );
                }
                // I resolve the current modal with the given response.
                function resolve( response ) {
                    if ( ! modal.deferred ) {
                        return;
                    }
                    modal.deferred.resolve( response );
                    modal.deferred = modal.params = null;
                    // Tell the modal directive to close the active modal window.
                    $rootScope.$emit( "modals.close" );
                }
            }
        );


app.directive(
            "bnModals",
            function( $rootScope, modals ) {
                // Return the directive configuration.
                return( link );
                // I bind the JavaScript events to the scope.
                function link( scope, element, attributes ) {
                    // I define which modal window is being rendered. By convention,
                    // the subview will be the same as the type emitted by the modals
                    // service object.
                    scope.subview = null;
                    // If the user clicks directly on the backdrop (ie, the modals
                    // container), consider that an escape out of the modal, and reject
                    // it implicitly.
                    element.on(
                        "click",
                        function handleClickEvent( event ) {
                            if ( element[ 0 ] !== event.target ) {
                                return;
                            }
                            scope.$apply( modals.reject );
                        }
                    );
                    // Listen for "open" events emitted by the modals service object.
                    $rootScope.$on(
                        "modals.open",
                        function handleModalOpenEvent( event, modalType ) {
                            scope.subview = modalType;
                        }
                    );
                    // Listen for "close" events emitted by the modals service object.
                    $rootScope.$on(
                        "modals.close",
                        function handleModalCloseEvent( event ) {
                            scope.subview = null;
                        }
                    );
                }
            }
        );





app.directive('scrollToItem', function () {
    return {
        restrict: 'A',
        scope: {
            scrollTo: "@"
        },
        link: function (scope, $elm, attr) {

            $elm.on('click', function () {
                $('html,body').animate({
                    scrollTop: $(scope.scrollTo).offset().top
                }, "slow");
            });
        }
    }
})

app.controller('input_controller', ['$scope', '$http', '$window', '$location', '$timeout', 'growl', '$uibModal', '$modal', '$filter', 'modals', '$log', 'ngProgressFactory', '$document', function($scope, $http, $window, $location, $timeout, growl, $uibModal, $modal, $filter, modals, $log, ngProgressFactory, $document) {
        
        
        $scope.$root.bodyStyle = {background: "url(../static/image_plain.jpg) no-repeat center center fixed"};
    
        $scope.logout = function() {
            $window.location.href = "http://localhost:8000/surveillance_interface/"
        }
        
        /*
            Init input variables
        */
        $scope.progressbar = ngProgressFactory.createInstance();
        
        $scope.progressbar.reset();
    
        $scope.startdate_obj = null
        $scope.enddate_obj = null
        $scope.starttime_obj = null
        $scope.endtime_obj = null
        
        $scope.goTo = function(elem_id) {
            
            var topPos = document.getElementById(elem_id).offsetTop;
            document.getElementById('carousels').scrollTop = topPos-2;

        }
    
        $scope.jump = function(elem_id) {
            
            console.log("id is: ", elem_id);
            var getPosition = function (element) {
                var e = document.getElementById(element);
                var left = 0;
                var top = 0;

                do {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                } while (e = e.offsetParent);

                return [left, top];
            }

            $window.scrollTo(getPosition(elem_id));  
            
        }
    
        $scope.rel = function (h) {
            
                var url = location.href; //Save down the URL without hash.
                location.href = "#" + h; //Go to the target element.
            history.replaceState(null, null, url); //Don't like hashes. Changing it back.
        }
    
        
    
    
    $scope.images1 = [
                        {"path" : "../static/images/cam1.png", "active" : true},
                        {"path" : "../static/images/cam2.png", "active" : false}
                    ]
    $scope.images2 = [
                        {"path" : "../static/images/cam3.png", "active" : true},
                        {"path" : "../static/images/cam4.png", "active" : false}
                    ]
    
    $scope.images3 = [
                        {"path" : "../static/images/cam3.png", "active" : true},
                        {"path" : "../static/images/cam4.png", "active" : false}
                    ]
    
    $scope.selectedCamera = "torso"
    
    $scope.cameras = [1, 2, 3, 4]
    
    $scope.camera_box = {
        1 : null,
        2 : null,
        3 : null,
        4 : null
    }
    
  $scope.items = ['item1', 'item2', 'item3'];

  $scope.animationsEnabled = true;

  $scope.open_date_modal = function (size) {
       var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myDateModalContent.html',
      controller: 'ModalDateInstanceCtrl',
      controllerAs: 'this',
      size: 'lg',
      resolve: {
        items: function () {
          return size;
        }
      }
    });
      
    
    modalInstance.result.then(function (query_params) {
        
        console.log("query_params.startdate: " + query_params.startdate);
        $scope.startdate = query_params.startdate
        $scope.startdate_obj = query_params.startdate_obj
        $scope.enddate = query_params.enddate 
        $scope.enddate_obj = query_params.enddate_obj
        $scope.starttime = query_params.starttime
        $scope.starttime_obj = query_params.starttime_obj
        $scope.endtime = query_params.endtime
        $scope.endtime_obj = query_params.endtime_obj
        
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
        
    });
  }
    
  $scope.open = function (size) {
      
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: 'this',
      size: size,
      resolve: {
        items: function () {
          return size;
        }
      }
        
    });
      
    $scope.COORDS = {
        
        "cam1" : null,
        "cam2" : null,
        "cam3" : null,
        "cam4" : null
    }

    modalInstance.result.then(function (COORD) {
            
        $scope.camera_box[$scope.selectedCamera] = COORD
        console.log("camera boxes: " + $scope.camera_box);
        
    }, function () {
        $log.info('Modal dismissed at: ' + new Date());
        
        });
  };
    
    $scope.openModal = function() {
        
        $scope.open($scope.selectedCamera) 
    }
    
    
        
    $scope.showSuccess = function(msg, title) {
        growl.success(msg,{title: title});
    }
                
    $scope.showError = function(msg, title) {
        growl.error(msg, {title: title});
    }
                
    $scope.showWarning = function(msg, title) {
        growl.warning(msg, {title: title});
    }
    
    $scope.hide_time = true
    
    console.log("In the input controller");
    
    $scope.colors_hair = [{name: "Gray"}, 
                     {name: "Black"}, 
                     {name: "Brown"}]
    
    $scope.colors_torso = [{name: "White"}, 
                     {name: "Black"}, 
                     {name: "Red"}, 
                     {name: "Green"}, 
                     {name: "Blue"}, 
                     {name: "Purple"}, 
                     {name: "Brown"}, 
                     {name: "Beige"}]
    
    $scope.colors_legs = [{name: "Gray"}, 
                     {name: "Black"}, 
                     {name: "Brown"}, 
                     {name: "Beige"}]
    
    $scope.races = [{name: "Caucasian"},
                     {name: "African"}]
    
    $scope.hairColor = {"name": "----"}
    $scope.torsoColor = {"name": "----"}
    $scope.legsColor = {"name": "----"}    
   
    
    $http.post('http://localhost:8000/surveillance_interface/getcameras/', null)
                .success(function(response_data, status) {
        
        console.log("response date: " + response_data)
        $scope.roles = response_data
        console.log($scope.roles);
        
    });
    
    
    $scope.user = {
        roles: []
    };
    
    
    $scope.selectedCam = 100
    
    var isInArray = function(arr, val) {
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                return true
            }
        }
        return false
    }
    
    $scope.changedCam = function(cam) {
        console.log("changing camera!!: " + cam);
        
        if (isInArray($scope.user.roles, cam)) {
        
        } else {
            $scope.selectedCamera = cam
            $scope.open(cam)
        }
        
        
        
    }
    
    
    /*
    rendering query
    */
    
    $scope.render_query = function() {
        
        var check_colours = function() {
            if ($scope.hairColor.name == "----") {
                $scope.showError("Invalid query", "Please select colour for hair")
                return true
            } else if ($scope.torsoColor.name == "----") {
                $scope.showError("Invalid query", "Please select colour for torso")
                return true
            } else if ($scope.legsColor.name == "----") {
                $scope.showError("Invalid query", "Please select colour for legs")
                return true
            }
            return false
        }
        
        var check_dates_selected = function() {
            
            if ($scope.startdate_obj == null) {
                $scope.showError("Invalid query", "Please select a start date")
                return true
            } else if ($scope.enddate_obj == null) {
                $scope.showError("Invalid query", "Please select a end date")
                return true
            } else if ($scope.starttime_obj == null) {
                $scope.showError("Invalid query", "Please select a start time for start date")
                return true
            } else if ($scope.endtime_obj == null) {
                $scope.showError("Invalid query", "Please select a end time for end date")
                return true
            }
            return false            
        }
        
        var check_date_order = function () {
            //alert($scope.starttime_obj.getTime() + ", " + $scope.enddate_obj.getTime())
            if ($scope.startdate_obj.getTime() > $scope.enddate_obj.getTime()) {
                $scope.showError("Invalid query", "End date is before start date")
                return true
            } 
            /*if ($scope.startdate_obj.getTime() == $scope.enddate_obj.getTime()) {
                if ($scope.endtime_obj < $scope.starttime_obj) {
                    $scope.showError("Invalid query", "Select end time to be later than start time")
                    return true
                }
            }*/
            return false
        }
        
        var check_cameras = function() {
            if ($scope.user.roles.length == 0) {
                $scope.showError("Invalid query", "Please select a camera")
                return true
            }
            return false
        }
        
        //Error handling
        /*
        1) no colour selected for hair, torso or legs
        2) No dates selected
        3) No times selected
        4) second date is before the first date
        5) dates are the same by time 2 is smaller 
        6) check camera subset
        */
        
        if (check_colours() == true) {
            return
        }
        if (check_dates_selected() == true) {
            return 
        }
        if (check_date_order() == true) {
            return
        }
        if (check_cameras() == true) {
            return
        }
        $scope.progressbar.reset();
        var param = {
            "haircolor" : $scope.hairColor.name,
            "torsocolor" : $scope.torsoColor.name,
            "legscolor" : $scope.legsColor.name,
            "startdate" : $scope.startdate,
            "enddate" : $scope.enddate,
            "starttime" : $scope.starttime,
            "endtime" : $scope.endtime,
            "camerasubset" : $scope.user.roles,
            "boxes" : $scope.camera_box,
            "startdate_obj" : $scope.startdate_obj,
            "enddate_obj" : $scope.enddate_obj,
            "starttime_obj" : $scope.starttime_obj,
            "endtime_obj" : $scope.endtime_obj
        }
        
        $scope.progressbar.start();
        $scope.$root.input_profile = param
        //cfpLoadingBar.start();
        $http.post('http://localhost:8000/surveillance_interface/performquery/', param)
                .success(function(response_data, status) {
                
                $scope.$root.results = response_data
                //cfpLoadingBar.complete();
                $scope.progressbar.complete();
                
                $window.location.href = "http://localhost:8000/surveillance_interface/#results"
                
        
        });
    }
    
}]);


app.controller('ModalDateInstanceCtrl', function ($uibModalInstance, items, $scope, $filter) {
    
    
        
    $scope.today = function() {
        $scope.dt = new Date();
        $scope.dt2 = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.dt = null;
        $scope.dt2 = null;
    };

    
    
  $scope.options = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.options.minDate = $scope.options.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.setDate = function(year, month, day) {
      
    $scope.dt = new Date(year, month, day);
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date(tomorrow);
  afterTomorrow.setDate(tomorrow.getDate() + 1);
    
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
    
$scope.options2 = {
    customClass: getDayClass2,
    minDate: new Date(),
    showWeeks: true
  };
    
function getDayClass2(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
     


    
    $scope.time = null
    
    
/*
    Time picker code
*/    
$scope.mytime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 5;

  //$scope.options = {
    //hstep: [1, 2, 3],
    //mstep: [1, 5, 10, 15, 25, 30]
  //};

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };

  $scope.update = function() {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };

  $scope.changed = function () {
    console.log('Time changed to: ' + $scope.mytime);
  };

  $scope.clear = function() {
    $scope.mytime = null;
  };

    
    
$scope.mytime2 = new Date();

  
  $scope.update = function() {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime2 = d;
  };

  $scope.changed = function () {
    console.log('Time changed to: ' + $scope.mytime2);
  };

  $scope.clear = function() {
    $scope.mytime2 = null;
  };

    
   
                
  $scope.ok = function () {
        var oneDay = 24*60*60*1000;
        firstDate = new Date(1970, 01, 01)
        secondDate1 = $scope.dt.setMonth($scope.dt.getMonth() + 0);
        
        secondDate2 = $scope.dt2.setMonth($scope.dt2.getMonth() + 0);
        var diff_day1 = (secondDate1 - firstDate)
        var diff_day2 = (secondDate2 - firstDate)
        //var diff_day1 = Math.abs((firstDate.getTime() - $scope.dt.getTime()) / (oneDay)) * (24*60*60*1000);
        //var diff_day2 = Math.abs((firstDate.getTime() - $scope.dt2.getTime()) / (oneDay)) * (24*60*60*1000);
      
      
        return_param = {
            "startdate" : $filter('date')($scope.dt, "yyyyMMdd"),
            "startdate_obj" : $scope.dt,
            "enddate" : $filter('date')($scope.dt2, "yyyyMMdd"),
            "enddate_obj" : $scope.dt2,
            "starttime" : $filter('date')($scope.mytime, "hhmmssa"),
            "starttime_obj" : $scope.mytime,
            "endtime" : $filter('date')($scope.mytime2, "hhmmssa"),
            "endtime_obj" : $scope.mytime2,
        }
        
        
        $uibModalInstance.close(return_param);
  };
    
    
    

});


app.controller('ModalInstanceCtrl', function ($uibModalInstance, items, $scope) {
    var cam_num = items
    
    $scope.image_src = "no image"
     if (cam_num == 1) {
         $scope.image_src = "../static/images/cam1.png"
     } else if (cam_num == 2) {
         $scope.image_src = "../static/images/cam2.png"
     } else if (cam_num == 3) {
         $scope.image_src = "../static/images/cam3.png"
     } else {
         $scope.image_src = "../static/images/cam4.png"
     }
    
    
  $scope.aspectRatio = 0;

    $scope.Coords = {
        left: 0.29,
        top: 0.398,
        right: 0.867,
        bottom: 0.835,
        height: 0.437,
        width: 0.577
    };

    $scope.$watch(function () {
        return $scope.coords;
    }, function (newCoords) {
        console.log("newCoords: " + newCoords);
    });
    
  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

 
    
  $scope.ok = function () {
      var return_obj = {
          "coordinates" : $scope.coords,
          "cam_num" : $scope.selected.item
      }
      
    $uibModalInstance.close($scope.coords);
  };

});

