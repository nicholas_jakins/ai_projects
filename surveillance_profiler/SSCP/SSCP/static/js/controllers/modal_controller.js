app.controller('modalController', ['$scope', '$http', '$window', '$location', '$timeout', 'growl', '$modalInstance', function($scope, $http, $window, $location, $timeout, growl, $modalInstance) {
    
    console.log("modal controller");
    $scope.mytime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 5;

  //$scope.options = {
    //hstep: [1, 2, 3],
    //mstep: [1, 5, 10, 15, 25, 30]
  //};

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };

  $scope.update = function() {
    var d = new Date();
    d.setHours( 14 );
    d.setMinutes( 0 );
    $scope.mytime = d;
  };

  $scope.changed = function () {
    $log.log('Time changed to: ' + $scope.mytime);
  };

  $scope.clear = function() {
    $scope.mytime = null;
  };

    $scope.openModal = function() {
        console.log("")
    }
    
    
    $scope.close = function () {                    
             $modalInstance.close($scope.mytime);                       
        };
    
}]);