import psycopg2
import datetime
import cv2
import os
import shutil


def delete_all_frames(cur, conn):
	
	cur.execute("SELECT path_to_frame FROM image_frame")
	image_frame_paths = cur.fetchall()

	for i in image_frame_paths:
		print "path: ", i
		os.remove(i[0])

	cur.execute("DELETE FROM image_frame")
	conn.commit()


def create_query_date_procedure(cur, conn):

	cur.execute("CREATE OR REPLACE FUNCTION get_dates(start_date date, end_date date, start_time time, end_time time, camera_text text[]) RETURNS setof image_frame AS $$ \
				 DECLARE \
				 	cameras smallint[];\
				 	result image_frame%rowtype; \
				 BEGIN \
				 	cameras = string_to_array(camera_text,',');\
				  	RETURN QUERY \
				 	SELECT * FROM image_frame \
					WHERE view_date >= start_date \
						AND   view_date <= end_date \
						AND   view_time >= start_time \
						AND   view_time <= end_time \
						AND   camera_num = ANY(cameras);\
				    END; $$ \
				 LANGUAGE PLPGSQL;")
	conn.commit()

def q(cur, start_date, end_date, start_time, end_time, cameras):
	'''cur.execute("SELECT * FROM image_frame \
					WHERE view_date > %s \
					AND   view_date <= %s;", (start_date, end_date))'''

	cur.execute("SELECT * FROM get_dates(%s, %s, %s, %s, %s);", (start_date, end_date, start_time, end_time, cameras,))
	results = cur.fetchall()
	for r in results:
		print r
	

def get_all(cur):
	cur.execute("SELECT view_time FROM image_frame \
				 WHERE view_date = '20160714' \
				 AND view_time < '120951'")
	res = cur.fetchall()
	print res
	
def connect_to_database(name, username):
    return psycopg2.connect(database=name, user=username)



'''conn = connect_to_database("cslabdvr", "nicholasjakins")
cur = conn.cursor()

create_query_date_procedure(cur, conn)

#get_all(cur)

q(cur, '20160714', '20160714', '120938', '120951', '1, 2, 3')'''


