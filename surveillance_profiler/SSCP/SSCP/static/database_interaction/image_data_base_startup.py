import psycopg2

'''
Instructions for setting up the database
Make sure there is a server running, issue the command "postgres -D /usr/local/var/postgres"
Then issue the command "psql" in a seperate terminal
\q to quit
\l to list databases
\connect [database_name] to connect to a database
CREATE DATABASE [database_name]; to create a new database
'''

def create_dummy_table(cur):
	cur.execute("CREATE TYPE t AS (x integer, y integer)")
	cur.execute("CREATE TABLE dummy (id text primary key not null, counts integer[], coords t[])")



def create_table_profiler_info(cur):
	'''cur.execute("DROP TABLE IF EXISTS profiler_info")
	cur.execute("DROP TYPE IF EXISTS bounding_box")'''

	cur.execute('CREATE TYPE bounding_box AS (x integer, y integer, width integer, height integer)')
	cur.execute("CREATE TABLE profiler_info (path_to_frame text primary key not null, boxes bounding_box[])")

def create_table_frame(cur):
	cur.execute("CREATE TABLE image_frame (path_to_frame text primary key not null, view_date date, view_time time, camera_num smallint)")

def add_frame(path_to_image, todays_date, time_of_viewing, cam_num, cur):
	cur.execute("INSERT INTO image_frame(path_to_frame, view_date, view_time, camera_num) VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING",\
				 (path_to_image, todays_date, time_of_viewing, cam_num))

def connect_to_database(name, username):
    return psycopg2.connect(database=name, user=username)

def query_all(cur):
	cur.execute("SELECT * FROM image_frame")
	print cur.fetchall()


def insert_into_pi(cur):
	#self.cur.execute("INSERT INTO colorCategory(username, category, color) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING", \
	                 #(username, category, "("+str(HSV_values[0])+", "+str(HSV_values[1])+", "+str(HSV_values[2])+")"))

	#cur.execute("INSERT INTO profiler_info(path_to_frame, boxes) VALUES (%s, CAST(%(boxes)s AS bounding_box[]))", \
	#						 ("path", {'boxes': [(1, 2, 3, 4), (5, 6, 7, 8)]}))

	'''INSERT INTO namesages(namesandages) 
	VALUES(ARRAY[ROW('john', 24),ROW('david', 38)]::nameage[]);
	ARRAY[ROW('1', '2', '3', '4'), ROW('5', '6', '7', '8')::profiler_info]'''

	#cur.execute("INSERT INTO profiler_info VALUES (%s, %s) ON CONFLICT DO NOTHING", ("image_path", "ARRAY[ROW('1', '2', '3', '4'), ROW('5', '6', '7', '8')::profiler_info]"))
	cur.execute("INSERT INTO profiler_info (path_to_frame, boxes) VALUES (%s, %s::bounding_box[]) ON CONFLICT DO NOTHING", ("image_path", [('1', '2', '3', '4'), (('5', '6', '7', '8'))],))
	cur.execute("SELECT * FROM profiler_info")
	results = cur.fetchall()
	print results


'''ap = argparse.ArgumentParser()
ap.add_argument("-i", "--initialize_database", required=True, help="create the tables if not already present True for init, False for not")


args = vars(ap.parse_args())'''

conn = connect_to_database("cslabdvr", "nicholasjakins")
cur = conn.cursor()

#create_table_profiler_info(cur)


print "connection established..."
#create_table_frame(cur)

#add_frame("/Users/nicholasjakins/Desktop/test_shared_folder/temp", "20160713", "141830", "01", cur)
#query_all(cur)

#insert_into_pi(cur)

'''create_dummy_table(cur)
cur.execute("INSERT INTO dummy (id, counts, coords) VALUES (%s, %s, %s::t[])", ("test", "{1, 2, 3, 5}", [("1", "2"), ("3", "4")],))
cur.execute("SELECT * FROM dummy")
r = cur.fetchall()
print r'''

#conn.commit()

cur.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
print cur.fetchall()
print "successfully created database"


conn.close()
