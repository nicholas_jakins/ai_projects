from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^surveillance_interface/', include('surveillance_interface.urls'))
]
