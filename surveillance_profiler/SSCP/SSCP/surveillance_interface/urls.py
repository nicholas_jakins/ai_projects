from django.conf.urls import url
from surveillance_interface import views

urlpatterns = [
    #url(''),
    url(r'^$', views.idx, name='index'),
    url(r'^signin/$', views.register_signin, name='signin'),
    url(r'^getcameras/$', views.get_camera_number, name='getcameras'),
    url(r'^performquery/$', views.perform_query, name='performquery'),
    url(r'^getScore/$', views.get_score, name='getscore'),
    url(r'^input/$', views.get_input, name='get_input'),                       
    ]

