import imutils
import cv2

import psycopg2

#------------------------------------------------------------------------------------------------------------
import sys
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.template.loader import get_template
from django.shortcuts import render_to_response
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

import json
import random
from random import shuffle
from django.contrib.auth.models import User
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth import authenticate
import numpy as np

from shutil import copyfile
import matplotlib.pyplot as plt

import os.path
import time
from operator import itemgetter



#-----------------------------------------Import database support class------------------------------------------
sys.path.insert(0, '/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/database_interaction/')
sys.path.insert(0, '/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/profiler/')
sys.path.insert(0, '/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/profiler/scoring/discodb/')

import perform_queries
import boundingBoxes as bb

import inference.Infer as I
import Color_model.paramConfig as cm
import scoring.score_frame as sf
from discodb import DiscoDB, Q

import ROIGeneration as rg
#----------------------------------------------------------------------------------------------------------------


#-----------------------------------------Profiler support-------------------------------------------------------
dict_colors = {0:"Gray", 1:"Black", 2:"Yellow", 3:"Orange", 4:"Pink", 5:"Red", 6:"Green", 7:"Blue", 8:"Purple", 9:"Brown", 10:"Beige"}
dict_colors_num = {"Gray":0, "Black":1, "Yellow":2, "Orange":3, "Pink":4, "Red":5, "Green":6, "Blue":7, "Purple":8, "Brown":9, "Beige":10}

path_to_profiler = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/profiler/"
base_footage_path = "/Users/nicholasjakins/Desktop/test_shared_folder/Footage"
path_to_image_query = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images_from_query"

def fact_sum(x):
    result = 0
    while x != 1:
        result = result + np.log(x)
        x = x - 1
    return result

def get_log_cache():
    log_cache = np.zeros((101,))
    temp = [fact_sum(i) for i in xrange(1, 101)]
    log_cache[1:101] = temp
    return log_cache
    
def connect_to_database(name, username):
    return psycopg2.connect(database=name, user=username)

def draw_bounding_box(frame_path, box):
    # Use true paths not relative
    name = frame_path.split("/", 12)[-1]
    frame_path = path_to_image_query + "/" + name
    #print "frame_path for drawing the box is: ", frame_path
    
    img_to_draw = cv2.imread(frame_path)
    cv2.imwrite(frame_path, img_to_draw)


def read_partition_config():
	f = open(path_to_profiler + "Partitioning/gaussian_config.py", "r")
	data = f.read()
	str_to_execute = "D = " + data
	str_to_execute = str.replace(str_to_execute, "array", "")
	exec str_to_execute
	f.close()

	return (D[0], D[1])

def init_scoring():
    color_model = cm.HSVModel()
    M, C = read_partition_config()
    return (color_model, M, C)

def get_true_path(image_name):
    name = image_name.split("/", 8)[-1]
    camera_number = name.split("_", 5)[1][1]
    true_path = base_footage_path + "/C" + camera_number + "/" + name
    return true_path

def get_bounding_boxes(frame_path, cur):
    
    true_path = get_true_path(frame_path)
    #print "true_path is: ", true_path
    
    cur.execute("SELECT boxes FROM profiler_info WHERE path_to_frame = %s", (true_path,))
    boxes = cur.fetchall()
    
    dict_boxes = {"Boxes" : [], "Image" : true_path}
    
    for box in boxes:
        #print "box: ", box
        
        #{'Frame': 0, 'Boxes': [array([196, 125, 291, 314])]}, {'Frame': 1, 'Boxes': [array([196,  82, 273, 236])]}
        
        idx1 = box[0].find("(")
        idx2 = box[0].find(")")
        box_tuple_str = box[0][idx1:idx2 + 1]
        str_to_exec = "b = " + "np.array(" + box_tuple_str + ")"
        exec str_to_exec
        #print "b is: ", b
        
        # Draw the boxes on the images
        #draw_bounding_box(frame_path, b)
        dict_boxes["Boxes"].append(b)
        
    return dict_boxes

#----------------------------------------------------------------------------------------------------------------

def change_path(image_path, target_dir):
    #print "image_path: ", image_path
    path = image_path["path"]
    return target_dir + "/" + path.split("/", 4)[-1]

'''
    superuser login: nicholasjakins
    superuser password: M0sa!c$$
'''

def store_bounding_box_info(frames, cur):
    #print "info for frames: ", frames
    for f in frames:
        
        image_frame = cv2.imread(f)
        list_boxes = frames[f]
        for box in list_boxes:
            cv2.rectangle(image_frame, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (255, 255, 255), 1)
            
            
        # form tuples of strings
        bbs = [(str(i[0]), str(i[1]), str(i[2]), str(i[3])) for i in list_boxes]
        #print "BBS: ", bbs
        cur.execute("INSERT INTO profiler_info (path_to_frame, boxes) VALUES (%s, %s::bounding_box[])\
                     ON CONFLICT DO NOTHING", (f, bbs,))
        cv2.imwrite(f, image_frame)
    
    # draw the bounding boxes on the frames for display
    
    
def find(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1


def get_region(camera_rois, cam_num, img_width, img_height):
    str_to_exec = "camera_rois = " + camera_rois
    exec str_to_exec
    try:
        if camera_rois[cam_num]['top'] == None:
            return None
    except KeyError:
        return None
    
    region_per = (camera_rois[cam_num]['top'], camera_rois[cam_num]['left'], camera_rois[cam_num]['bottom'], camera_rois[cam_num]['right'])
        
    x0 = region_per[1] * img_width
    y0 = region_per[0] * img_height
    x1 = region_per[3] * img_width
    y1 = region_per[2] * img_height
    
    rec = rg.rect(int(x0), int(y0), int(x1), int(y1))
    
    return rec
    
def check_region(rois, image_path, cam_num):
    print "rois are!!!!!: ", rois
    
    debug_dir = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/debugging/"
    img_height = 245
    img_width = 300
    
    conn = perform_queries.connect_to_database("cslabdvr", "nicholasjakins")
    cur = conn.cursor()
    
    boxes = get_bounding_boxes(image_path, cur)
    conn.close()
    
    for box in boxes["Boxes"]:
        rec1 = get_region(rois, cam_num, img_width, img_height)
        if rec1 == None:
            return True
        #img = cv2.imread(image_path)
        #cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), (0, 0, 255), 1)
        
        #cv2.rectangle(img, (rec1.x0, rec1.y0), (rec1.x1, rec1.y1), (0, 255, 0), 1)
        #cv2.imwrite(debug_dir+"wtf.png", img)
        
        
        rec2 = rg.rect(box[0], box[1], box[2], box[3])
        intersects = rg.intersects(rec1, rec2)
        if intersects[0] == True:
            #print "intersects"
            return True
    
    #print "does not intersect"        
    return False
    
    

def get_frames_datetime(startdate, enddate, starttime, endtime, cams_str, cams, camera_rois):
        
    conn = perform_queries.connect_to_database("cslabdvr", "nicholasjakins")
    cur = conn.cursor()
    cams_str = str.replace(cams_str, "[", "")
    cams_str = str.replace(cams_str, "]", "")
    #print "----------------------------------------------------------------------------------------------------------------"
    #print cams_str
    #print starttime
    #print endtime
    cur.execute("SELECT path_to_frame, camera_num FROM get_dates(%s, %s, %s, %s, %s);", (startdate, enddate, starttime, endtime, cams_str,))
    res = cur.fetchall()
    
    #dict_camera = {"C" + str(c) : [] for c in cams}
    
    #{"day" : None, "images" : [], "slider" : None}
    dict_camera = {"C" + str(c) : [] for c in cams}
    data = {}
    
    days = {}
    
    for r in res:
        #print r
        src = r[0]
        #print src
        
        cam_num = str(r[1])
        
        if check_region(camera_rois, src, cam_num) == False:
            continue
        
        
        image_name = r[0].split("/", 7)[-1]
        #print "image_name: ", image_name
        des = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images_from_query" + "/" + image_name
        alt_dir = "../static/images_from_query" + "/" + image_name
        
        #print "copying file to destination: ", src, ", ", des
                
        copyfile(src, des)
        
        # also want to copy the masked images
        src_mask = src[0:src.find(".")] + "_mask.png"
        dst_mask = des[0:des.find(".")] + "_mask.png"
        copyfile(src_mask, dst_mask)
        
        date = alt_dir.split("/", 10)[-1]
        #print "date is: ", date
        day = date[0:8]
        
        cam_num = "C" + str(r[1])
        #print "dict_camera: ", dict_camera
        
        # nothing has been added
        if len(dict_camera[cam_num]) == 0:
            actual_images = []
            images = []
            
            images.append({"active" : True, "path" : alt_dir, "hour" : image_name[11:13], "view_flag" : True})
            actual_images.append({"path" : alt_dir, "active" : True})
            
            dict_camera[cam_num].append({"day" : day, "slider" : None, "images" : images, "actual_list" : actual_images})
        else:
            #print "dict_camera after first image has been added: ", dict_camera
            # check if day is in
            if any(d["day"] == day for d in dict_camera[cam_num]):
                idx_of_day = find(dict_camera[cam_num], "day", day)
                dict_camera[cam_num][idx_of_day]["images"].append({"active" : False, "path" : alt_dir, "hour" : image_name[11:13], "view_flag" : True})
                dict_camera[cam_num][idx_of_day]["actual_list"].append({"active" : False, "path" : alt_dir})
            else:
                # new day
                actual_images = []
                images = []
                
                images.append({"active" : True, "path" : alt_dir, "hour" : image_name[11:13], "view_flag" : True})
                actual_images.append({"active" : True, "path" : alt_dir})
                
                dict_camera[cam_num].append({"day" : day, "slider" : None, "images" : images, "actual_list" : actual_images})
            
    
    res_dict = {"data" : dict_camera, "cams" : [c for c in dict_camera.keys()]}
    
    json_days = json.dumps(res_dict, sort_keys=True, indent=4)
    #print json_days
        
    #dict_camera["C" + str(r[1])].append(alt_dir)  
    
    conn.commit()
    conn.close()
    
    
    # Store the results in days
    
    return (json_days, res_dict)
    
# Create your views here.
@csrf_exempt
def idx(request):
    if request.method != "POST":
        context = RequestContext(request)
        context_dict = {}
        return render_to_response('signin.html', context_dict, context)
    else:
        return HttpResponse("")
    
@csrf_exempt
def register_signin(request):
    print "registered signin!!!!"
    print "request: ", request
    data = json.loads(request.body)
    username = smart_str(xstr(data['user_name']))
    password = smart_str(xstr(data['password']))
    
    print "username: ", username
    print "password: ", password
    
    user = authenticate(username=username, password=password)
    resp = {}
    
    if user is not None:
        if user.is_active:
            print("User is valid, active and authenticated")
            resp['status'] = 0
        else:
            print("The password is valid, but the account has been disabled!")
            resp['status'] = 1
    else:
        # the authentication system was unable to verify the username and password
        print("The username and password were incorrect.")
        resp['status'] = 2
    
    
    json_str = json.dumps(resp, sort_keys=True, indent=4)
    print "json_str: ", json_str
        
    return HttpResponse(json_str)


@csrf_exempt
def get_camera_number(request):
    #print "in get cameras"
    path_to_config_file = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/Setup/setup_con.py"
    f = open(path_to_config_file, "r")
    str_to_execute = "res = " + f.read()
    exec str_to_execute
    #print "res: ", np.array([1, 2, 3, 4])
    
    json_str = json.dumps(res, sort_keys=True, indent=4)
    #print json_str
    return HttpResponse(json_str)


def write_region(camera_rois):
    img3 = cv2.imread("/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images/cam3.png")
    debug_dir = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/debugging/"
    
    print "type(camera_rois): ", type(camera_rois)
    str_to_exec = "camera_rois = " + camera_rois
    exec str_to_exec
    
    img_width = img3.shape[1]
    img_height = img3.shape[0]
    
    for cam_num in camera_rois:
        if camera_rois[cam_num] == None:
            continue
        
        region_per = (camera_rois[cam_num]['top'], camera_rois[cam_num]['left'], camera_rois[cam_num]['bottom'], camera_rois[cam_num]['right'])
        
        x0 = region_per[1] * img_width
        y0 = region_per[0] * img_height
        x1 = region_per[3] * img_width
        y1 = region_per[2] * img_height
        
        cv2.rectangle(img3, (int(x0), int(y0)), (int(x1), int(y1)), (0, 0, 255), 1)
        cv2.imwrite(debug_dir + "example.png", img3)
        
        
def normalize_and_sort_scores(results, max_score, min_score):
    print "max: ", max_score
    print "min: ", min_score
    
    diff = max_score - min_score
    
    results_json = json.dumps(results, sort_keys=True, indent=4)
    print results_json
    
    for camera_num in results["data"]:
        
        day_cnt = 0
        for day_info in results["data"][camera_num]:
            image_cnt = 0
            for images in day_info["images"]:
                current_score = max_score - results["data"][camera_num][day_cnt]['actual_list'][image_cnt]["score"]
                results["data"][camera_num][day_cnt]['actual_list'][image_cnt]["score"] = 1.0 - (current_score / float(diff))
                results["data"][camera_num][day_cnt]["images"][image_cnt]["score"] = 1.0 - (current_score / float(diff))
                image_cnt = image_cnt + 1
            day_cnt = day_cnt + 1
    
    
    for camera_num in results["data"]:
        day_cnt = 0
        for day_info in results["data"][camera_num]:
            image_cnt = 0
            for images in day_info["images"]:
                new_list = sorted(results["data"][camera_num][day_cnt]['actual_list'], key=itemgetter('score'), reverse=True)
                results["data"][camera_num][day_cnt]['actual_list'] = new_list
                
                new_list2 = sorted(results["data"][camera_num][day_cnt]["images"], key=itemgetter('score'), reverse=True)
                results["data"][camera_num][day_cnt]["images"] = new_list2
                
                image_cnt = image_cnt + 1
        day_cnt = day_cnt + 1
        
    
    return results
    
@csrf_exempt
def perform_query(request):
    #print "in perform query!"
    
    data = json.loads(request.body)
    
    hair_color = smart_str(xstr(data['haircolor']))
    torso_color = smart_str(xstr(data['torsocolor']))
    legs_color = smart_str(xstr(data['legscolor']))
    start_date = smart_str(xstr(data['startdate']))
    end_date = smart_str(xstr(data['enddate']))
    start_time = smart_str(xstr(data['starttime']))
    end_time = smart_str(xstr(data['endtime']))
    camera_subset = smart_str(xstr(data['camerasubset']))
    camera_rois = smart_str(xstr(data['boxes']))
    
    
    #for t in hair_color:
        #print t
    print hair_color, ", ", torso_color, ", ", legs_color, ", ", start_date, ", ", end_date, ", ", start_time, ", ", end_time, ", ", camera_subset, ", ", camera_rois
    
    # debug the camera region selector
    
    write_region(camera_rois)
    
    
    
    str_to_exec = "cams = " + camera_subset
    exec str_to_exec
    
    # get image frames from the database
    results_json, results = get_frames_datetime(start_date, end_date, start_time, end_time, camera_subset, cams, camera_rois)
    
    # Score the results for each image
    color_model, mean_partition, cov_partition = init_scoring()
    
    conn = perform_queries.connect_to_database("cslabdvr", "nicholasjakins")
    cur = conn.cursor()    
    
    # color specification
    
    color_spec = {0 : dict_colors_num[hair_color], 1 : dict_colors_num[torso_color], 2 : dict_colors_num[legs_color]}
    
    time_for_updating_att = 0
    time_for_updating_neighbours = 0
    score_db_time = 0
    time_for_images = 0
    time_for_calc = 0
    time_for_init = 0
    time_for_overall_scoring = 0
    time_caching_logs = 0
    start_time = time.time()
    
    db = DiscoDB.load(file("/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/profiler/scoring/hsvobs_raw.db", "r"))
    
    # generate connections
    conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
    conn_cslabdvr = psycopg2.connect(database="cslabdvr", user="nicholasjakins")
	
    log_cache = get_log_cache()
    
    max_score = 0.0
    min_score = 10000
    for camera_num in results["data"]:
        #print "camera_num: ", camera_num
        day_cnt = 0
        for day_info in results["data"][camera_num]:
            image_cnt = 0
            for images in day_info["images"]:
                s = time.time()
                #print 'images["path"]: ', images["path"]
                
                # get bounding_box information
                boxes = get_bounding_boxes(images["path"], cur)
                             
                name = images["path"].split("/", 8)[-1]
                
                path = path_to_image_query + "/" + name
                #print "path to read: ", path
                
                # need to read in the mask too
                img_mask_path = path[0:path.find(".", 4)] + "_mask.png"
                
                img = cv2.imread(path)
                img_mask = cv2.imread(img_mask_path)
                
                #print "mask is: ", img_mask
                
                #print "image to read is: ", img
                e = time.time()
                time_for_images = time_for_images + (e - s)
                
                boxes["Images"] = path
                
                #print "THE PATH TO THE IMAGE THAT THE SCORING ALGORITHM WILL USE IS: ", boxes["Images"]
                # here we score the frame
                #print boxes
                print "score: ", image_cnt, "/", len(day_info["images"])
                #star = time.time()
                score_obj = sf.score(boxes, img, img_mask, mean_partition, cov_partition, color_spec, color_model, path, db, conn, conn_cslabdvr, log_cache)
                    
                s = score_obj.score 
                
                s = s * -1
                if s > max_score:
                    max_score = s
                
                if s < min_score:
                    min_score = s
                    
                    
                #en = time.time()
                time_for_updating_att = time_for_updating_att + score_obj.time_to_update_att
                time_for_updating_neighbours = time_for_updating_neighbours + score_obj.time_for_updating_neighbours
                score_db_time = score_db_time + score_obj.database_time
                time_for_calc = time_for_calc + score_obj.calculation_time
                time_for_init = time_for_init + score_obj.initial_time
                time_for_overall_scoring = time_for_overall_scoring + score_obj.whole_score
                time_caching_logs = time_caching_logs + score_obj.log_caching
                print "after score"
                
                results["data"][camera_num][day_cnt]['actual_list'][image_cnt]["score"] = s
                
                
                #results["data"][camera_num][day_cnt]["images"][image_cnt]["score"] = s
                
                image_cnt = image_cnt + 1
                
            day_cnt = day_cnt + 1
    end_time = time.time()
    print "total time: ", (end_time - start_time)
    print "time for images without scoring: ", time_for_images
    print "time_for_overall_scoring: ", time_for_overall_scoring
    print "     time for updating attributes: ", time_for_updating_att
    print "     time_for_updating_neighbours: ", time_for_updating_neighbours
    print "     time spent with the database: ", score_db_time
    print "     calculations: ", time_for_calc
    print "     time for preamble: ", time_for_init
    print "     log caching time: ", time_caching_logs
    
    print "max_score: ", max_score
    results = normalize_and_sort_scores(results, max_score, min_score)
    
    results_json = json.dumps(results, sort_keys=True, indent=4)
    print results_json
    return HttpResponse(results_json)

def generate_meta_info(img, bounding_box, head_histo, torso_histo, legs_histo, score, path):
    root_dir = "/Users/nicholasjakins/Documents/RW771_SOURCE/rw771-source/Demo/SSCP/SSCP/static/images_from_query/"
    modified_path = path[0:path.find(".")] + "_meta.png"
    name = modified_path.split("/", 10)[-1]
    modified_path = root_dir + name
    print "modified_path: ", modified_path
    str_to_exec = "b = " + bounding_box
    exec str_to_exec
    
    # draw bounding_box
    cv2.rectangle(img, (int(b[0]), int(b[1])), (int(b[2]), int(b[3])), (0, 0, 255), 1)
    #cv2.imwrite(modified_path, img)
    
    img_RGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    a1 = plt.subplot2grid((3,2), (0,0), rowspan=3)
    plt.imshow(img_RGB)
    plt.title("Image frame")
    
    colours_to_train = sf.colors_to_train
    
    observations = (head_histo, torso_histo, legs_histo)
    
    for idx in colours_to_train:
        
        a2 = plt.subplot2grid((3,2), (idx,1))
        t = plt.gca()
        t.axes.get_xaxis().set_visible(False)
        t.axes.get_yaxis().set_visible(False)

        enumerated_list = [i for i in xrange(1, len(colours_to_train[idx]) + 1, 1)]

        bar_list = plt.bar(enumerated_list, observations[idx])

        for i in range(len(colours_to_train[idx])):
            bar_list[i].set_color(sf.dict_colors[colours_to_train[idx][i]])

        plt.title(sf.body_part_dict[idx] + " histogram")
    
    plt.savefig(modified_path)
    
    while not os.path.exists(modified_path):
        time.sleep(1)
    print "finished saving figure!!!!"
    
    
@csrf_exempt
def get_score(request):
    conn = connect_to_database("cslabdvr", "nicholasjakins")
    cur = conn.cursor()
    
    data = json.loads(request.body)
    img_path = smart_str(xstr(data['image_name']))
    img_path = get_true_path(img_path)
    
    cur.execute("SELECT * FROM scores WHERE scores.image_path = (%s)", (img_path,))
    results = cur.fetchall()
    print "results: ", results
    
    img = cv2.imread(results[0][0])
    bounding_box = results[0][1]
    head_histo = results[0][2]
    torso_histo = results[0][3]
    legs_histo = results[0][4]
    score = results[0][5]
    
    generate_meta_info(img, bounding_box, head_histo, torso_histo, legs_histo, score, img_path)
    
    print "in get score: ", img_path
    return HttpResponse("")

@csrf_exempt
def get_input(request):
    print "in register input"
    return HttpResponse("")




def xstr(s):
    return None if s == "" else str(s)



