import numpy as np
import random

class random_scheme:

	def __init__(self):
		pass

	def __str__(self):
		return 'random scheme'

	def get_color(self):
		return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

class darker_scheme:

	def __init__(self):
		pass

	def __str__(self):
		return 'darker scheme'

	def get_color(self):
		return (random.randint(0, 50), random.randint(0, 50), random.randint(0, 50))

class lighter_scheme:

	def __init__(self):
		pass

	def __str__(self):
		return 'lighter scheme'

	def get_color(self):
		return (random.randint(200, 255), random.randint(200, 255), random.randint(200, 255))

		