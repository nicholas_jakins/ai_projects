from generator_factory import random_scheme
from generator_factory import darker_scheme
from generator_factory import lighter_scheme

global RANDOM_SCHEME
global DARKER_SCHEME
global LIGHTER_SCHEME

RANDOM_SCHEME = 0
DARKER_SCHEME = 1
LIGHTER_SCHEME = 2


class color_generator(random_scheme, darker_scheme, lighter_scheme):
    
    def __init__(self, scheme=0):

        schemes = {RANDOM_SCHEME: random_scheme(), \
                   DARKER_SCHEME: darker_scheme(), \
                   LIGHTER_SCHEME: lighter_scheme()}
        
        self.color_scheme = schemes[scheme]

    def get_generated_colors(self):
    	if self.colors is None:
    		self.generate_colors(0)

    	return self.colors

    def generate_colors(self, num_of_colors):
    	self.colors = [self.generate_color() for i in xrange(0, num_of_colors, 1)]

    def generate_color(self):
        return self.color_scheme.get_color()

    def serialize_for_sql_insert(self):
    	refined_color_input = str(self.colors).replace("[", "{")
        refined_color_input = str(refined_color_input).replace("]", "}")
        refined_color_input = str(refined_color_input).replace("(", "{")
        refined_color_input = str(refined_color_input).replace(")", "}")

        return refined_color_input