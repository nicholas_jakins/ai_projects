from __future__ import unicode_literals

from django.db import models
import os

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

# Create your models here.



