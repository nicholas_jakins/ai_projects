import imutils
import cv2
from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
import argparse
import numpy as np
from imutils.object_detection import non_max_suppression
from imutils import paths
from imutils.object_detection import non_max_suppression

import psycopg2
import sys

class database_connection(object):

	def __init__(self, name, user):
		self.open_connection(name, user)
		self.cur = self.conn.cursor()
        print 'successful connection:'

	def open_connection(self, NAME, USER):
		try:
			self.conn = psycopg2.connect(database=NAME, user=USER)
		except:
			print 'failed to connect'


	def close_connection(self, conn):
		try:
			conn.close()
		except:
			'failed to close connection'

	def create_files_table(self):
		self.cur.execute('CREATE TABLE files (id serial primary key, orig_filename text not null, file_data bytea not null)')


	def insert_image(self, img_name):
		
		f = open(img_name,'rb')
		print "opened file: ", img_name
		filedata = psycopg2.Binary(f.read())
		self.cur.execute("INSERT INTO files(id, orig_filename, file_data) VALUES (DEFAULT,%s,%s) RETURNING id", (img_name, filedata))
		returned_id = self.cur.fetchone()[0]
		f.close()
		print("Stored {0} into DB record {1}".format(img_name, returned_id))


	def fetch_image(self, ID, destination):
		f = open(destination,'wb')
		self.cur.execute("SELECT file_data, orig_filename FROM files WHERE id = %s", ("1"))
		(file_data, orig_filename) = self.cur.fetchone()
		
		#self.display_image(file_data)
		f.write(file_data)
		f.close()


		#print("Fetched {0} into file {1}; original filename was {2}".format(args.fetch, args.filename, orig_filename))

	def display_image(self, data):
		#image = color.rgb2gray(data.imread(image_name))
		image = color.rgb2gray(data)
		fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
                    cells_per_block=(1, 1), visualise=True)

		fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

		ax1.axis('off')
		ax1.imshow(image, cmap=plt.cm.gray)
		ax1.set_title('Input image')
		ax1.set_adjustable('box-forced')

		# Rescale histogram for better display
		hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))

		ax2.axis('off')
		ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
		ax2.set_title('Histogram of Oriented Gradients')
		ax1.set_adjustable('box-forced')
		plt.show()


	def list_all_tables(self):
		self.cur.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
		print self.cur.fetchall()

print "hello"