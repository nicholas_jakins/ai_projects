import __init__ as config
import imutils
import cv2
from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
import argparse
import numpy as np
from imutils.object_detection import non_max_suppression
from imutils import paths
from imutils.object_detection import non_max_suppression

import psycopg2

#------------------------------------------------------------------------------------------------------------
import sys
from django.shortcuts import render

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.template.loader import get_template
from django.shortcuts import render_to_response
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

import json
import random
from random import shuffle

global num_questions
global color_scheme

from color_generator import color_generator as col_gen

class database_connection(object):
    
    def __init__(self, conn):
        self.con = conn
        self.cur = conn.cursor()
        self.list_all_tables()
        
    def gen_image(self, HSV, idx):
            
        height = config.IMG_HEIGHT
        width = config.IMG_WIDTH
        image = np.zeros((height,width,3), np.uint8)
            
        image[:, :] = HSV
        image = cv2.imwrite(config.IMAGES_DIR_ABS_PATH + str(idx) + ".png", image)
    
    def insertResult(self, username, category, idx):
        # get hsv value from image path
        pathToImage = config.IMAGES_DIR_ABS_PATH + str(idx) + ".png"
        
        IMG_BGR = cv2.imread(pathToImage)
        IMG_HSV = cv2.cvtColor(IMG_BGR, cv2.COLOR_BGR2HSV)
        HSV_values = IMG_HSV[0][0]
        
        self.cur.execute("INSERT INTO colorCategory(username, category, color) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING", \
                         (username, category, "("+str(HSV_values[0])+", "+str(HSV_values[1])+", "+str(HSV_values[2])+")"))
        
        self.con.commit()
        
    def getUserInfo(self, username):
        self.cur.execute('SELECT colors FROM modified_users WHERE modified_users.username = %s', (username,))
        results = self.cur.fetchall()
        
        
        res = {}
        res['colors'] = results[0][0]
        return res
    
    def list_all_tables(self):
		self.cur.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
		print self.cur.fetchall()
    
    # returns 0 if there is no user and inserts the user into the database
    # returns 1 if there is an existing username, no insertion
    def is_user(self, username, num_questions, scheme):
        
        self.cur.execute('SELECT username FROM modified_users WHERE modified_users.username = %s', (username,))
        results = self.cur.fetchall()
        
        if len(results) == 0:
            
            generator = col_gen.color_generator(scheme)
            generator.generate_colors(num_questions)
            refined_color_input = generator.serialize_for_sql_insert()
            
            self.cur.execute('INSERT INTO modified_users(username, colors) VALUES (%s, %s)', (username, refined_color_input))
            self.con.commit()
            
            count = 1
            for hsv in generator.get_generated_colors():
                
                height = config.IMG_HEIGHT
                width = config.IMG_WIDTH
                image = np.zeros((height,width,3), np.uint8)
                image[:, :] = hsv
                image = cv2.imwrite(config.IMAGES_DIR_ABS_PATH+str(count)+".png", image)
                count = count + 1
            
            return 0
        else:
            return 1

#------------------------------------------------------------------------------------------------------------

@csrf_exempt
def idx(request):
    if request.method != "POST":
        context = RequestContext(request)
        context_dict = {}
        return render_to_response('index.html', context_dict, context)
    else:
        return HttpResponse("")
    

@csrf_exempt
def start_survey(request):
    data = json.loads(request.body)
    username = xstr(data['user_name'])
    num_questions = int(data['num_questions'])
    color_scheme = int(data['scheme'])
    
    conn = connect_to_database("colorsurvey", config.DATABASE_USRNAME)
    db = database_connection(conn)
    db.list_all_tables()
    isUser = db.is_user(username, num_questions, color_scheme)
    
    conn.close()
    
    response = {}
    response['status'] = isUser
    
    json_str = json.dumps(response, sort_keys=True, indent=4)
    return HttpResponse(json_str)
    
@csrf_exempt
def begin(request):
    
    data = json.loads(request.body)
    username = xstr(data['user'])
    conn = connect_to_database("colorsurvey", config.DATABASE_USRNAME)
    db = database_connection(conn)
    res = db.getUserInfo(username)
    
    resp = {}
    resp['color_order'] = res['colors']
    resp['size'] = len(res['colors'])
    
    json_str = json.dumps(resp, sort_keys=True, indent=4)    
    return HttpResponse(json_str)

@csrf_exempt
def store_results(request):
    
    data = json.loads(request.body)  
    user = xstr(data['user'])
    category = xstr(data['category'])
    idx = xstr(data['index'])
        
    conn = connect_to_database("colorsurvey", config.DATABASE_USRNAME)
    db = database_connection(conn)
    db.insertResult(user, category, idx)
    
    return HttpResponse("")

@csrf_exempt
def next_image(request):
    
    data = json.loads(request.body) 
    
    H = xstr(data['h'])
    S = xstr(data['s'])
    V = xstr(data['v'])
    idx = xstr(data['index'])
    
    conn = connect_to_database("colorsurvey", config.DATABASE_USRNAME)
    db = database_connection(conn)
    db.gen_image((H, S, V), idx)
    
    return HttpResponse("")

@csrf_exempt
def get_colors_location(request):
    
    resp = {'colors_location' : config.IMAGES_DIR}
    
    json_str = json.dumps(resp, sort_keys=True, indent=4)
    return HttpResponse(json_str)
    
def connect_to_database(name, username):
    return psycopg2.connect(database=name, user=username)

def xstr(s):
    return None if s == "" else str(s)