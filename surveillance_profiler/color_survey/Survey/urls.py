from django.conf.urls import url
from Survey import views

urlpatterns = [
    url(r'^$', views.idx, name='index'),
    url(r'^get_color_dir/$', views.get_colors_location, name='get_colors_location'),
    url(r'^get_name/$', views.start_survey, name='get_name'),
    url(r'^begin_survey/$', views.begin, name='begin_survey'),
    url(r'^store_results/$', views.store_results, name='store_results'),
    url(r'^generate_next_image/$', views.next_image, name='next_image'),
                       
]