import yaml
import os

global IMAGES_DIR
global DATABASE_USRNAME
global DATABASE

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APP_NAME = 'Survey'
FWD_SLASH = '/'
YML_FILENAME = 'survey_config.yaml'

PATH_TO_YAML = BASE_DIR + FWD_SLASH + APP_NAME + FWD_SLASH + YML_FILENAME

with open(PATH_TO_YAML, 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


IMG_WIDTH = cfg['image']['width']
IMG_HEIGHT = cfg['image']['height']
    
DATABASE_USRNAME = cfg['postgres']['username']
DATABASE = cfg['postgres']['database']

IMAGES_DIR = '../static/colors/'
IMAGES_DIR_ABS_PATH = BASE_DIR + IMAGES_DIR[2:]