'''
To create a new database make sure that the psql server is running "postgres -D /usr/local/var/postgres".
Then execute "> psql"
Followed by  "> CREATE DATABASE colorSurvey"
'''
from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2

from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
import cv2.bgsegm as contrib
import itertools

import psycopg2

def create_table_modified_users(cur):
    cur.execute('CREATE TABLE modified_users (username text primary key not null, colors integer[][])')
    
    
def create_table_users(cur):
    cur.execute('CREATE TABLE users (username text primary key not null, batchNumber int not null, colors integer[])')

def create_table_colorCategory(cur):   
    cur.execute('CREATE TYPE hsv AS (hue real, saturation real, value real)')
    
    cur.execute('CREATE TABLE colorCategory \
                 (username text, \
                  category text, \
                  color hsv, \
                  PRIMARY KEY(username, color))')

def myArrayConverter(arr):
	#print "input:", arr
	#print arr.shape[0]
	exec_str = "t = " + arr
	exec exec_str
	print "t: ", type(t[0])
	HSV = np.zeros((3,), np.uint8)

	HSV[0] = t[0]
	HSV[1] = t[1]
	HSV[2] = t[2]
	print HSV
	return HSV

#establish a connection
conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")

#create cursor
cur = conn.cursor()
res = cur.execute("SELECT username FROM modified_users")
print cur.fetchall()

res = cur.execute("SELECT * FROM colorCategory")
print cur.fetchall()


'''cur.execute("SELECT color, category FROM colorCategory WHERE colorCategory.username = %s", ("Nicole",))
results = cur.fetchall()
print results

for hsv in results:

	print hsv
	print "hsv: ", hsv[0]
	HSV = myArrayConverter(hsv[0])
	

	height = 500
	width = 500
	image = np.zeros((height,width,3), np.uint8)

	image[:, :] = HSV
	image_BGR = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

	cv2.imshow("image in BGR but with HSV: "+str(HSV), image_BGR)
	cv2.waitKey(0)'''



#cur.execute("DELETE FROM modified_users")
#cur.execute("DELETE FROM colorCategory")
#conn.commit()


#create_table_colorCategory(cur)
#conn.commit()

#cur.execute('INSERT INTO modified_users(username, colors) VALUES (%s, %s)', ("test_username", "{{1,2,3}, {4,5,6}}"))
#conn.commit()

#cur.execute('DELETE from modified_users')
#cur.execute('SELECT * FROM modified_users')

#cur.execute('SELECT * FROM modified_users')
#results = cur.fetchall()
#print results

#create_table_modified_users(cur)
#conn.commit()

#Drop tables
#cur.execute("DROP TYPE hsv")
#cur.execute("DROP TABLE users")
#cur.execute("DROP TABLE colorCategory CASCADE")
#conn.commit()

#create tables

#create_table_users(cur)
#create_table_colorCategory(cur)

#cur.execute("DELETE FROM users")
#conn.commit()

#cur.execute("DELETE FROM colorCategory")
#conn.commit()

#cur.execute("SELECT * FROM colorCategory")
#print cur.fetchall()

#cur.execute("SELECT * FROM users")
#print cur.fetchall()

#conn.commit()

#list all tables
cur.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
print cur.fetchall()

conn.close()