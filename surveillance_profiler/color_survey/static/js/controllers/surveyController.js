app.controller('surveyController', ['$scope', '$http', '$window', '$location', '$timeout', 'growl', function($scope, $http, $window, $location, $timeout, growl) {
    
    $scope.images = []
    $scope.index = 1
    $scope.totalImages = 0
    $scope.imageSource = $scope.$root.IMAGES_LOC+String($scope.index)+".png"
    
    $scope.relocateHome = function() 
    {
        $window.location.href = "http://localhost:8000/Survey/"   
    }
    
    param = 
    {
        'user': $scope.$root.USERNAME
    }
    
    $http.post('http://localhost:8000/Survey/begin_survey/', param).success(function(response_data, status) 
    {
        var hsv_values = response_data.color_order

        $scope.images = hsv_values
        $scope.totalImages = response_data.size                
    })
    
    $scope.nextColor = function(color_category) 
    {
        if (color_category == "Skip") 
        {
            $scope.index = $scope.index + 1
            $scope.imageSource = $scope.$root.IMAGES_LOC+$scope.index+".png"
            
            if ($scope.index == ($scope.totalImages - 1)) 
            {
                $('#dummyModal').modal('show')            
            }
            
            return;
        }
        
        $scope.showSuccess = function(msg, title) 
        {
            growl.success(msg,{title: title});
        }
        
        if ($scope.index == ($scope.totalImages - 1)) 
        {
            $('#dummyModal').modal('show')            
        }
                
        param = 
        {
            "user" : $scope.$root.USERNAME,
            "category" : color_category,
            "index": $scope.index
        }
        
        $http.post('http://localhost:8000/Survey/store_results/', param).success(function(response_data, status) 
        {})
        
        param = 
        {
            "h" : $scope.images[$scope.index][0],
            "s" : $scope.images[$scope.index][1],
            "v" : $scope.images[$scope.index][2],
            "index" : $scope.index
        }
        
        $scope.index = $scope.index + 1
        $scope.imageSource = $scope.$root.IMAGES_LOC + $scope.index + ".png"        
    }    
}]);