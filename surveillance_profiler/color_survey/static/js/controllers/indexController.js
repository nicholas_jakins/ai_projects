app.controller('indexController', ['$scope', '$http', '$window', '$location', '$timeout', 'growl', function($scope, $http, $window, $location, $timeout, growl) 
{
    $scope.user_name = ""
    $scope.show_input_form = true
    
    $scope.num_questions = 10;
    $scope.color_scheme = 0;
    
    $scope.question_options = [10, 20, 50, 100];
    $scope.color_schemes = [{'name' : 'Random', 'value' : 0}, 
                            {'name' : 'Darker', 'value' : 1}, 
                            {'name' : 'Lighter', 'value' : 2}]
    
    $scope.startSurvey = function() 
    {
        $scope.$root.USERNAME = $scope.user_name
        
        $http.get('http://localhost:8000/Survey/get_color_dir/').success(function(response_data, status)
        {
            $scope.$root.IMAGES_LOC = response_data.colors_location
            console.log('images location: ' + $scope.$root.IMAGES_LOC);
        })
        
        param = 
        {
            'user_name' : $scope.user_name,
            'num_questions' : $scope.num_questions,
            'scheme' : $scope.color_scheme.value
        }
        
        console.log("whyyyy! " + $scope.color_scheme.value);
        
        $http.post('http://localhost:8000/Survey/get_name/', param).success(function(response_data, status) 
        {
                
                $scope.showSuccess = function(msg, title) 
                {
                    growl.success(msg,{title: title});
                }
    
            
                $scope.showError = function(msg, title) 
                {
                    
                    growl.error(msg, {title: title});
                }
                
                $scope.showWarning = function(msg, title) 
                {
                    growl.warning(msg, {title: title});
                }
            
                console.log("response_data: ", response_data.status)
                
                var resp = response_data.status
                var colors_dir = response_data.color_dir
                    
                if (resp == 0) 
                {
                    $scope.show_input_form = false
                    $scope.showSuccess(" Valid user name: ", "Success")
                    $window.location.href = "http://localhost:8000/Survey/#surv"    
                } 
                else 
                {
                    $scope.showError(" User name already exists: ", "Error")
                }
        })     
    }
    
    $scope.show_form = function() 
    {
        $scope.show_input_form = true
    }
        
}]);