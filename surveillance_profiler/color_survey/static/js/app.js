var app = angular.module('main', ['ngRoute', 'angular-growl', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider) {
	$routeProvider
        .when('/surv', {
            controller: 'surveyController',
			templateUrl: '../static/views/color_selector.html'
        })
        .otherwise({
			redirectTo: '/'
		});
});