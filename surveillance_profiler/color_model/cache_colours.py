import paramConfig as p
from discodb import DiscoDB, Q
import psycopg2
import numpy as np
import struct

colo_obj = p.HSVModel()
colo_obj.cache_color_observations()

conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
cur = conn.cursor()

cur.execute("SELECT * FROM hsvobservations")
results = cur.fetchall()

hsv_data = {}
count = 0

for r in results:
	
	packed_data = pack_data(convert(r[0]))
	hsv_data[packed_data] = str(r[1])
	
	if count % 10000 == 0:
		print count

	count = count + 1

db = DiscoDB(hsv_data)

db.dump(file('./hsvobs_raw.db', 'w'))