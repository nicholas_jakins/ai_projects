import numpy as np
import matplotlib.pyplot as plt
import psycopg2
import cv2
import random
import paramConfig as pc


for row in xrange(1, 10, 1):
	for column in range(1, 10, 1):

		# generate a random BGR coordinate
		BGR_img = np.zeros((10, 10, 3), np.uint8)

		BGR = np.array([random.randint(0, 50), random.randint(0, 50), random.randint(0, 50)])

		for i in range(BGR_img.shape[0]):
			for j in range(BGR_img.shape[1]):
				BGR_img[i][j] = BGR

		block_RGB = cv2.cvtColor(BGR_img, cv2.COLOR_BGR2RGB)

		t = plt.gca()
		t.axes.get_xaxis().set_visible(False)
		t.axes.get_yaxis().set_visible(False)
		plt.subplot2grid((10, 10), (row, column))
		plt.imshow(block_RGB)
		plt.title("")

plt.tight_layout()
plt.show()