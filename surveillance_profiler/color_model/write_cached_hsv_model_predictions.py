from discodb import DiscoDB, Q
import psycopg2
import numpy as np
import struct

def pack_data(data):
	return struct.pack('d'*len(data), *data)

def unpack(raw_data):
	return struct.unpack('d'*3, raw_data)

def unpack_int(raw_data):
	return struct.unpack('d', raw_data)

def convert(s):
	s = s.replace("(", "")
	s = s.replace(")", "")
	s = s.split(",")
	s = [int(i) for i in s]	
	return s

colo_obj = p.HSVModel()
colo_obj.cache_color_observations()

conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
cur = conn.cursor()

cur.execute("SELECT * FROM hsvobservations")
results = cur.fetchall()

hsv_data = {}

count = 0

for r in results:

	packed_data = pack_data(convert(r[0]))
	hsv_data[packed_data] = str(r[1])
	
	if count % 10000 == 0:
		print count

	count = count + 1

db = DiscoDB(hsv_data)
db.dump(file('./hsvobs_raw.db', 'w'))
