from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2

from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
from cv2 import bgsegm as contrib
import itertools
from numpy.linalg import inv
from numpy import linspace

import psycopg2
import math
from scipy.stats import multivariate_normal
from scipy.stats import vonmises
from scipy import special
import random
import matplotlib.pyplot as plt


#dict_colors = {0:"Black", 1:"Yellow", 2:"Orange", 3:"Pink", 4:"Red", 5:"Green", 6:"Blue", 7:"Purple", 8:"Brown", 9:"Beige"}
dict_colors = {0: "White", 1:"Gray", 2:"Black", 3:"Yellow", 4:"Orange", 5:"Pink", 6:"Red", 7:"Green", 8:"Blue", 9:"Purple", 10:"Brown", 11:"Beige"}
#class HSV4D:

class HSVVonMises:

	def __init__(self):
		np.set_printoptions(suppress=True)
		self.conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
		self.cur = self.conn.cursor()
		self.mises_dist = []
		self.densities = []
		self.gather_stats()
		

	def von_mises(self, x, mu, k):
		res =  np.exp(k * np.cos(x - mu)) / (2 * np.pi * (special.iv(0, k)))
		return res

	def gather_stats(self):
		results = []
		for color_idx in range(len(dict_colors)):
			self.cur.execute("SELECT color FROM colorCategory WHERE colorCategory.category = %s", (dict_colors[color_idx],))
			results.append(self.cur.fetchall())

		for r in results:

			res = self.myArrayConverter(np.array(r))
			#print len(res)
			# generate input for the von mises
			
			hues = [i[0] for i in res]
			

			sv = [[i[1], i[2]] for i in res]

			self.process_hues(np.array(hues))

			self.process_saturation_value(np.array(sv))
			

	def process_saturation_value(self, sv):
		#print "sv: ", sv
		mu, cov = self.compute_covariance_mean(sv)
		self.densities.append((mu, cov))


	def compute_covariance_mean(self, data_matrix):
		N = data_matrix.shape[0]
		mean = np.mean(data_matrix, axis=0)

		dev_from_mean = np.subtract(data_matrix, np.array(mean))
		CSSCP = np.dot(dev_from_mean.T, dev_from_mean)
	
		covariance = np.array([([row[0] / float(N - 1), row[1] / float(N - 1)]) for row in CSSCP])
		return (mean, covariance)

	def process_hues(self, hues):
		
		hues = [(i / 179.0) * (2 * np.pi) for i in hues]

		#print "hues: ", hues
		mean = np.mean(hues)
		#print "mean: ", mean
		
		concentration = 1.0 / np.var(hues)
		#print concentration

		self.mises_dist.append((mean, concentration))

	def myArrayConverter(self, arr):
		convertArr = np.zeros((arr.shape[0], 3))

		for s in range(arr.shape[0]):
		
			exec_str = "t = " + arr[s][0]
			exec exec_str
			convertArr[s] = t


		return convertArr


	def get_color_category(self, observation):
		likelihoods = []

		observed_hue = (observation[0] / 179.0) * (2 * np.pi)

		observed_sv = np.array([observation[1], observation[2]])
		#print (observed_hue, observed_sv)

		for idx in range(len(self.mises_dist)):

			mu, k = (self.mises_dist[idx][0], self.mises_dist[idx][1])
			likelihood_hue = self.von_mises(observed_hue, mu, k)

			m, c = (self.densities[idx][0], self.densities[idx][1])
			#print "mean: ", m
			#print "cov: ", c
			likelihood_sv = multivariate_normal.pdf(observed_sv, mean=m, cov=c)


			likelihoods.append(likelihood_hue * likelihood_sv)

		#print likelihoods
		lh = np.array(likelihoods)
		max_index = lh.argmax()		

		return max_index

	def display_hue_distributions(self):

		self.num_rows = 3
		self.num_cols = 4
		f = plt.figure()
		f.suptitle('Hue distributions for color categories as angles using von mises', fontsize=14, fontweight='bold')
		
		likelihoods = []

		number_of_plot = 1
		for idx in range(len(self.mises_dist)):


			mu, k = (self.mises_dist[idx][0], self.mises_dist[idx][1])

			# display all 11 distributions
			self.display_von_mises(mu, k, f, number_of_plot)

			number_of_plot = number_of_plot + 1
		plt.show()

	def display_von_mises(self, mu, k, f, number_of_plot):

		
		ax = f.add_subplot(self.num_rows, self.num_cols, number_of_plot)
		t = plt.gca()
		t.axes.get_xaxis().set_visible(False)
		t.axes.get_yaxis().set_visible(False)

		plt.axis([0, 2*np.pi, 0, 2])
		x = np.linspace(0, 2*np.pi, num = 100)
		
		y = self.von_mises(x, mu, k)
		plt.title(dict_colors[number_of_plot - 1])
		plt.plot(x, y, c=dict_colors[number_of_plot - 1])
		
		

class HSVModel:

	def __init__(self):
        
		np.set_printoptions(suppress=True)
		self.conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
		self.cur = self.conn.cursor()
		self.densities = []
		self.gather_stats()

	def cache_color_observations(self):
		hsv_values = np.zeros((433500, 3))
		assignment_of_colors = np.zeros((433500,))
		
		conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
		cur = conn.cursor()
				
		count = 0
		big_count = 1
		for hue in xrange(0, 179, 1):
			for saturation in range(0, 255, 1):
				for value in range(0, 255, 1):
					hsv_to_insert = "(" + str(hue) + ", " + str(saturation) + ", " + str(value) + ")"
					hsv_coord = np.array([hue, saturation, value])					
					category = self.get_color_category(hsv_coord)
					cur.execute('INSERT INTO hsvobservations(color, category) VALUES (%s, %s)', (hsv_to_insert, category))

					if count % 10000 == 0:
						print count
					
					count = count + 1

		conn.commit()
		conn.close()
		
	def gather_stats(self):
		results = []
		for color_idx in range(len(dict_colors)):
			self.cur.execute("SELECT color FROM colorCategory WHERE colorCategory.category = %s", (dict_colors[color_idx],))
			res = self.cur.fetchall()
			
			#print "res: ", len(res)
			results.append(res)
		
		cnt = 0

		for i in results:
			
			res = self.myArrayConverter(np.array(i))
			
			HSV_cart = np.zeros((len(res), 2), np.float)
			HUES = [i[0] for i in res]
			hues = [(i[0] / 179.0) * 2 * np.pi for i in res]
			
			c = 0
			for h in hues:
				HSV_cart[c] = np.array([np.cos(h), np.sin(h)])
				c = c + 1
		
			data = [[HSV_cart[i][0], HSV_cart[i][1], res[i][1], res[i][2]] for i in range(len(res))]

			self.process_data(np.array(data))			

			cnt = cnt + 1
	
	def process_data(self, data):
		mu, cov = self.compute_covariance_mean(data)
		self.densities.append((mu, cov))
		
	def get_color_category(self, observation):
		likelihoods = []

		updated_hue = (observation[0] / 179.0) * (2 * np.pi)

		observed_h1 = np.cos(updated_hue)
		observed_h2 = np.sin(updated_hue)

		obs = np.array([observed_h1, observed_h2, observation[1], observation[2]])

		maximum_likelihood = -1
		max_index = -1

		for idx in range(len(self.densities)):
			
			m, c = (self.densities[idx][0], self.densities[idx][1])
			likelihood = multivariate_normal.pdf(obs, mean=m, cov=c)

			if likelihood > maximum_likelihood:
				maximum_likelihood = likelihood
				max_index = idx
	
		return max_index

	def plot_on_unit(self, coordinates, col, f):
		#print coordinates

		# calculate average angle from axis
		top = 0
		bottom = 0
		list_of_angles = []

		average_xy = np.mean(coordinates, axis=0)
		print average_xy

		x = coordinates[:, 0]
		y = coordinates[:, 1]
		
		plt.axis([-1.5, 1.5, -1.5, 1.5])
		
		
		plt.scatter(x, y, 20, c=col)

		if average_xy[0] < 0:
			x = linspace(-1, 0, 10)	
		else:
			x = linspace(0, 1, 10)
		
		grad = average_xy[1] / average_xy[0]

		print grad
		y = grad * x
		plt.plot(x, y, c=col)
		
		#plt.show()

		
	def compute_covariance_mean(self, data_matrix):
		N = data_matrix.shape[0]
		mean = np.mean(data_matrix, axis=0)

		dev_from_mean = np.subtract(data_matrix, np.array(mean))
		CSSCP = np.dot(dev_from_mean.T, dev_from_mean)
	
		covariance = np.array([([row[0] / float(N - 1), row[1] / float(N - 1), row[2] / float(N - 1), row[3] / float(N - 1)]) for row in CSSCP])
		return (mean, covariance)

	def myArrayConverter(self, arr):
		convertArr = np.zeros((arr.shape[0], 3))

		for s in range(arr.shape[0]):
		
			exec_str = "t = " + arr[s][0]
			exec exec_str
			convertArr[s] = t


		return convertArr



class colorModel:

	def __init__(self, read_config):
		if read_config == 1:
			f = open('./cm_config.py', 'r')
			input_parameters = f.read()
			input_parameters = str.replace(input_parameters, "array", "")
			f.close()

			str_to_execute = "dict_parameters = " + input_parameters
			exec str_to_execute
			self.densities = dict_parameters
			
		elif read_config == 0:
			
			self.conn = psycopg2.connect(database="colorsurvey", user="nicholasjakins")
			self.cur = self.conn.cursor()
			self.densities = []
			self.compute_parameters()
			

	def compute_parameters(self):
		f = open("cm_config.py", "w")
		results = []
		for color_idx in range(len(dict_colors)):
			self.cur.execute("SELECT color FROM colorCategory WHERE colorCategory.category = %s", (dict_colors[color_idx],))
			results.append(self.cur.fetchall())
		cnt = 0
		for i in results:
			res = self.myArrayConverter(np.array(i))
			idx = 0

			for hsv in res:
				rep_hsv = np.zeros((1, 1, 3), np.uint8)
				rep_hsv[:, :] = hsv

				IMG_BGR = cv2.cvtColor(rep_hsv, cv2.COLOR_HSV2BGR)

				res[idx] = IMG_BGR[0][0]
				idx = idx + 1

			mean, covariance = self.compute_covariance_mean(res)
			self.densities.append((cnt, mean, covariance))
			cnt = cnt + 1

		f.write(str(self.densities))
		f.close()

	def compute_covariance_mean(self, data_matrix):
		N = data_matrix.shape[0]
		mean = np.mean(data_matrix, axis=0)

		dev_from_mean = np.subtract(data_matrix, np.array(mean))
		CSSCP = np.dot(dev_from_mean.T, dev_from_mean)
	
		covariance = np.array([([row[0] / float(N - 1), row[1] / float(N - 1), row[2] / float(N - 1)]) for row in CSSCP])
		return (mean, covariance)

	def myArrayConverter(self, arr):
		convertArr = np.zeros((arr.shape[0], 3))

		for s in range(arr.shape[0]):
		
			exec_str = "t = " + arr[s][0]
			exec exec_str
			convertArr[s] = t


		return convertArr

	def get_color_category(self, observation):

		observed_img = np.zeros((100, 100, 3), np.uint8)
		observed_img[:, :] = observation
		
		likelihoods = []

		for i in self.densities:
			likelihoods.append(multivariate_normal.pdf(observation, mean=i[1], cov=i[2]))

		lh = np.array(likelihoods)
		max_index = lh.argmax()
		return dict_colors[max_index]







