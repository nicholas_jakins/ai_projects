from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2

from skimage.feature import hog
import matplotlib.pyplot as plt
from skimage import data, color, exposure
import itertools
from numpy.linalg import inv

import psycopg2
import math
from scipy.stats import multivariate_normal
import random


import color_model as pc


# generate many samples 
def test_model(color_model):
	f = plt.figure()

	for i in xrange(1, 100, 1):

		f.add_subplot(10, 10, i)

		t = plt.gca()
		t.axes.get_xaxis().set_visible(False)
		t.axes.get_yaxis().set_visible(False)

		random_BGR = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
		print "random_RGB: ", random_BGR

		img_BGR = np.zeros((100, 100, 3), np.uint8)
		img_BGR[:, :] = random_BGR
		
		img_HSV = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2HSV)
		observation = img_HSV[0][0]
		print "observation: ", observation
		
		plt.title(color_model.get_color_category(observation))

		img_RGB = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)
		plt.imshow(img_RGB)
		
	plt.tight_layout()
	plt.show()